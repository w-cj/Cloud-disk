package com.etc.entity;

import java.io.Serializable;

public class Share implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// share主键id
	private Integer shareid;
	// 分享作者id
	private Integer fromuserid;
	// 可以查看的用户id
	private Integer touserid;
	// 分享的链接地址
	private String shareaddress;
	// 分享的状态(所有人还是单个)
	private String sharestate;
	// 文件名id
	private Integer fileid;
	// 分享时间
	private String sharetime;

	public Integer getFileid() {
		return fileid;
	}

	public void setFileid(Integer fileid) {
		this.fileid = fileid;
	}

	public String getSharetime() {
		return sharetime;
	}

	public void setSharetime(String sharetime) {
		this.sharetime = sharetime;
	}

	public Integer getShareid() {
		return shareid;
	}

	public void setShareid(Integer shareid) {
		this.shareid = shareid;
	}

	public Integer getFromuserid() {
		return fromuserid;
	}

	public void setFromuserid(Integer fromuserid) {
		this.fromuserid = fromuserid;
	}

	public Integer getTouserid() {
		return touserid;
	}

	public void setTouserid(Integer touserid) {
		this.touserid = touserid;
	}

	public String getShareaddress() {
		return shareaddress;
	}

	public void setShareaddress(String shareaddress) {
		this.shareaddress = shareaddress == null ? null : shareaddress.trim();
	}

	public String getSharestate() {
		return sharestate;
	}

	public void setSharestate(String sharestate) {
		this.sharestate = sharestate == null ? null : sharestate.trim();
	}

	@Override
	public String toString() {
		return "Share [shareid=" + shareid + ", fromuserid=" + fromuserid + ", touserid=" + touserid + ", shareaddress="
				+ shareaddress + ", sharestate=" + sharestate + "]";
	}

}