<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="zh-CN" />
<meta name="renderer" content="webkit" />
<meta name="viewport"
	content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no" />

<title>Cloud-disk</title>

<link rel="stylesheet" href="${pageContext.request.contextPath}/static/mdui/css/mdui.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/main.css">
<script src="${pageContext.request.contextPath}/static/mdui/js/mdui.min.js"></script>
<script src="${pageContext.request.contextPath}/static/jq/jq.min.js"></script>
<script src="${pageContext.request.contextPath}/static/coll/common.js"></script>
<script src="${pageContext.request.contextPath}/static/coll/jquery.cookie.js"></script>

</head>



	<body class="mdui-drawer-body-left ${ theme }">
	<header
		class="mdui-toolbar mdui-color-blue-600 mdui-appbar mdui-appbar-fixed">
		
		<span
			class="mdui-btn mdui-btn-icon mdui-ripple mdui-ripple-white mdui-text-color-indigo-200"
			mdui-drawer="{target: '#drawer', swipe: true}"><i
			class="mdui-icon material-icons">menu</i></span> <a href="${pageContext.request.contextPath}/toSystem"
			class="mdui-typo-title mdui-text-color-white-text"> <span
			class="mdui-color-purple-400"
			style="padding: 0 4px; margin-right: 2px;"> <span
				style="color: #f8d409;">C</span>loud
		</span><span class="">-</span>disk主页
		</a> <a  class="mdui-typo-title mdui-text-color-white-text">
			<span class="">好友请求</span> <span style="color: #f8d409;"
			name="FriendReq"></span>
		</a>
		
		<div class="mdui-row">
		<div class="mdui-col-md-1"></div>
		
		<div class="mdui-col-md-1"></div>
		</div>
		<div class="mdui-toolbar-spacer"></div>

		<div class="mdui-hidden-sm-down">${ username }</div>

		<!-- <a href="System?theme=1" mdui-tooltip="{content: '切换主题'}"> <i
			class="mdui-icon material-icons">color_lens</i>
		</a> -->

		<!-- github logo -->

		<a class="mdui-hidden-sm-down" href=""
			class="mdui-btn mdui-btn-icon mdui-ripple mdui-ripple-white"
			mdui-tooltip="{content: '查看 Github'}"> <svg version="1.1"
				id="Layer_1" xmlns="http://www.w3.org/2000/svg"
				xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				viewBox="0 0 36 36" enable-background="new 0 0 36 36"
				xml:space="preserve" class="mdui-icon"
				style="width: 24px; height: 24px;">
                <path fill-rule="evenodd" clip-rule="evenodd"
					fill="#ffffff"
					d="M18,1.4C9,1.4,1.7,8.7,1.7,17.7c0,7.2,4.7,13.3,11.1,15.5
          c0.8,0.1,1.1-0.4,1.1-0.8c0-0.4,0-1.4,0-2.8c-4.5,1-5.5-2.2-5.5-2.2c-0.7-1.9-1.8-2.4-1.8-2.4c-1.5-1,0.1-1,0.1-1
          c1.6,0.1,2.5,1.7,2.5,1.7c1.5,2.5,3.8,1.8,4.7,1.4c0.1-1.1,0.6-1.8,1-2.2c-3.6-0.4-7.4-1.8-7.4-8.1c0-1.8,0.6-3.2,1.7-4.4
          c-0.2-0.4-0.7-2.1,0.2-4.3c0,0,1.4-0.4,4.5,1.7c1.3-0.4,2.7-0.5,4.1-0.5c1.4,0,2.8,0.2,4.1,0.5c3.1-2.1,4.5-1.7,4.5-1.7
          c0.9,2.2,0.3,3.9,0.2,4.3c1,1.1,1.7,2.6,1.7,4.4c0,6.3-3.8,7.6-7.4,8c0.6,0.5,1.1,1.5,1.1,3c0,2.2,0,3.9,0,4.5
          c0,0.4,0.3,0.9,1.1,0.8c6.5-2.2,11.1-8.3,11.1-15.5C34.3,8.7,27,1.4,18,1.4z"></path>
            </svg>
		</a> -->

		<!-- exit icon -->
		<a href="Logout"> <span
			class="mdui-btn mdui-btn-icon mdui-ripple mdui-ripple-white"
			mdui-tooltip="{content: '退出'}"> <i
				class="mdui-icon material-icons mdui-text-color-white">exit_to_app</i>
		</span>
		</a>
	</header>
	<div style="height: 70px;"></div>


	<div id="drawer" class="mdui-drawer mdui-color-grey-800"
		style="margin-top: 56px;">
		<ul class="mdui-list mdui-collapse" mdui-collapse="options">
			<!-- <div class="mdui-collapse-item">
				<div class="mdui-collapse-item-header mdui-list-item mdui-ripple">
					<i
						class="mdui-list-item-icon mdui-icon material-icons mdui-text-color-green-300">dashboard</i>
					<div class="mdui-list-item-content">系统信息</div>
					<i class="mdui-collapse-item-arrow mdui-icon material-icons">keyboard_arrow_down</i>
				</div>
				<div class="mdui-collapse-item-body mdui-list">
					<a href="System" class="mdui-list-item mdui-ripple ">基本仪表盘</a>
				</div>
			</div> -->

			<div class="mdui-collapse-item">
				<div class="mdui-collapse-item-header mdui-list-item mdui-ripple ">
					<i
						class="mdui-list-item-icon mdui-icon material-icons mdui-text-color-red">layers</i>
					<div class="mdui-list-item-content">文件管理</div>
					<i class="mdui-collapse-item-arrow mdui-icon material-icons">keyboard_arrow_down</i>
				</div>
				<div class="mdui-collapse-item-body mdui-list">
					<a href="${pageContext.request.contextPath}/directory?secondId=0&userId=${user.userid}""
						class="mdui-list-item mdui-ripple ">所有文件目录</a> <a
						href="${pageContext.request.contextPath}/getFile?fileType=4&userId=${user.userid}"
						class="mdui-list-item mdui-ripple ">图片</a> <a
						href="${pageContext.request.contextPath}/getFile?fileType=2&userId=${user.userid}"
						class="mdui-list-item mdui-ripple ">视频</a> <a
						href="${pageContext.request.contextPath}/getFile?fileType=1&userId=${user.userid}"
						class="mdui-list-item mdui-ripple ">音乐</a> <a
						href="${pageContext.request.contextPath}/getFile?fileType=3&userId=${user.userid}"
						class="mdui-list-item mdui-ripple ">文档</a> <!-- <a
						href="FileManager?type=bt&page=1"
						class="mdui-list-item mdui-ripple ">种子</a> <a
						href="FileManager?type=code&page=1"
						class="mdui-list-item mdui-ripple ">源代码</a> <a
						href="FileManager?type=run&page=1"
						class="mdui-list-item mdui-ripple ">可执行文件</a> -->
				</div>
			</div>

			<div class="mdui-collapse-item">
				<div class="mdui-collapse-item-header mdui-list-item mdui-ripple">
					<i
						class="mdui-list-item-icon mdui-icon material-icons mdui-text-color-blue">book</i>
					<div class="mdui-list-item-content">帮助支持</div>
					<i class="mdui-collapse-item-arrow mdui-icon material-icons">keyboard_arrow_down</i>
				</div>
				<div class="mdui-collapse-item-body mdui-list">
					<a href="${pageContext.request.contextPath}/about" class="mdui-list-item mdui-ripple ">关于我们</a>
				</div>
			</div>

			<div class="mdui-list-item mdui-ripple">
				<i
					class="mdui-list-item-icon mdui-icon material-icons mdui-text-color-brown-300">cloud_upload</i>
				<div id="su-upload-text" class="mdui-list-item-content"
					onclick="PAGE.upload();">上传文件</div>
			</div>

			<%-- <div class="mdui-list-item mdui-ripple">
				<i
					class="mdui-list-item-icon mdui-icon material-icons mdui-text-color-orange-400">chat</i>
				<div class="mdui-list-item-content">
					<a href="${pageContext.request.contextPath}/Online" onclick="">好友搜索</a>
				</div>
			</div> --%>

			<div class="mdui-list-item mdui-ripple">
				<i
					class="mdui-list-item-icon mdui-icon material-icons mdui-text-color-green-400">chat</i>
				<div class="mdui-list-item-content">
					<a href="${pageContext.request.contextPath}/addFriend" onclick="">添加好友</a>
				</div>
			</div>


			<div class="mdui-list-item mdui-ripple">
				<i
					class="mdui-list-item-icon mdui-icon material-icons mdui-text-color-indigo-300">share</i>
				<div class="mdui-list-item-content">
					<a href="${pageContext.request.contextPath}/myShare/${user.userid}">我的分享</a>
				</div>
			</div>
			<div class="mdui-list-item mdui-ripple">
				<i
					class="mdui-list-item-icon mdui-icon material-icons mdui-text-color-indigo-300">share</i>
				<div class="mdui-list-item-content">
					<a href="${pageContext.request.contextPath}/friendShare?touserid=${user.userid}">好友分享</a>
				</div>
			</div>
			
			<div class="mdui-list-item mdui-ripple">
				<i
					class="mdui-list-item-icon mdui-icon material-icons mdui-text-color-orange-400">dashboard</i>
				<div class="mdui-list-item-content">
					<a href="${pageContext.request.contextPath}/myAudit?userId=${user.userid}" onclick="">我的审核</a>
				</div>
			</div>
		</ul>
	</div>

	<!-- 浮动按钮  -->
	<div class="mdui-fab-wrapper" id="exampleFab"
		mdui-fab="{trigger: 'hover'}">
		<button class="mdui-fab mdui-ripple  mdui-color-orange">
			<!-- 默认显示的图标 -->
			<i class="mdui-icon material-icons">add</i>

			<!-- 在拨号菜单开始打开时，平滑切换到该图标，若不需要切换图标，则可以省略该元素 -->
			<i class="mdui-icon mdui-fab-opened material-icons">apps</i>
		</button>
		<div class="mdui-fab-dial">

			<a href="System">
				<button
					class="mdui-fab mdui-fab-mini mdui-ripple mdui-color-orange-600">
					<i class="mdui-icon material-icons">dashboard</i>
				</button>
			</a> <a href="FileManager?page=1">
				<button class="mdui-fab mdui-fab-mini mdui-ripple mdui-color-blue">
					<i class="mdui-icon material-icons">layers</i>
				</button>
			</a> <a href="Online">
				<button class="mdui-fab mdui-fab-mini mdui-ripple mdui-color-red">
					<i class="mdui-icon material-icons">chat</i>
				</button>
			</a> <a onclick="PAGE.upload();">
				<button class="mdui-fab mdui-fab-mini mdui-ripple mdui-color-yellow">
					<i class="mdui-icon material-icons">cloud_upload</i>
				</button>
			</a> <a href="Share?page=1">
				<button class="mdui-fab mdui-fab-mini mdui-ripple mdui-color-pink">
					<i class="mdui-icon material-icons">share</i>
				</button>
			</a> <a onclick="PAGE.smoothscroll()">
				<button class="mdui-fab mdui-fab-mini mdui-ripple mdui-color-green">
					<i class="mdui-icon material-icons">vertical_align_top</i>
				</button>
			</a>
		</div>
	</div>
	</body>
	
	<div id="showReq" style="display: none" >
		
	</div>
	<script src="${pageContext.request.contextPath}/layer/2.1/layer.js"></script>
	<script type="text/javascript">
	//时时跟新好友请求
	//存储所有的好友请求
	var allReq = null;
	var replace = function() {
		$.ajax({
			url : "${pageContext.request.contextPath}/getFriendsReq",
			type : "get",
			data : {
				"userid" : ${user.userid}
			},
			success : function(data) {
				$("span[name='FriendReq']").text(data.length+"未处理");
				$('#showReq').children('div').remove();
				$.each(data,function(index,user){
					$('#showReq').append('<div>'+'<span class="ReqName">'+user.username+'请求添加好友'+'</span>'+'<button type="button" class="agree" title='+user.userid+'>'+'同意'+'</button>'+'<button type="button" class="refuse" title='+user.userid+'>'+'拒绝'+'</button>'+'</div>')
				})
				//allReq = cm;
			}
		});
	}

	//动态添加事件 弹出好友申请列表
	$(document).on('click','span[name="FriendReq"]',function(){
		layer.open({
			type : 1,
			area : [ "240px", "300px" ],
			offset: ['50px', '350px'],
			content : $("#showReq")
		})
		
	});
	
	//动态添加同意按钮事件
	$(document).on('click','.agree',function(){
		var fromuserid = $(this).prop('title');
		var touserid = ${user.userid};
		$.ajax({
			url:"${pageContext.request.contextPath}/addFriend",
			type:"post",
			data:{"fromuserid":fromuserid,"touserid":touserid,"relationstate":"1"},
			success:function(cm){
				if(cm.msg!=null){
					layer.msg(cm.msg,
							{
						icon : 1, //图标		
						time : 2000
					//2秒关闭（如果不配置，默认是3秒）
					});
				}
				
			}
		});
	});
	
	//动态添加拒绝按钮事件
	$(document).on('click','.refuse',function(){
		var fromuserid = $(this).prop('title');
		var touserid = ${user.userid};
		$.ajax({
			url:"${pageContext.request.contextPath}/delFriendsReq",
			type:"post",
			data:{"fromuserid":fromuserid,"touserid":touserid,"relationstate":"1"},
			success:function(cm){
				if(cm.msg!=null){
					layer.msg(cm.msg,
							{
						icon : 1, //图标		
						time : 2000
					//2秒关闭（如果不配置，默认是3秒）
					});
				}
				
			}
		});
	});
	
	//定时器 实时更新
	window.setInterval('replace()', '2000');
	
	</script>
	<%@include file="comp.jsp"%>