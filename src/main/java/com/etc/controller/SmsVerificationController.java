package com.etc.controller;
import javax.annotation.Resource;
import org.jboss.logging.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.etc.entity.Code;
import com.etc.service.CodeService;
import com.etc.util.CommenMassage;
import com.etc.util.CommonRpc;

/**
 * 发送验证码的控制器
 * @author Administrator
 *
 */
@Controller
public class SmsVerificationController {
	@Autowired
	private CommenMassage msg;
	@Autowired
	private CodeService codeService;
	@Autowired
	private Code code;
	private int num;
	@RequestMapping(value="send",method=RequestMethod.GET)
	@ResponseBody
	public CommenMassage sendMessage(@RequestParam(value="userphone") String userphone) {
		//随机生成一个4位数的验证码
		num=(int) (Math.random()*9000)+1000;
		code.setCode(num);
		code.setUserphone(userphone);
		//更新验证码
		Code cd= codeService.selectByUserPhone(userphone);
		if(cd!=null) {
			boolean flag=codeService.updateByPrimaryKey(code);
		}else {
			boolean flag=codeService.insert(code);
		}
		CommonRpc.sendMassage(userphone,num);
		msg.setMsg("发送成功");
		return msg;
		
	}
}
