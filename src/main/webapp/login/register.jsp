﻿<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="zh-CN" />
<meta name="renderer" content="webkit" />
<meta name="viewport"
	content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no" />

<title>注册 | Linkresources</title>

<link href="./static/template/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="./static/login/css/style.css" rel="stylesheet">
<link href="./static/common/image/icon.ico" rel="shortcut icon">

<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script
	src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css"
	rel="stylesheet">

<style>
#msg {
	width: 100%;
	line-height: 40px;
	font-size: 14px;
	text-align: center;
}

.reg-login-header {
	height: 90px;
	/*background-color: #00F7DE;*/
}

.logobox {
	height: 49px;
}

.form-horizontal .has-feedback .form-control-feedback {
	right: 0px;
	/*top: 21px;*/
}

.input-box-width {
	width: 300px;
	margin-bottom: 5px;
}

.notify-str {
	font-weight: 500;
	margin-bottom: 0px;
	font-size: 14px;
}

#loginpwd {
	display: inline-block;
}
</style>
<!--[if lt IE 9]>
        <div id='Not_' class="show-ui" style="height: 40px;background-color: rgb(221,79,67);text-align:center;line-height: 40px;color: white;">
            <b>无法访问! 您的浏览器版本过低或是兼容模式,请使用最新/更高版本的浏览器: IE10+ chrome FireFox 等，如果是国内浏览器请打开极速浏览模式（webkit内核）。</b>
        </div>
    <![endif]-->
</head>

<body>

	<div class="mvideocon" style="">
		<video class="mvideo" type="video/mp4" preload="metadata" loop=""
			autoplay="" muted=""
			poster="https://web.ccpgamescdn.com/eveonlineassets/bgvid/video-bg-image.jpg">
			<source src="static/bg.mp4" autostart="false">
		</video>
	</div>
	<div id='login-view' class="show-ui">
		<div id='login-sub'>
			<div class="container-fluid">
				<div class="row">
					<img id='logo' src="static/common/logo.png"> <br>
				</div>
				<div id='login-form' class="row">
					<!-- 表单开始 -->
					<form class="form-horizontal" role="form" id="Action_home"
						name="login_form"
						action="${pageContext.request.contextPath}/Register"
						target="_parent" method="post">
						<!--用户名校验提示 -->
						<div class="group" id="gaution" style="color: red; opacity: 0;">
							该用户名已被注册！！！</div>
						<!--用户名校验提示结束 -->
						<div class="form-group has-feedback">
							<div class="col-sm-12" style="padding-right: 1px">
								<input type="text" class="form-control border-1 minput"
									id="username" name="username" placeholder="注册用户名"
									autocomplete="off"> <span
									class="glyphicon glyphicon-user form-control-feedback"
									aria-hidden="true"></span>
							</div>
						</div>
						<div style="opacity: 0;">1</div>
						<div class="form-group has-feedback">
							<div class="col-sm-12" style="padding-right: 1px">
								<input type="password" class="form-control border-1 minput"
									id="userpwd" name="userpwd" placeholder="注册密码"
									autocomplete="off"> <span
									class="glyphicon glyphicon-th form-control-feedback"
									aria-hidden="true"></span>
							</div>

						</div>
						<!--手机号校验提示 -->
						<div class="group" id="phonecheck" style="color: red; opacity: 0;">
							该手机号已被注册！！！</div>
						<!--手机号校验提示结束 -->
						<div class="form-group has-feedback">
							<div class="col-sm-12" style="padding-right: 1px">
								<input type="text" class="form-control border-1 minput"
									id="userphone" name="userphone" placeholder="手机号码"
									autocomplete="off" style="border:;"> <span
									class="glyphicon glyphicon-user form-control-feedback"
									aria-hidden="true"></span>
							</div>
						</div>

						<div class="form-group has-feedback input-box-width"
							id="code-error"
							style="position: relative; left: 17px; top: -22px">
							<label id="pwd-exist" class="control-label notify-str"
								for="loginpwd" style="opacity: 0">验证码有误</label>
							<div>
								<input type="text" class="form-control" name="password"
									id="loginpwd" aria-describedby="inputSuccess2Status"
									placeholder="请输入验证码" style="width: 150px" />
								<button class="btn btn-success" id="code-btn" name="btn"
									style="width: 135px; float: right" disabled="disabled">发送验证码
								</button>
							</div>
							<span id="pwd-check" class="glyphicon form-control-feedback"
								aria-hidden="true"></span> <span id="inputSuccess2Status"
								class="sr-only">(success)</span>
						</div>

						<div class="radio"
							style="position: relative; left: 3px; top: -22px">
							<label> <input type="radio" name="optionsRadios"
								id="optionsRadios1" value="男" checked>男
							</label> <label> <input type="radio" name="optionsRadios"
								id="optionsRadios2" value="女">女
							</label>
						</div>
				</div>
				<!-- <div class="form-group">
							<div class="col-xs-12">
								<a href="/" id="form-help" class="text-right help-text">账户名支持中文，密码必须6到18位个字</a>
							</div>
						</div> -->
				<div class="form-group">
					<div class="col-sm-6">
						<a href="login.jsp">
							<div id='login-button' class="btn btn-danger btn-info border-1">
								<b>已有账号</b>
							</div>
						</a>
					</div>
					<div class="col-sm-6">
						<input id='register-button' class="btn btn-success "
							style="width: 135px;" disabled="disabled" value="现在注册" />

					</div>
				</div>
			</div>
			</form>
			<!-- 表单结束 -->
		</div>

	</div>
	</div>
	</div>
	<div id='view-info' class="alert alert-info show-ui tools-info">默认信息框</div>
</body>
<script type="text/javascript" src="./static/template/jq/jq.min.js"></script>

<script>
function btnRegister(){
	var p1 = $("#post1").val();
	var p2 = $("#post2").val();
	var p3 = $("#post3").val();
	if(p1.length < 2 || p1.length > 18){
		alert("错误的用户名长度");
		return;
	}
	/* if(p2 != p3){
		alert("两次输入的密码不一致");
		return;
	} */
	if(p2.length < 6 || p2.length > 18){
		alert("密码长度不正确")
		return;
	}
	if(p1.indexOf("'") != -1 || p1.indexOf(">") != -1 || p1.indexOf("<") != -1 || p1.indexOf('"') != -1 || p1.indexOf(' ') != -1  || p1.indexOf('-') != -1){
		alert("用户名有 非法字符");
		return;
	}
	$("#Action_home").submit();
}
</script>

<!--前台注册用户框失焦事件 -->
<script type="text/javascript">
$(function () {
	$("#username").blur(function () {
		$.ajax({
			url:"../checkUserByName",
			type:"GET",
			data:{
				"username":$("#username").val()
			},
			success:function(data){
				if(data.msg=="该用户存在"){
					//显示警告信息
					$("#gaution").css("opacity",1);
					//清空表单内容
					$("#Action_home")[0].reset();
				}else if(data.msg==""){
					$("#username")[0].style.border='1px solid red';
				}else{
					//隐藏警告信息
					$("#gaution").css("opacity",0);
					//取消边框警告
					$("#username")[0].style.border='';
				}
			}
			
		})
		
	})	
})
</script>
<!--前台注册用户框失焦事件 -->

<!--前台注册密码框失焦事件 -->
<script type="text/javascript">
$(function () {
	$("#userpwd").blur(function () {
		$.ajax({
			url:"../checkUserPwd",
			type:"GET",
			data:{
				"userpwd":$("#userpwd").val()
			},
			success:function(data){
				if(data.msg=="密码为空"){
					//显示警告信息
					$("#userpwd")[0].style.border='1px solid red';
				}else{
					//取消边框警告
					$("#userpwd")[0].style.border='';
				}
			}
			
		})
		
	})	
})
</script>
<!--前台注册密码框失焦事件 -->

<!--前台注册手机框失焦事件 -->
<script type="text/javascript">
$(function () {
	var InterValObj; //timer变量，控制时间
    var count = 60; //间隔函数，1秒执行
    var curCount = 0;//当前剩余秒数
    var sign=1;
	$("#userphone").blur(function () {
		$.ajax({
			url:"../checkUserByPhone",
			type:"GET",
			data:{
				"userphone":$("#userphone").val()
			},
			success:function(data){
				if(data.msg=="该手机号已注册"){
					//显示警告信息
					$("#phonecheck").css("opacity",1);
					//清空表单内容
					$("#userphone").val("");
					//禁用按钮
					$("#code-btn").attr("disabled",'false');
				}else if (data.msg=="该手机号不合规"){
					layer.msg(data.msg, {
						icon : 1,
						time : 2000
					});
					$("#phonecheck").css("opacity",0);
					//$("#userphone").val("该手机号格式不符合规定");
					$("#userphone")[0].style.border='1px solid red';
					//清空表单内容
					$("#userphone").val("");
					//禁用按钮
					$("#code-btn").attr("disabled",'true');
				}else if(curCount==0 && sign==1){
					$("#phonecheck").css("opacity",0);
					//启用按钮
					$("#code-btn").removeAttr("disabled");
					$("#userphone")[0].style.border='';
				}else{
					$("#phonecheck").css("opacity",0);
					//启用按钮
					$("#code-btn").attr("disabled","false");
					$("#userphone")[0].style.border='';
				}
			}
			
		})
		
	})	
	
        $('#code-btn').click(function () {
            curCount = count;
            $("#code-btn").attr("disabled", "true");
            $("#code-btn").text(curCount + "秒后可重新发送");
            InterValObj = window.setInterval(SetRemainTime, 1000); //启动计时器，1秒执行一次请求后台发送验证码
        });
    // timer处理函数
    function SetRemainTime() {
        if (curCount == 0) {
            window.clearInterval(InterValObj);//停止计时器
            $("#code-btn").removeAttr("disabled");//启用按钮
            $("#code-btn").text("发送验证码");
        } else {
            curCount--;
            $("#code-btn").text(curCount + "秒后可重新发送");
        }
    }
})

 var check = function(){
	 
	 if(($("#username").val()!="")&&($("#userpwd").val()!="")
	    &&($("#userphone").val()!="")&&($("#loginpwd").val()!="")){
		//启用按钮
    	$("#register-button").removeAttr("disabled");
    }else{
    	
    	$("#register-button").attr("disabled", "false");
    }
 }
 
 setInterval("check()",1000)

</script>
<!--前台注册前台注册发送验证码事件-->
<script src="${pageContext.request.contextPath}/layer/2.1/layer.js"></script>
<script type="text/javascript">
	$(function () {
		<!--发送短信-->
		 $("#code-btn").click(function () {
			 $.ajax({
					url:"../send",
					type:"GET",
					data:{
						"userphone":$("#userphone").val()
						//"password":$("#loginpwd").val()
					},
				success:function(data){
					if(data.msg=="发送成功"){
						layer.msg(data.msg, {
							icon : 1,
							time : 2000
						});
					  }else{
						  layer.msg("发送失败", {
								icon : 1,
								time : 2000
							});
						}
				
					}
				}) 
		}) 
		
	})
</script>


<!--前台注册功能 -->
<script type="text/javascript">
	$(function(){
		$("#register-button").click(function () {
			$.ajax({
				url:"../register",
				type:"GET",
				data:{
					"username":$("#username").val(),
					"userpwd":$("#userpwd").val(),
					"userphone":$("#userphone").val(),
					"password":$("#loginpwd").val(),
					"usersex":$("input[type='radio']:checked").val()
				},
			success:function(data){
				if(data.msg=="添加成功"){
					$("#Action_home")[0].reset();
					layer.msg(data.msg, {
						time : 2000
					}, function() {
						window.location.href="${pageContext.request.contextPath}/login/login.jsp";
					});
					
				  }else if(data.msg=="验证码有误"){
					  layer.msg(data.msg, {
							icon : 1,
							time : 2000
						});
					}else{
						layer.msg(data.msg, {
							icon : 1,
							time : 2000
						});
					}
				
				}
			})
			
		});
	})
	
</script>
<!--前台注册功能 -->

</html>