package com.etc;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.etc.entity.User;
import com.etc.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserTest {
	
	@Resource
	private UserService userService;
	
	
	@Test
	public void getUserByPage() {
		
		userService.selectAllUserByPage(0, 2, "").getData().forEach(System.out::println);
		
	}
	
	/*@Test
	public void addUser() {
		
		User user =  new User("小红", "77777", "88888");
		
		boolean flag = userService.insert(user);
		System.out.println(flag);
		
	}*/
	
	/*@Test
	public void updateUser() {
		
		User user =  new User(7, null, null,"321321", 1, 1);
		
		boolean flag = userService.updateUser(user);
		
		System.out.println(flag);
		
	}*/
	
	@Test
	public void deleteUser() {
		
		
		
		boolean flag = userService.deleteByPrimaryKey(7);
		
		System.out.println(flag);
		
	}

}
