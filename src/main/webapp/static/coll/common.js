//公共类 js

//每个页面的全局寄生变量
window.PAGE = {};

(function () {

	//文件上传
	PAGE.upload = function () {
		$("#m-upload-file").click();
	}

	//异步上传
	PAGE.formDataUpload = function (file, progress) { //$("#m-upload-file")[0].files[0]
		if (typeof FormData != "function") {
			tools.popWindow("很遗憾，您的浏览器不兼容异步文件上传。请使用现代浏览器！");
			return null;
		}
		var oMyForm = new FormData();
		oMyForm.append("time", new Date().toUTCString());
		oMyForm.append("upload_file", file);

		var oReq = new XMLHttpRequest();
		oReq.open("POST", "UpLoader", true);
		oReq.onload = function (oEvent) {
			if (oReq.status == 200) {
				window.location.reload();
			} else {
				mdui.alert("文件上传失败！");
			}
		};
		oReq.upload.addEventListener("progress", function (evt) {
			var percentComplete = Math.round(evt.loaded * 100 / evt.total);
			progress(percentComplete);
		}, false);
		oReq.send(oMyForm);

	};


	//复制到剪切板
	PAGE.copyToText = function (text) {
		var oInput = document.createElement('input');
		oInput.value = text;
		document.body.appendChild(oInput);
		oInput.select(); // 选择对象
		document.execCommand("Copy"); // 执行浏览器复制命令
		oInput.className = 'oInput';
		oInput.style.display = 'none';
		mdui.snackbar({
			message: '链接地址已复制',
			position: 'right-bottom'
		});
	}

	//获取所有列表复选框已选择的元素
	PAGE.getCheckedList = function (el) {
		var files_arr = [];
		var file_list = $(el);
		for (var i = 0; i < file_list.length; i++) {
			var isChecked = $(file_list[i]).is(':checked');
			if (isChecked) {
				var name = file_list[i].dataset.elev;
				console.log("CHECKED --> ", name);
				files_arr.push(name);
			}
		}
		return files_arr;
	}

	//全选所有列表
	PAGE._alloperbool = false;
	PAGE.alloper = function (el) {
		var file_list = $(el);
		for (var i = 0; i < file_list.length; i++) {
			if (PAGE._alloperbool) {
				$(file_list[i]).removeAttr("checked");
			} else {
				$(file_list[i]).attr("checked", 'true');
			}
		}
		PAGE._alloperbool = !PAGE._alloperbool;
	}

	//回到顶部
	PAGE.smoothscroll = function () {
		var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
		if (currentScroll > 0) {
			window.requestAnimationFrame(PAGE.smoothscroll);
			window.scrollTo(0, currentScroll - (currentScroll / 5));
		}
	};

})();