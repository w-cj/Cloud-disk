package com.etc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etc.dao.ShareMapper;
import com.etc.entity.Share;
import com.etc.service.ShareService;

/**
 * 好友分享的service层实现类
 * 
 * @author zeng
 *
 */
@Service
public class ShareServiceImpl implements ShareService {

	/**
	 * 注入ShareMapper
	 */
	@Autowired
	private ShareMapper shareMapper;

	/**
	 * 删除一条分享记录
	 */
	@Override
	public boolean deleteShareById(Integer shareId) {
		// TODO Auto-generated method stub
		return shareMapper.deleteByPrimaryKey(shareId) > 0;
	}

	/**
	 * 添加一条分享记录
	 */
	@Override
	public boolean addShare(Share share) {
		// TODO Auto-generated method stub
		return shareMapper.insert(share) > 0;
	}

	/**
	 * 查询总的分享数
	 */
	@Override
	public Integer selectShare() {
		// TODO Auto-generated method stub
		return shareMapper.selectShare();
	}

}
