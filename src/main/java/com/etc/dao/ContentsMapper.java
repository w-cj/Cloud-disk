package com.etc.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.etc.entity.Contents;

/**
 * 
 * @author China 20192019年5月24日上午9:32:50
 */
@Mapper
@Repository
public interface ContentsMapper {
	int deleteByPrimaryKey(Integer contentsid);

	int insert(Contents record);

	int insertSelective(Contents record);

	Contents selectByPrimaryKey(Integer contentsid);

	int updateByPrimaryKeySelective(Contents record);

	int updateByPrimaryKey(Contents record);

	/**
	 * 添加目录获取主键值
	 * 
	 * @param record
	 *            目录对象
	 * @return 主键值
	 */
	int insertSelectiveGetKey(Contents record);

	// 根据上一级目录的id获取该目录所拥有的目录
	List<Contents> getContentsBySecondId(Integer secondId, Integer userId);

	// 根据上一级目录的id和模糊查询名字获取该目录所拥有的目录
	List<Contents> getContentsByLike(@Param(value = "userId") Integer userId,@Param(value = "content") String content);

	// 根据上一级目录的id来查找上一级目录的路径
	String getContentsPathBySecondId(Integer secondId);

	// 根据上一级目录的id来查找上一级目录的再上一级目录的id
	Integer getSecondIdBySecondId(Integer secondId);

	// 通过上一级目录id和文件名查询单个文件
	Contents selectBySecondIdAndContentsName(@Param(value = "secondId") Integer secondId,
			@Param(value = "contentsName") String contentsName);

	int deleteBySecondIdAndContentsName(@Param(value = "secondId") Integer secondId,
			@Param(value = "contentsName") String contentsName);
}