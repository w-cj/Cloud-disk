package com.etc.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.etc.entity.Share;
import com.etc.entity.User;

@Mapper
@Repository
public interface ShareMapper {
	
	
    int updateByPrimaryKeySelective(Share record);
	/**
	 * 删除一条分享记录
	 * @param shareid 分享的id
	 * @return 受影响行数
	 */
    int deleteByPrimaryKey(Integer shareid);
    /**
     * 添加一条分享记录
     * @param record 分享对象
     * @return 受影响行数
     */
    int insert(Share record);
   
    /**
     * 查询分享记录
     * @param userid 用户的id
     * @return 返回一个分享记录的集合
     */
    List<User> selectByPrimaryKey(@Param("userid")Integer userid);
    
    int selectShare();
}