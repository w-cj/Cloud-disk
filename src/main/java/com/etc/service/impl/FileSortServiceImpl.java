package com.etc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etc.dao.FileSortMapper;
import com.etc.entity.FileSort;
import com.etc.service.FileSortService;
import com.etc.util.PageData;

/**
 * 分页查询每种文件类型的service接口实现类
 * 
 * @author zeng
 *
 */
@Service
public class FileSortServiceImpl implements FileSortService {

	/**
	 * 注入FileSortMapper
	 */
	@Autowired
	private FileSortMapper fileSortMapper;

	/**
	 * 分页查询图片
	 * 
	 * @param page
	 *            当前页
	 * @param limit
	 *            每页的页数
	 * @param content
	 *            模糊查询的内容
	 * @return PageData的对象
	 */
	@Override
	public PageData<FileSort> selectPictureBypage(int page, int limit, String content) {
		// TODO Auto-generated method stub
		int start = (page - 1) * limit;

		List<FileSort> list = fileSortMapper.selectPicture(start, limit, content);

		int count = fileSortMapper.getCount(content);

		PageData<FileSort> pd = new PageData<>(list, count, limit, page);
		return pd;

	}

	/**
	 * 分页查询音乐
	 * 
	 * @param page
	 *            当前页
	 * @param limit
	 *            每页的页数
	 * @param content
	 *            模糊查询的内容
	 * @return PageData的对象
	 */
	@Override
	public PageData<FileSort> selectMusicBypage(int page, int limit, String content) {
		// TODO Auto-generated method stub
		int start = (page - 1) * limit;

		List<FileSort> list = fileSortMapper.selectMusic(start, limit, content);

		int count = fileSortMapper.getCount(content);

		PageData<FileSort> pd = new PageData<>(list, count, limit, page);
		return pd;
	}

	/**
	 * 分页查询视频
	 * 
	 * @param page
	 *            当前页
	 * @param limit
	 *            每页的页数
	 * @param content
	 *            模糊查询的内容
	 * @return PageData的对象
	 */
	@Override
	public PageData<FileSort> selectMovieBypage(int page, int limit, String content) {
		// TODO Auto-generated method stub
		int start = (page - 1) * limit;

		List<FileSort> list = fileSortMapper.selectMovie(start, limit, content);

		int count = fileSortMapper.getCount(content);

		PageData<FileSort> pd = new PageData<>(list, count, limit, page);
		return pd;
	}

	/**
	 * 分页查询文档
	 * 
	 * @param page
	 *            当前页
	 * @param limit
	 *            每页的页数
	 * @param content
	 *            模糊查询的内容
	 * @return PageData的对象
	 */
	@Override
	public PageData<FileSort> selectTmpBypage(int page, int limit, String content) {
		// TODO Auto-generated method stub
		int start = (page - 1) * limit;

		List<FileSort> list = fileSortMapper.selecttmp(start, limit, content);

		int count = fileSortMapper.getCount(content);

		PageData<FileSort> pd = new PageData<>(list, count, limit, page);
		return pd;
	}

}
