<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@include file="header.jsp"%>


<div class="mdui-container">

	<div class="mdui-row">
		<div class="mdui-col-md-1"></div>
		<div class="mdui-col-md-10">
			<form class="form-horizontal" role="form" id="Action_home"
				name="login_form" 
				target="_parent" method="get">
				<div style="margin-top: 10%;">
					<div class="mdui-textfield">
						<div class="mdui-textfield">
							<input id="content" name="content" class="mdui-textfield-input" type="text"
								maxlength="18" placeholder="搜索用户" autocomplete="off" />
						</div>
						<button  onclick="getUser()"
							class="mdui-btn mdui-color-purple-400 mdui-ripple" type="button">搜索</button>
					</div>
				</div>
			</form>
		</div>
		<div class="mdui-col-md-1"></div>
	</div>

	<div class="mdui-row">
		<div class="mdui-col-md-1"></div>
		<div class="mdui-col-md-10">
			<div style="margin-top: 30px;" id="divshow">
				<%-- <c:forEach items="${users}" varStatus="i" var="item">
					<div class="mdui-chip su-user-list"
						onclick="PAGE.toShareFiles('${ item.username }');">
						<span class="mdui-chip-icon mdui-text-uppercase">${ item.username.toCharArray()[0] }</span>
						<span class="mdui-chip-title">${ item.username }</span>
					</div>
				</c:forEach> --%>
			</div>
		</div>
		<div class="mdui-col-md-1"></div>
	</div>

</div>

<script>
var content = null;
var getUser = function(){
	if($('#content').val()==""){
		content = $('#content').val();	
	}
	//查找陌生人添加好友
	$.ajax({
		url:"getFriendsUser",
		type:"get",
		data:{
			"content":$('#content').val(),
			"userid":${user.userid}
		},
		success:function(data){
			//先移除原来的
			$("#divshow").children('div').remove();
			//在后面添加div
			$.each(data,function(index,data1){
				if(data1.username!='${user.username}'){
					$("#divshow").append('<div class="mdui-chip su-user-list" style="margin:5px">'+'<span class="mdui-chip-icon mdui-text-uppercase">'+'</span>'+'<span class="mdui-chip-title">'+data1.username+'</span>'+'<button class="adddiv btn-default" style="text-align:center;margin:5px" type="button" title='+data1.userid+'>'+'点我分享'+'</button>'+'</div>')
				}
				
			});
			
		}
	});
}
</script>

<%@include file="footer.jsp"%>
