package com.etc.entity;

import java.io.Serializable;

/**
 * 数据库tab_user表对应的实体类
 * 
 * @author zeng
 *
 */
public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// 用户编号
	private Integer userid;
	// 用户名
	private String username;
	// 用户密码
	private String userpwd;
	// 用户手机号码
	private String userphone;
	// 用户状态
	private Integer userstate;
	// 用户等级
	private Integer userlevel;
	// 用户性别
	private String usersex;
	// 用户注册时间
	private String userdatetime;

	public String getUsersex() {
		return usersex;
	}

	public void setUsersex(String usersex) {
		this.usersex = usersex;
	}

	public String getUserdatetime() {
		return userdatetime;
	}

	public void setUserdatetime(String userdatetime) {
		this.userdatetime = userdatetime;
	}

	public User(Integer userid, String userpwd) {
		this.userid = userid;
		this.userpwd = userpwd;
	}

	public User(String username, String userpwd, String userphone, String usersex) {
		super();
		this.username = username;
		this.userpwd = userpwd;
		this.userphone = userphone;
		this.usersex = usersex;
	}

	public User() {
		// TODO Auto-generated constructor stub
	}

	public User(Integer userid, String username, String userpwd, String userphone, Integer userstate, Integer userlevel,
			String usersex, String userdatetime) {
		super();
		this.userid = userid;
		this.username = username;
		this.userpwd = userpwd;
		this.userphone = userphone;
		this.userstate = userstate;
		this.userlevel = userlevel;
		this.usersex = usersex;
		this.userdatetime = userdatetime;
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username == null ? null : username.trim();
	}

	public String getUserpwd() {
		return userpwd;
	}

	public void setUserpwd(String userpwd) {
		this.userpwd = userpwd == null ? null : userpwd.trim();
	}

	public String getUserphone() {
		return userphone;
	}

	public void setUserphone(String userphone) {
		this.userphone = userphone == null ? null : userphone.trim();
	}

	public Integer getUserstate() {
		return userstate;
	}

	public void setUserstate(Integer userstate) {
		this.userstate = userstate;
	}

	public Integer getUserlevel() {
		return userlevel;
	}

	public void setUserlevel(Integer userlevel) {
		this.userlevel = userlevel;
	}

	@Override
	public String toString() {
		return "User [userid=" + userid + ", username=" + username + ", userpwd=" + userpwd + ", userphone=" + userphone
				+ ", userstate=" + userstate + ", userlevel=" + userlevel + ", usersex=" + usersex + ", userdatetime="
				+ userdatetime + "]";
	}

}