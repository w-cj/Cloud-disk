package com.etc.entity;

import java.io.Serializable;

/**
 * 统计文件类型数量的实体类
 * 
 * @author Administrator
 *
 */
public class FileCount implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// 文件的类型
	private String type;
	// 每种类型的数量
	private Integer count;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "FileCount [type=" + type + ", count=" + count + "]";
	}

	public FileCount(String type, Integer count) {
		super();
		this.type = type;
		this.count = count;
	}

	public FileCount() {
		// TODO Auto-generated constructor stub
	}
}
