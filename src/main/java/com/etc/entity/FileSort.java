package com.etc.entity;

import java.io.Serializable;

/**
 * 文件类
 * 
 * @author Administrator
 *
 */
public class FileSort implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// 文件的编号
	private Integer fileid;
	// 文件的名字
	private String filename;
	// 文件的地址
	private String filepath;
	// 文件的类型
	private Integer filetype;
	// 文件的上传时间
	private String filetime;
	// 文件的大小
	private String filesize;
	// 上传的文件人的名字
	private String username;

	public Integer getFileid() {
		return fileid;
	}

	public void setFileid(Integer fileid) {
		this.fileid = fileid;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename == null ? null : filename.trim();
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath == null ? null : filepath.trim();
	}

	public Integer getFiletype() {
		return filetype;
	}

	public void setFiletype(Integer filetype) {
		this.filetype = filetype;
	}

	public String getFiletime() {
		return filetime;
	}

	public void setFiletime(String filetime) {
		this.filetime = filetime;
	}

	public String getFilesize() {
		return filesize;
	}

	public void setFilesize(String filesize) {
		this.filesize = filesize == null ? null : filesize.trim();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username == null ? null : username.trim();
	}

	@Override
	public String toString() {
		return "FileSort [fileid=" + fileid + ", filename=" + filename + ", filepath=" + filepath + ", filetype="
				+ filetype + ", filetime=" + filetime + ", filesize=" + filesize + ", username=" + username + "]";
	}

	public FileSort(Integer fileid, String filename, String filepath, Integer filetype, String filetime,
			String filesize, String username) {
		super();
		this.fileid = fileid;
		this.filename = filename;
		this.filepath = filepath;
		this.filetype = filetype;
		this.filetime = filetime;
		this.filesize = filesize;
		this.username = username;
	}

	public FileSort() {
		// TODO Auto-generated constructor stub
	}

}