package com.etc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etc.dao.FileMapper;
import com.etc.entity.Files;
import com.etc.service.FileService;
import com.etc.util.PageData;

/**
 * 文件的service层实现类
 * 
 * @author zeng
 *
 */
@Service
public class FileServiceImpl implements FileService {

	/**
	 * 注入FileMapper
	 */
	@Autowired
	private FileMapper fileMapper;

	/**
	 * 根据id查询文件
	 * 
	 * @param fileid
	 * @return 文件对象
	 */
	public Files getFileById(Integer fileId) {

		return fileMapper.selectByPrimaryKey(fileId);

	}

	/**
	 * 实现文件的显示和分页
	 * 
	 * @param page
	 *            第几页
	 * @param limit
	 *            显示数据条数
	 * @param content
	 *            模糊查询
	 * @return PageData工具类
	 */
	@Override
	public PageData<Files> getFileByPage(int page, int limit, String content) {
		// 得到起始位置
		int start = (page - 1) * limit;
		// 得到记录总条数
		int count = fileMapper.getCount(content);
		// 得到分页和模糊查询后的数据
		List<Files> list = fileMapper.getFileByBage(start, limit, content);
		PageData<Files> pg = new PageData<Files>(list, count, limit, page, content);
		return pg;
	}

	/**
	 * 根据上一级目录的id获取该目录所拥有的文件
	 * 
	 * @param secondId
	 *            上一级目录id
	 * @return 文件列表
	 */
	@Override
	public List<Files> getFileBySecondId(Integer secondId, Integer userId) {

		return fileMapper.getFileBySecondId(secondId, userId);
	}

	/**
	 * 重命名文件
	 * 
	 * @param oldname
	 * @param newfilename
	 * @param secondId
	 * @return 修改结果
	 */
	@Override
	public boolean reName(String oldName, String newFileName, Integer secondId) {

		// 通过旧文件名得到旧文件
		Files file = fileMapper.selectBySecondIdAndFileName(secondId, oldName);

		// 修改文件名
		file.setFilename(newFileName);

		// 保存修改后的文件并根据结果返回true or false
		boolean flag = fileMapper.updateByPrimaryKey(file) > 0;

		if (flag) {
			return true;
		}
		return false;
	}

	/**
	 * 删除文件
	 * 
	 * @param secondId
	 * @param filesName
	 * @return
	 */
	@Override
	public boolean deleteFiles(Integer secondId, String filesName) {

		boolean flag = fileMapper.deleteBySecondIdAndFileName(secondId, filesName) > 0;

		return flag;
	}

	/**
	 * 统计所有文件
	 * 
	 * @return 文件数
	 */
	@Override
	public int getCount() {
		return fileMapper.getCount("");
	}

	/**
	 * 根据用户id和文件类型查找所有文件
	 * 
	 * @param userId
	 *            用户id
	 * @param fileType
	 *            文件类型
	 * @return 文件集合
	 */
	@Override
	public List<Files> selectByUserIdAndFileType(Integer userId, Integer fileType) {
		// TODO Auto-generated method stub
		return fileMapper.selectByUserIdAndFileType(userId, fileType);
	}

	/**
	 * 查询单个文件的方法
	 * 
	 * @param secondId
	 *            文件的父目录id
	 * @param fileName
	 *            文件的路径
	 * @return 文件对象
	 */
	@Override
	public Files selectByFnameAndSid(Integer secondId, String fileName) {
		// TODO Auto-generated method stub
		return fileMapper.selectBySecondIdAndFileName(secondId, fileName);
	}

	/**
	 * 添加单个文件
	 * 
	 * @param file
	 *            文件对象
	 * @return 受影响行数的主键值
	 */
	@Override
	public int addFile(Files file) {
		// TODO Auto-generated method stub
		return fileMapper.insertSelective(file);
	}

	/**
	 * 根据上一级目录ID和文件名模糊查找
	 * 
	 * @param secondId
	 * @return 文件集合
	 */
	@Override
	public List<Files> getFileByLike(Integer userId, String content) {
		// TODO Auto-generated method stub
		return fileMapper.getFileByLike(userId, content);
	}

	/**
	 * 查询单个文件的方法
	 * 
	 * @param secondId
	 *            文件的父目录id
	 * @param fileName
	 *            文件的路径
	 * @return 文件对象
	 */
	@Override
	public Files selectByFnameAndSidAndUid(Integer secondId, String fileName, Integer userId) {
		// TODO Auto-generated method stub
		return fileMapper.selectBySecondIdAndFileNameAndUserId(secondId, fileName, userId);
	}

}
