$(function() {
	/*
	 * ChartJS ------- Data and config for chartjs
	 */
	'use strict';
	var type = [];
	var count = [];
	$.ajax({

		type : "get",
		url : "filelist",
		dataType : "json",
		success : function(data) {
			
			 $.each(data, function(index, obj) {
				 type.push(obj.type);
				count.push(obj.count);
			});

			var comit = {
				labels : type,
				datasets : [ {
					label : '/个',
					data : count,
					backgroundColor : [ 'rgba(255, 99, 132, 0.2)' ],
					borderColor : [ 'rgba(255,99,132,1)' ],
					borderWidth : 1
				} ]
			};

			var options = {
				scales : {
					yAxes : [ {
						ticks : {
							beginAtZero : true
						}
					} ]
				},
				legend : {
					display : false
				},
				elements : {
					point : {
						radius : 0
					}
				}

			};

			if ($("#lineChart").length) {
				var lineChartCanvas = $("#lineChart").get(0).getContext("2d");
				var lineChart = new Chart(lineChartCanvas, {
					type : 'line',
					data : comit,
					options : options
				});
			}
		}
	});
});
