package com.etc.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.etc.entity.Contents;
import com.etc.entity.Files;
import com.etc.service.ContentsService;
import com.etc.service.FileService;

/**
 * 删除的工具类
 * 
 * @author Administrator
 *
 */
@Component
public class DeleteUtil {

	@Autowired
	private FileService fileService;

	@Autowired
	private ContentsService contentsService;

	public boolean deleteContents(Integer secondId, Integer userId) {

		boolean flag = false;
		// 得到要删除目录下的所有目录
		List<Contents> listContents = contentsService.getContentsBySecondId(secondId, userId);
		// 得到要删除目录下的所有文件
		List<Files> listFile = fileService.getFileBySecondId(secondId, userId);
		// 如果有则删除
		if (listContents.size() > 0) {
			for (int i = 0; i < listContents.size(); i++) {
				flag = contentsService.deleteContents(secondId, listContents.get(i).getContentsname());
				if (flag) {
					deleteContents(listContents.get(i).getContentsid(), userId);
				} else {
					return flag;
				}
			}
		}
		if (listFile.size() > 0) {
			for (int i = 0; i < listFile.size(); i++) {
				flag = fileService.deleteFiles(secondId, listFile.get(i).getFilename());
				return flag;
			}
		}
		return flag;
	}
}
