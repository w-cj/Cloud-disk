package com.etc.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.etc.entity.User;
/**
 * 用户和管理员的管理
 * @author China
 *20192019年5月24日上午9:33:34
 */
@Repository
@Mapper
public interface UserMapper {
	//根据用户Id删除用户
    int deleteByPrimaryKey(@Param("userid") Integer userid);
    //增加用户
    int insert(User record);
    //
    int insertSelective(User record);
    //根据用户Id查找用户
    User selectByPrimaryKey(Integer userid);
    //
    int updateByPrimaryKeySelective(User record);
    //根据用户Id修改用户
    int updateByPrimaryKey(User record);
    
    
    /**
     * 分页查询用户
     * @param page  当前页
     * @param limit  每页的页数
     * @param content  模糊查询的内容
     * @return  用户的一个集合
     */
 	List<User> selectAllUserByPage(@Param(value = "page") Integer page, @Param(value = "limit") Integer limit,
 			                        @Param(value = "content") String content);
    
    
    /**
     * 模糊查询总用户的数目
     * @param content  模糊查询的内容
     * @return 模糊查询用户的总数目
     */
 	int getCount(String content);
 	

 	/**
 	 * 根据用户名查询用户信息
 	 * @param username 用户名
 	 * @return 用户对象
 	 */
 	User selectByUserName(String username);

 	 /**
     * 分页查询管理员
     * @param page  当前页的起始记录位置
     * @param limit  每页的页数
     * @param content  模糊查询的内容
     * @return   管理员的集合
     */
 	List<User> selectAllAdminByPage(@Param(value = "page") Integer page, @Param(value = "limit") Integer limit,
 			                        @Param(value = "content") String content);

 	

 	/**
 	 * 根据用户手机号码查询用户信息
 	 * @param userphone 用户手机号
 	 * @return  用户
 	 */
 	User selectByUserPhone(String userphone);

    /**
     * 模糊查询总用户的数目
     * @param content  模糊查询的内容
     * @return  模糊查询管理员的总数目
     */
 	int getAdminCount(String content);

 	/**
 	 * 登录的判断
 	 * @param username
 	 * @return 用户
 	 */
	User selectUserByName(String username);
 	
	/**
 	 * 根据模糊查询查找用户
 	 * @param content 模糊查询的内容
 	 * @return 返回一个用户集合
 	 */
 	List<User> selectByNameOrPhone(@Param("content")String content,@Param("userid")Integer userid);
 	
 	/**
 	 * 统计总用户数
 	 * @return  总共的用户数
 	 */
    int selectCountUser() ;

}