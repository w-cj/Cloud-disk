﻿<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="zh-CN" />
<meta name="renderer" content="webkit" />
<meta name="viewport"
	content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no" />

<title>手机号登录 | Linkresources</title>

<link href="./static/template/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="./static/login/css/style.css" rel="stylesheet">
<link href="./static/common/image/icon.ico" rel="shortcut icon">
<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
    <script
            src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
    <!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css"
          rel="stylesheet">
          
           <style>
        #msg {
            width: 100%;
            line-height: 40px;
            font-size: 14px;
            text-align: center;
        }

        .reg-login-header {
            height: 90px;
            /*background-color: #00F7DE;*/
        }

        .logobox {
            height: 49px;
        }

        .form-horizontal .has-feedback .form-control-feedback {
            right: 0px;
            /*top: 21px;*/
        }

        .input-box-width {
            width: 300px;
            margin-bottom: 5px;
        }

        .notify-str {
            font-weight: 500;
            margin-bottom: 0px;
            font-size: 14px;
        }

        #loginpwd {
            display: inline-block;
        }
    </style> 
<!--[if lt IE 9]>
        <div id='Not_' class="show-ui" style="height: 40px;background-color: rgb(221,79,67);text-align:center;line-height: 40px;color: white;">
            <b>无法访问! 您的浏览器版本过低或是兼容模式,请使用最新/更高版本的浏览器: IE10+ chrome FireFox 等，如果是国内浏览器请打开极速浏览模式（webkit内核）。</b>
        </div>
    <![endif]-->
</head>

<body>

	<div class="mvideocon" style="">
        <video class="mvideo" type="video/mp4" preload="metadata" loop="" autoplay="" muted="" poster="https://web.ccpgamescdn.com/eveonlineassets/bgvid/video-bg-image.jpg">
            <source src="static/bg.mp4" autostart="false">
        </video>
    </div>
	<div id='login-view' class="show-ui">
		<div id='login-sub'>
			<div class="container-fluid">
				<div class="row">
					<img id='logo' src="static/common/logo.png"> <br>
				</div>
				<div id='login-form' class="row">
					<!-- 表单开始 -->
					<form class="form-horizontal" role="form" id="Action_home" name="login_form" action="" target="_parent" method="post">
					
						<!--手机号校验提示 -->
						<div class="group" id="phonecheck" style="color:red;opacity:0;">
							该手机号未注册！！！
						</div>
						<!--手机号校验提示结束 -->
						<div class="form-group has-feedback">
							<div class="col-sm-12" style="padding-right: 1px">
								<input type="text" class="form-control border-1 minput"
									id="userphone" name="userphone" placeholder="手机号码" autocomplete="off" style="border: ;"> <span
									class="glyphicon glyphicon-user form-control-feedback"
									aria-hidden="true"></span>
							</div>
						</div>
						
						<div class="form-group has-feedback input-box-width" id="code-error" style="position:relative;left:17px;top:-22px">
                    <label id="pwd-exist" class="control-label notify-str" for="loginpwd"
                           style="opacity: 0">验证码有误</label>
                    <div>
                        <input type="text" class="form-control" name="password" id="loginpwd"
                               aria-describedby="inputSuccess2Status" placeholder="请输入验证码"
                               style="width: 150px"/>
                        <button class="btn btn-success" id="code-btn" name="btn"
                                style="width: 135px;float: right" disabled="disabled">发送验证码
                        </button>
                    </div>
                    <span id="pwd-check" class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    <span id="inputSuccess2Status" class="sr-only">(success)</span>
                </div>
				
				</div>	
						
						<div class="form-group">
						 	<div class="col-sm-6" >
								<a href="login.jsp">
									<div id='login-button' class="btn btn-danger btn-info border-1" style="width: 135px;">
										<b>账号登录</b>
									</div>
								</a>
							</div> 
							
							<div class="col-sm-6">
								<input id='register-button' class="btn btn-success " style="width: 135px;" disabled="disabled" value="登录" />
									
								</div>
							</div>
						</div>
					</form>
					<!-- 表单结束 -->
				</div>
				
			</div>
		</div>
	</div>
	<div id='view-info' class="alert alert-info show-ui tools-info">默认信息框</div>
</body>
<script type="text/javascript" src="./static/template/jq/jq.min.js"></script>


<!--前台手机号登录框失焦事件 -->
<script type="text/javascript">
$(function () {
	var InterValObj; //timer变量，控制时间
    var count = 60; //间隔函数，1秒执行
    var curCount = 0;//当前剩余秒数
    var sign=1;
	$("#userphone").blur(function () {
		$.ajax({
			url:"../checkUserByPhone",
			type:"GET",
			data:{
				"userphone":$("#userphone").val()
			},
			success:function(data){
				if(data.msg=="该手机号未注册"){
					//显示警告信息
					$("#phonecheck").css("opacity",1);
					//清空表单内容
					$("#userphone").val("");
					//禁用按钮
					$("#code-btn").attr("disabled",'false');
				}else if (data.msg=="该手机号不合规"){
					layer.msg(data.msg, {
						icon : 1,
						time : 2000
					});
					//$("#userphone").val("该手机号格式不符合规定");
					$("#userphone")[0].style.border='1px solid red';
					//清空表单内容
					$("#userphone").val("");
					//禁用按钮
					$("#code-btn").attr("disabled","false");
				}else if(curCount==0 && sign==1){
					$("#phonecheck").css("opacity",0);
					//启用按钮
					$("#code-btn").removeAttr("disabled");
					$("#userphone")[0].style.border='';
				}else{
					$("#phonecheck").css("opacity",0);
					//启用按钮
					$("#code-btn").attr("disabled","false");
					$("#userphone")[0].style.border='';
				}
			}
			
		})
		
	})	
	
        $('#code-btn').click(function () {
            curCount = count;
            $("#code-btn").attr("disabled", "true");
            $("#code-btn").text(curCount + "秒后可重新发送");
            InterValObj = window.setInterval(SetRemainTime, 1000); //启动计时器，1秒执行一次请求后台发送验证码
        });
    // timer处理函数
    function SetRemainTime() {
        if (curCount == 0) {
            window.clearInterval(InterValObj);//停止计时器
            $("#code-btn").removeAttr("disabled");//启用按钮
            $("#code-btn").text("发送验证码");
        } else {
            curCount--;
            $("#code-btn").text(curCount + "秒后可重新发送");
        }
    }
 })

 var check = function(){
	 
	 if(($("#userphone").val()!="")&&($("#loginpwd").val()!="")){
		//启用按钮
    	$("#register-button").removeAttr("disabled");
    }else{
    	
    	$("#register-button").attr("disabled", "false");
    }
 }
 
 setInterval("check()",1000)

</script>
<!--前台登录发送验证码事件-->
<script src="${pageContext.request.contextPath}/layer/2.1/layer.js"></script>
<script type="text/javascript">
	$(function () {
		<!--发送短信-->
		 $("#code-btn").click(function () {
			 $.ajax({
					url:"../send",
					type:"GET",
					data:{
						"userphone":$("#userphone").val()
						//"password":$("#loginpwd").val()
					},
				success:function(data){
					if(data.msg=="发送成功"){
						layer.msg(data.msg, {
							icon : 1,
							time : 2000
						});
					  }else{
						  layer.msg("发送失败", {
								icon : 1,
								time : 2000
							});
						}
				
					}
				}) 
		}) 
		
	})

</script>


<!--前台验证码登录功能 -->
<script type="text/javascript">

	$(function(){
		$("#register-button").click(function () {
			$.ajax({
				url:"../phonelogin",
				type:"GET",
				data:{
					"userphone":$("#userphone").val(),
					"password":$("#loginpwd").val()
				},
			success:function(data){
				if(data.msg=="登陆成功"){
					location.href="../toSystem";
				  }else if(data.msg=="验证码有误"){
					  $("#pwd-exist").css("opacity",1);
					}else{
						layer.msg(data.msg, {
							icon : 1,
							time : 2000
						});
						
					}
				
				}
			})
			
		});
	})

</script>
<!--前台验证码登录功能 -->
</html>