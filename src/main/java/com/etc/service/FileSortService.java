package com.etc.service;

import com.etc.entity.FileSort;
import com.etc.util.PageData;

/**
 * 分页查询每种文件类型的service接口
 * 
 * @author Administrator
 *
 */
public interface FileSortService {

	/**
	 * 分页查询图片
	 * 
	 * @param page
	 *            当前页
	 * @param limit
	 *            每页的页数
	 * @param content
	 *            模糊查询的内容
	 * @return PageData的对象
	 */
	public PageData<FileSort> selectPictureBypage(int page, int limit, String content);

	/**
	 * 分页查询音乐
	 * 
	 * @param page
	 *            当前页
	 * @param limit
	 *            每页的页数
	 * @param content
	 *            模糊查询的内容
	 * @return PageData的对象
	 */
	public PageData<FileSort> selectMusicBypage(int page, int limit, String content);

	/**
	 * 分页查询视频
	 * 
	 * @param page
	 *            当前页
	 * @param limit
	 *            每页的页数
	 * @param content
	 *            模糊查询的内容
	 * @return PageData的对象
	 */
	public PageData<FileSort> selectMovieBypage(int page, int limit, String content);

	/**
	 * 分页查询文档
	 * 
	 * @param page
	 *            当前页
	 * @param limit
	 *            每页的页数
	 * @param content
	 *            模糊查询的内容
	 * @return PageData的对象
	 */
	public PageData<FileSort> selectTmpBypage(int page, int limit, String content);

}
