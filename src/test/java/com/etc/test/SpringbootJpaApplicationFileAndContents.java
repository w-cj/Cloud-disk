package com.etc.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.etc.dao.ContentsMapper;
import com.etc.dao.FileMapper;
import com.etc.entity.Contents;
import com.etc.entity.Files;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootJpaApplicationFileAndContents {

	@Autowired
	ContentsMapper cm;

	@Autowired
	FileMapper fileMapper;

	@Test
	public void contextLoads() {
		List<Files> list = fileMapper.selectByUserIdAndFileType(1, 3);
		System.out.println(list);
	}

	@Test
	public void contextLoads01() {
		Contents contents = cm.selectByPrimaryKey(18);
		contents.setContentsid(null);
		int key = cm.insertSelectiveGetKey(contents);
		
		System.out.println(contents.getContentsid());
	}

}
