package com.etc.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.etc.dao.ContentsMapper;
import com.etc.dao.FileMapper;
import com.etc.dao.FriendsMapper;
import com.etc.entity.Contents;
import com.etc.entity.Friends;
import com.etc.entity.User;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootJpaApplicationFriendsDaoTest {

	@Autowired
	FriendsMapper friendsMapper;
	
	@Autowired
	FileMapper fileMapper ;

	@Test
	public void addFriendsReq() {

		// 创建一个好友对象
		Friends friends = new Friends();

		friends.setFromuserid(5);

		friends.setTouserid(6);

		friends.setRelationstate(2);

		// 调用添加好友请求的方法
		friendsMapper.addFriendsReq(friends);

	}

	@Test
	public void handleFriendsReq() {

		// 创建一个好友对象
		Friends friends = new Friends();

		friends.setFromuserid(5);

		friends.setTouserid(6);

		friends.setRelationstate(1);

		Friends friends2 = new Friends();

		friends2.setFromuserid(6);

		friends2.setTouserid(5);

		friends2.setRelationstate(1);

		// 调用添加好友请求的方法
		friendsMapper.handleFriendsReq(friends);
		friendsMapper.insertSelective(friends2);
	}

	@Test
	public void getFriendsReq() {

		List<User> list = friendsMapper.getFriendsReq(2);
		System.out.println(list);

	}

	@Test
	public void getFriendsUser() {

		List<User> list = friendsMapper.getFriendsUser(5);
		System.out.println(list);

	}

	@Test
	public void delFriendsOrReq() {

		// 创建一个好友对象
		Friends friends = new Friends();

		friends.setFromuserid(1);

		friends.setTouserid(2);

		friendsMapper.delFriendsOrReq(friends);
	}
	
	@Test
	public void testFileMapper() {
		int i = fileMapper.getCount("") ;
		System.out.println(i);
	}

}
