package com.etc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.etc.entity.UserRegisterCount;
import com.etc.service.UserRCService;

@RestController
public class UserRCController {
	
	@Autowired
	private UserRCService   userRCService;
	
	
	@GetMapping(value="getUsercount")
	public List<UserRegisterCount> getUserCount(){
			
		List<UserRegisterCount>  list =userRCService.getUserCount();
		return list;
		
	}
	
	
	

}
