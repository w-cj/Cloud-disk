package com.etc.entity;

import java.io.Serializable;

/**
 * 
 * @author China 20192019年5月24日上午9:09:08
 */
public class Files implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// 目录Id
	private Integer fileid;
	// 目录名（文件所在的文件夹名）
	private String filename;
	// 文件储存路径
	private String filepath;
	// 文件所在的文件夹名
	private Integer secondid;
	// 文件上传的时间
	private String filetime;
	// 文件上传的类型，4为图片,1为音乐，2为视频，3为文档，4为图片
	private Integer filetype;
	// 用户Id
	private Integer userid;
	// 文件上传的大小
	private String filesize;

	public String getFilesize() {
		return filesize;
	}

	public void setFilesize(String filesize) {
		this.filesize = filesize;
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public Integer getFileid() {
		return fileid;
	}

	public void setFileid(Integer fileid) {
		this.fileid = fileid;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename == null ? null : filename.trim();
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath == null ? null : filepath.trim();
	}

	public Integer getSecondid() {
		return secondid;
	}

	public void setSecondid(Integer secondid) {
		this.secondid = secondid;
	}

	public String getFiletime() {
		return filetime;
	}

	public void setFiletime(String filetime) {
		this.filetime = filetime;
	}

	public Integer getFiletype() {
		return filetype;
	}

	public void setFiletype(Integer filetype) {
		this.filetype = filetype;
	}

	@Override
	public String toString() {
		return "File [fileid=" + fileid + ", filename=" + filename + ", filepath=" + filepath + ", secondid=" + secondid
				+ ", filetime=" + filetime + ", filetype=" + filetype + ", userid=" + userid + ", filesize=" + filesize
				+ "]";
	}

	public Files(Integer fileid, String filename, String filepath, Integer secondid, String filetime, Integer filetype,
			Integer userid, String filesize) {
		super();
		this.fileid = fileid;
		this.filename = filename;
		this.filepath = filepath;
		this.secondid = secondid;
		this.filetime = filetime;
		this.filetype = filetype;
		this.userid = userid;
		this.filesize = filesize;
	}

	public Files() {
		// TODO Auto-generated constructor stub
	}
}