<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
<!-- Required meta tags -->
<script src="js/jquery-3.3.1.js"></script>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>管理员列表</title>
<!-- plugins:css -->
<link rel="stylesheet"
	href="node_modules/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet"
	href="node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css">

<link rel="stylesheet"
	href="node_modules/jquery-bar-rating/dist/themes/css-stars.css">
<link rel="stylesheet"
	href="node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" />

<link rel="stylesheet" href="css/style.css">
<!-- endinject -->
<link rel="shortcut icon" href="images/favicon.png" />

</head>

<body>

	<div class="container-scroller">
		<!-- partial:partials/_navbar.html -->
		<%@ include file="head.jsp"%>
		<!-- partial -->
		<div class="container-fluid page-body-wrapper">
			<div class="row row-offcanvas row-offcanvas-right">
				<%@ include file="left.jsp"%>
				<div class="content-wrapper">
					<table class="layui-hide" id="test" lay-filter="test"></table>
				</div>
			</div>
			<!-- row-offcanvas ends -->
		</div>
		<!-- page-body-wrapper ends -->
	</div>
	<!-- container-scroller -->

	<script type="text/html" id="toolbarDemo">
    <div class="layui-btn-container demoTable">
		<button class="layui-btn layui-btn-sm but002" data-type="reload">搜索</button>
		<div class="layui-inline">
			<input class="layui-text" name="id" id="demoReload"
				autocomplete="off">
		</div>

  </div>
</script>

	<script>
		layui.use('table', function() {
			var table = layui.table;

			table.render({
				elem : '#test',
				url : 'listAdmin',
				toolbar : '#toolbarDemo',
				title : '管理员数据表',
				cols : [ [ {
					field : 'userid',
					title : '编号',
					fixed : 'left',

					unresize : true,
					sort : true
				}, {
					field : 'username',
					title : '用户名'

				}, {
					field : 'usersex',
					title : '性别'

				}, {
					field : 'userphone',
					title : '电话'

				}, {
					field : 'userdatetime',
					title : '注册时间'
				}, {
					field : 'userlevel',
					title : '等级',
					sort : true,

					templet : function(res) {
						switch (res.userlevel) {
						case 0:
							return "管理员"
						case 1:
							return "普通用户"

						}
						//return res.userlevel == 0 ? "普通用户" : "管理员"
					}
				}, {
					field : 'userstate',
					title : '状态',
					sort : true,
					width : 80,
					templet : function(res) {
						return res.userstate == 0 ? "已注销" : "正常"
					}
				} ] ],
				page : true,
				id : 'testReload'
			});

			
			var $ = layui.$, active = {
				reload : function() {
					var demoReload = $('#demoReload');

					//执行重载
					table.reload('testReload', {
						page : {
							curr : 1
						//重新从第 1 页开始
						},
						where : {
							content : demoReload.val()
						}
					});
				}
			};

			$('.but002').on('click', function() {

				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
	
		});
	</script>

	<!-- plugins:js -->
	<script src="node_modules/jquery/dist/jquery.min.js"></script>
	<script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
	<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
	<script
		src="node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js"></script>

	<script src="node_modules/chart.js/dist/Chart.min.js"></script>

	<script src="js/off-canvas.js"></script>
	<script src="js/misc.js"></script>

	<script src="js/dashboard.js"></script>

</body>

</html>