package com.etc.service;

import java.util.List;

import com.etc.entity.Friends;
import com.etc.entity.User;
/**
 * 添加好友，删除好友，发送好友请求的业务接口
 * @author 黎嘉华
 *
 */
public interface FriendService {
	
	/**
	 * 发送好友申请
	 * @param friends
	 * @return 返回是否成功
	 */
	public boolean sendFriendsReq(Friends friends);
	
	/**
	 * 成功添加好友
	 * @param friends 好友对象
	 * @return 返回是否添加成功
	 */
	public boolean addFriend(Friends friends);
	
	/**
	 * 拒绝添加好友
	 * @param recond 好友对象
	 * @return
	 */
	public boolean delFriends(Friends recond);
	
	/**
	 * 删除好友
	 * @param recond 好友对象
	 * @return
	 */
	public boolean delFriendsReq(Friends recond);
	
	/**
     * 查找好友请求
     * @param userid userid 用户id
     * @return 返回所有的好友的请求集合
     */
	public List<User> getFriendsReq(Integer userId);
    
    /**
     * 查找所有的关系好友
     * @param userid 用户id
     * @return 返回一个好友的集合
     */ 
    public List<User> getFriendsUser(Integer userId ,String content);
    
    /**
     * 判断好友请求是否存在
     * @param fromuserid 发送请求者
     * @param touserid 被发送的请求者
     * @return
     */
    public List<Friends> getFriendByReq(Integer fromuserId,Integer touserId);
}
