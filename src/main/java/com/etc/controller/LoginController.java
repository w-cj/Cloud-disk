package com.etc.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.etc.entity.User;
import com.etc.entity.UserPwd;
import com.etc.service.FileService;
import com.etc.service.ShareService;
import com.etc.service.UserService;
import com.etc.util.CommenMassage;

/**
 * 
 * @author China 2019年5月24日上午10:26:01
 */
@Controller
public class LoginController {

	@Autowired
	private UserService userService;

	@Autowired
	private FileService fileService;

	@Autowired
	private CommenMassage commenMassage;
	
	@Autowired
	private ShareService shareService ;

	/**
	 * 登录验证用户名是否存在
	 * 
	 * @param username
	 *            输入的用户名
	 * @return Commenmassage 对象
	 */
	@GetMapping("loginVilidate")
	@ResponseBody
	public CommenMassage Userlogin(String username) {
		User user = userService.selectUserByName(username);
		String msg = "该管理员不存在";
		if (user != null) {
			msg = "该管理员存在";
		}
		commenMassage.setMsg(msg);
		return commenMassage;
	}

	/**
	 * 后台登录
	 * 
	 * @param username
	 *            输入的用户名
	 * @param userpwd
	 *            输入的密码
	 * @param session
	 *            用来存储数据
	 * @return CommenMassage的对象
	 */
	@PostMapping("login")
	@ResponseBody
	public CommenMassage backUserLogin(String username, String userpwd, HttpSession session) {
		User user = userService.selectUserByName(username);
		int countUser = userService.selectCountUser();
		int countFile = fileService.getCount();
		int countShare = shareService.selectShare() ;
		session.setAttribute("countUser", countUser);
		session.setAttribute("countFile", countFile);
		session.setAttribute("countShare", countShare);
		String msg = "密码错误";
		if (user != null) {
			if (user.getUserpwd().equals(userpwd)) {
				session.setAttribute("admin", user);
				msg = "登录成功";
			}
		}
		commenMassage.setMsg(msg);
		return commenMassage;
	}

	/**
	 * 后台退出登录
	 * 
	 * @param session 清楚存储在session里面的信息
	 * @return  String类型--页面
	 */
	@GetMapping("backdoLogout")
	public String backdoLoginOut(HttpSession session) {

		session.invalidate();

		return "login";

	}

	/**
	 * 修改密码的验证
	 * 
	 * @param session 取登录的管理员的密码与用户输入的对比
	 * @param userpwd 管理员三次输入的密码
	 * @return  CommenMassage对象--提示信息
	 */
	@PutMapping("backupdatePwd")
	@ResponseBody
	public CommenMassage updatePwd(HttpSession session, @RequestBody UserPwd userpwd) {
		String pwd = userpwd.getUserPwd();
		String pwd1 = userpwd.getUserPwd1();
		String pwd2 = userpwd.getUserPwd2();

		String msg = "";
		User u = (User) session.getAttribute("admin");
		User users = new User(u.getUserid(), pwd1);
		if (u.getUserpwd().equals(pwd)) {
			if (pwd.equals(pwd1)) {

				msg = "旧密码不能与新密码一致";

			} else {
				if (pwd1.equals(pwd2)) {

					boolean flag = userService.updateByPrimaryKeySelective(users);
					if (flag) {
						msg = "修改密码成功";
					} else {

						msg = "修改密码失败";
					}

				} else {
					msg = "两次密码输入不一致";
				}
			}
		} else {

			msg = "旧密码输入错误";

		}
		commenMassage.setMsg(msg);
		return commenMassage;
	}

	/**
	 * 前台退出
	 * 
	 * @return 页面跳转
	 */
	@GetMapping("Logout")
	public String Logout(HttpSession session) {
		session.removeAttribute("user");
		return "redirect:login/login.jsp";

	}
	

}
