<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<!-- 文件上传的表单 -->
<form id="m-upload-form" style="display: none;" method="post"
	enctype="multipart/form-data" action="">
	<input type="file" name="upload_file" id="m-upload-file"  accept="application/octet-stream,application/vnd.ms-excel,image/png,image/jpeg,image/gif,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,audio/mp4, video/mp4,audio/mpeg,.pdf,text/plain,application/x-msdownload,video/x-ms-wmv"/>
</form>


<div class="mdui-dialog" id="su_file_uploading">
	<div class="mdui-dialog-title" id="su_file_upload_progress2">上传文件</div>
	<div class="mdui-dialog-content">
		正在异步传输，上传过程中，请不要刷新网页或者进行任何操作 <span ></span>
		<div class="mdui-progress">
			<div id="su_file_upload_progress" class="mdui-progress-determinate" style="width: 1%;"></div>
		</div>
	</div>
	<div class="mdui-dialog-actions"></div>
</div>

<!--文件上传到指定的位置 -->
<script>
function GetQueryString(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
	var r = window.location.search.substr(1).match(reg);
	if (r != null){
		return unescape(r[2]);
	}
	return null;
}
	$("#m-upload-file").on("change", function() {
		var processEle = $("#su-upload-text");
			var formData = new FormData($("#m-upload-form")[0]);
			 if(GetQueryString("secondId")==null){
				formData.append("secondid",0);
			}else if($(".secondid").val()==null){
					formData.append("secondid",GetQueryString("secondId")); 
			}else{
				//点击上传时将当前所在的目录id一起传给控制器
				formData.append("secondid",$(".secondid").val());
			}
			
			$.ajax({
				url:"${pageContext.request.contextPath}/fileupload",
				type:"POST",
				data:formData,
				processData:false,
				contentType:false,
				success:function(data){
					layer.msg(data.msg,
							{
						time : 2000
					//2秒关闭（如果不配置，默认是3秒）
					});
				}
			});
	});

	
</script>
