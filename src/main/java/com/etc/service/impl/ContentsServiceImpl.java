package com.etc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etc.dao.ContentsMapper;
import com.etc.entity.Contents;
import com.etc.service.ContentsService;

/**
 * 目录的service层实现类
 * 
 * @author zeng
 *
 */
@Service
public class ContentsServiceImpl implements ContentsService {

	/**
	 * 注入ContentsMapper
	 */
	@Autowired
	private ContentsMapper contentsMapper;

	/**
	 * 根据上一级目录的id获取该目录所拥有的文件
	 * 
	 * @param secondId
	 *            上一级目录ID
	 * @param userId
	 *            用户ID
	 * @return 所有目录
	 */
	@Override
	public List<Contents> getContentsBySecondId(Integer secondId, Integer userId) {
		return contentsMapper.getContentsBySecondId(secondId, userId);
	}

	/**
	 * 添加目录
	 * 
	 * @param contents
	 *            目录对象
	 * @return 增加结果
	 */
	@Override
	public boolean addContents(Contents contents) {

		// 对传入的路径做处理
		// 获取上一级目录的路径
		String parentName = contentsMapper.getContentsPathBySecondId(contents.getSecondid());

		// 如果上一级目录的路径为空则代表他是最顶级目录否则拼接路径
		if (parentName == null) {

			// 设置目录的路径为空
			contents.setContentspath(null);
		} else {

			// 拼接路径
			String contentsPath = parentName + "\\" + contents.getContentsname();

			// 存储路径
			contents.setContentspath(contentsPath);
		}

		// 调用mapper类的方法
		return contentsMapper.insertSelective(contents) > 0;
	}

	/**
	 * 根据上一级目录的id来查找上一级目录的再上一级目录的id
	 * 
	 * @param secondId
	 *            上一级目录ID
	 * @return 目录ID
	 */
	@Override
	public Integer getSecondIdBySecondId(Integer secondId) {

		return contentsMapper.getSecondIdBySecondId(secondId);
	}

	/**
	 * 重命名
	 * 
	 * @param oldname
	 *            修改前的目录名
	 * @param newfilename
	 *            要修改为的目录名
	 * @param secondId
	 *            上一级目录ID
	 * @return 修改结果
	 */
	@Override
	public boolean reName(String oldName, String newFileName, Integer secondId) {
		// 通过旧目录名得到旧目录
		Contents contents = contentsMapper.selectBySecondIdAndContentsName(secondId, oldName);

		// 修改目录名
		contents.setContentsname(newFileName);

		// 修改目录路径
		String parentName = contentsMapper.getContentsPathBySecondId(secondId);

		// 如果上一级目录的路径为空则代表他是最顶级目录否则拼接路径
		if (parentName == null) {

			// 设置目录的路径为空
			contents.setContentspath(null);
		} else {

			// 拼接路径
			String contentsPath = parentName + "\\" + contents.getContentsname();

			// 存储路径
			contents.setContentspath(contentsPath);
		}

		// 保存修改后的目录并根据结果返回true or false
		boolean flag = contentsMapper.updateByPrimaryKey(contents) > 0;

		if (flag) {
			return true;
		}
		return false;
	}

	/**
	 * 删除目录
	 * 
	 * @param secondId
	 * @param contentsName
	 * @return 删除结果
	 */
	@Override
	public boolean deleteContents(Integer secondId, String contentsName) {

		boolean flag = contentsMapper.deleteBySecondIdAndContentsName(secondId, contentsName) > 0;

		return flag;
	}

	/**
	 * 根据上一级目录获取上一级目录路径
	 * 
	 * @param secondId
	 *            上一级目录id
	 * @return 上一级目录路径
	 */
	@Override
	public String getContentsPathById(Integer secondId) {
		// TODO Auto-generated method stub
		return contentsMapper.getContentsPathBySecondId(secondId);
	}

	/**
	 * 增加目录
	 * 
	 * @param contents
	 *            目录对象
	 * @return 增加结果
	 */
	@Override
	public int insertSelectiveGetKey(Contents contents) {

		// TODO Auto-generated method stub
		return contentsMapper.insertSelectiveGetKey(contents);
	}

	/**
	 * 根据上一级目录id和目录名得到当前目录
	 * 
	 * @param secondId
	 *            上一级目录ID
	 * @param contentsName
	 *            目录名
	 * @return 目录对象
	 */
	@Override
	public Contents selectBySecondIdAndContentsName(Integer secondId, String contentsName) {
		// TODO Auto-generated method stub
		return contentsMapper.selectBySecondIdAndContentsName(secondId, contentsName);
	}

	/**
	 * 根据目录ID查询目录
	 * 
	 * @param contentsid
	 *            目录ID
	 * @return 目录
	 */
	@Override
	public Contents selectByPrimaryKey(Integer contentsId) {
		// TODO Auto-generated method stub
		return contentsMapper.selectByPrimaryKey(contentsId);
	}

	/**
	 * 根据上一级目录的id和模糊查询名字获取该目录所拥有的目录
	 * 
	 * @param userId
	 *            用户ID
	 * @param content
	 *            模糊查询的条件
	 * @return 目录对象的集合
	 */
	@Override
	public List<Contents> getContentsByLike(Integer userId, String content) {
		// TODO Auto-generated method stub
		return contentsMapper.getContentsByLike(userId, content);
	}

}
