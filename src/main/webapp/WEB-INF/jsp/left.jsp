<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


	<nav class="sidebar sidebar-offcanvas" id="sidebar">
		<ul class="nav">
			<li class="nav-item"><a class="nav-link" href="index">
					<span class="menu-title">主页</span> <i
					class="mdi mdi-home menu-icon"></i>
			</a></li>
			
			
			
			<li class="nav-item"><a class="nav-link"
				href="userlist"> <span class="menu-title">用户管理</span>
					<i class="mdi mdi-chart-bar menu-icon"></i>
			</a></li>
			
			
			
			<li class="nav-item"><a class="nav-link"
				href="adminlist"> <span class="menu-title">管理员管理</span>
					<i class="mdi mdi-chart-bar menu-icon"></i>
			</a></li>
		
			<li class="nav-item"><a class="nav-link" data-toggle="collapse"
				href="#ui-basic2" aria-expanded="false" aria-controls="ui-basic">
					<span class="menu-title">文件管理</span> <i class="menu-arrow"></i> <i
					class="mdi mdi-crosshairs-gps menu-icon"></i>
			</a>
				<div class="collapse" id="ui-basic2">
					<ul class="nav flex-column sub-menu">
						<li class="nav-item"><a class="nav-link"
							href="picture">图片列表</a></li>
						<li class="nav-item"><a class="nav-link"
							href="music">音乐列表</a></li>
						<li class="nav-item"><a class="nav-link"
							href="movie">视频列表</a></li>
						<li class="nav-item"><a class="nav-link"
							href="tmp">文档列表</a></li>
						
					</ul>
				</div></li>
			<li class="nav-item"><a class="nav-link"
				href="audit"> <span class="menu-title">审核列表</span>
					<i class="mdi mdi-format-list-bulleted menu-icon"></i>
			</a></li>
			
		</ul>

	</nav>

