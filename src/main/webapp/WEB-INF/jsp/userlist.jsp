<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
<!-- Required meta tags -->
<script src="js/jquery-3.3.1.js"></script>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>用户列表</title>
<!-- plugins:css -->
<link rel="stylesheet"
	href="node_modules/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet"
	href="node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css">

<link rel="stylesheet"
	href="node_modules/jquery-bar-rating/dist/themes/css-stars.css">
<link rel="stylesheet"
	href="node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" />

<link rel="stylesheet" href="css/style.css">
<!-- endinject -->
<link rel="shortcut icon" href="images/favicon.png" />

</head>

<body>
	<!-- 修改用户表单 -->
	<div id="editform" style="display: none">
		<form class="layui-form" action="" lay-filter="example" id="examples">
			<div class="layui-form-item">
				<br> <label class="layui-form-label">编号</label>
				<div class="layui-input-block">
					<input type="text" name="userid" id="userid" lay-verify="title"
						autocomplete="off" class="layui-input" readonly>
				</div>
			</div>

			<div class="layui-form-item">
				<br> <label class="layui-form-label">用户名</label>
				<div class="layui-input-block">
					<input type="text" name="username" id="username" lay-verify="title"
						autocomplete="off" class="layui-input">
				</div>
			</div>

			<div class="layui-form-item">
				<label class="layui-form-label">性别</label>
				<div class="layui-input-block">
					<input type="radio" name="usersex" class="sex" id="sexmen"
						value="男" title="男"> <input type="radio" name="usersex"
						class="sex" id="sexwomen" value="女" title="女">
				</div>
			</div>

			<div class="layui-form-item">
				<label class="layui-form-label">电话</label>
				<div class="layui-input-block">
					<input type="text" name="userphone" id="userphone"
						lay-verify="title" autocomplete="off" class="layui-input">
				</div>
			</div>


			<div class="layui-form-item">
				<div class="layui-input-block" align="center">
					<button class="layui-btn btn-update" type="button" lay-submit=""
						lay-filter="demo1">立即提交</button>
				</div>
			</div>
		</form>
	</div>


	<!-- 增加用户表单 -->
	<div id="editform01" style="display: none">
		<form class="layui-form" action="" lay-filter="example02"
			id="examples02">

			<div class="layui-form-item">
				<br> <label class="layui-form-label">用户名</label>
				<div class="layui-input-block">
					<input type="text" name="username" id="username02"
						lay-verify="title" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">密码</label>
				<div class="layui-input-block">
					<input type="password" name="userpwd" id="userpwd02"
						autocomplete="off" class="layui-input">
				</div>
			</div>

			<div class="layui-form-item">
				<label class="layui-form-label">性别</label>
				<div class="layui-input-block">
					<input type="radio" name="usersex" class="sex" id="sexmen02"
						value="男" title="男"> <input type="radio" name="usersex"
						class="sex" id="sexwomen02" value="女" title="女">
				</div>
			</div>

			<div class="layui-form-item">
				<label class="layui-form-label">电话</label>
				<div class="layui-input-block">
					<input type="text" name="userphone" id="userphone02"
						lay-verify="title" autocomplete="off" class="layui-input">
				</div>
			</div>

			<div class="layui-form-item">
				<div class="layui-input-block" align="center">
					<button class="layui-btn btn-update" type="button" lay-submit=""
						lay-filter="demo02">立即提交</button>
				</div>
			</div>
		</form>
	</div>

	<div class="container-scroller">
		<!-- partial:partials/_navbar.html -->
		<%@ include file="head.jsp"%>
		<!-- partial -->
		<div class="container-fluid page-body-wrapper">
			<div class="row row-offcanvas row-offcanvas-right">
				<%@ include file="left.jsp"%>
				<div class="content-wrapper">
					<table class="layui-hide" id="test" lay-filter="test"></table>
				</div>
			</div>
			<!-- row-offcanvas ends -->
		</div>
		<!-- page-body-wrapper ends -->
	</div>
	<!-- container-scroller -->

	<script type="text/html" id="toolbarDemo">
    <div class="layui-btn-container demoTable">
		<button class="layui-btn layui-btn-sm btn001">增加用户</button>
		<button class="layui-btn layui-btn-sm but002" data-type="reload">搜索</button>
		<div class="layui-inline">
			<input class="layui-text" name="id" id="demoReload"
				autocomplete="off">
		</div>

  </div>
</script>

	<script type="text/html" id="barDemo">
  <btton class="btn btn-primary btn-xs btn btn-danger" lay-event="edit"><i class="mdi mdi-upload"></i>编辑</btton>
  <btton class="btn btn-primary btn-xs btn btn-info"  lay-event="del"><i class="mdi mdi-delete"></i>禁/启</btton>
</script>

	<script>
		layui.use('table', function() {
			var table = layui.table;

			table.render({
				elem : '#test',
				url : 'listUser',
				toolbar : '#toolbarDemo',
				title : '用户数据表',
				cols : [ [  {
					field : 'userid',
					title : '编号',
					fixed : 'left',
					unresize : true,
					sort : true
				}, {
					field : 'username',
					title : '用户名'

				}, {
					field : 'usersex',
					title : '性别'

				}, {
					field : 'userphone',
					title : '电话'

				}, {
					field : 'userdatetime',
					title : '注册时间',
					sort : true
				}, {
					field : 'userlevel',
					title : '等级',
					sort : true,

					templet : function(res) {
						switch (res.userlevel) {
						case 0:
							return "管理员"
						case 1:
							return "普通用户"

						}
						//return res.userlevel == 0 ? "普通用户" : "管理员"
					}
				}, {
					field : 'userstate',
					title : '状态',
					
					width : 80,
					templet : function(res) {
						return res.userstate == 0 ? "已注销" : "正常"
					}
				}, {
					fixed : 'right',
					title : '操作',
					toolbar : '#barDemo',
					width : 180
				} ] ],
				page : true,
				id : 'testReload'
			});

			//监听行工具事件
			table.on('tool(test)', function(obj) {

				var data = obj.data;
				if (obj.event === 'del') {
					layer.confirm('真的要执行该操作么', function(index) {

						$.ajax({
							url : "deleteUser/" + data.userid,
							type : "delete",
							success : function(data, status) {
								layer.msg(data.msg, {
									icon : 1, //图标		
									time : 2000
								//2秒关闭（如果不配置，默认是3秒）
								}, function() {
									if (data.msg == "操作成功") {

										location.reload();
									}
								});
							}
						});

					});

				} else if (obj.event === 'edit') {

					$("#userid").attr("value", data.userid);

					$("#username").attr("value", data.username);
					
					$("#userphone").attr("value", data.userphone);
                 
					if (data.usersex=="男") {
						$("input[name='usersex'][value='男']").attr("checked",
								true);
						$("input[name='usersex'][value='女']").attr("checked",
								false);
					} else {
						$("input[name='usersex'][value='男']").attr("checked",
								false);
						$("input[name='usersex'][value='女']").attr("checked",
								true);
					}
					
					layer.open({
						type : 1,
						area : [ "600px", "500px" ],
						content : $("#editform")
					})
					layui.form.render();

				}
			});
			var $ = layui.$, active = {
				reload : function() {
					var demoReload = $('#demoReload');

					//执行重载
					table.reload('testReload', {
						page : {
							curr : 1
						//重新从第 1 页开始
						},
						where : {
							content : demoReload.val()
						}
					});
				}
			};

			$('.but002').on('click', function() {

				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});

			layui.form.on('submit(demo1)', function(data) {

				$.ajax({

					type : "put",//提交方式
					url : "updateUser",
					data : JSON.stringify(data.field),
					contentType : "application/json",
					success : function(cm) {

						layer.msg(cm.msg, {

							icon : 1,
							time : 2000

						}, function() {

							if (cm.msg == "修改用户成功") {
								location.reload();
								/* location.href = "userlist.jsp"; */
							}

						})

					}
				})

			});

			$(document).on("click", ".update", function() {

				layer.open({
					type : 1,
					area : [ "600px", "500px" ],
					content : $("#editform01")
				})
				layui.form.render();

			});

			$(document).on("click", ".btn001", function() {

				layer.open({
					type : 1,
					area : [ "600px", "500px" ],
					content : $("#editform01")
				})
				layui.form.render();

			});

			layui.form.on('submit(demo02)', function(data) {

				$.ajax({

					type : "put",//提交方式
					url : "addUser",
					data : JSON.stringify(data.field),
					contentType : "application/json",
					success : function(cm) {
						layer.msg(cm.msg, {
							icon : 1,
							time : 2000
						}, function() {

							if (cm.msg == "增加用户成功") {

								location.reload();
							}
						})
					}
				});

			});
		});
	</script>

	<!-- plugins:js -->
	<script src="node_modules/jquery/dist/jquery.min.js"></script>
	<script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
	<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
	<script
		src="node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js"></script>

	<script src="node_modules/chart.js/dist/Chart.min.js"></script>

	<script src="js/off-canvas.js"></script>
	<script src="js/misc.js"></script>

	<script src="js/dashboard.js"></script>

</body>

</html>