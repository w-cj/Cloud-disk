package com.etc.service;

import java.util.List;

import com.etc.entity.User;
import com.etc.util.PageData;

/**
 * 对用户和管理员操作的service接口
 * 
 * @author chao
 *
 */
public interface UserService {

	/**
	 * 分页查询用户
	 * 
	 * @param page
	 *            当前页
	 * @param limit
	 *            每页的页数
	 * @param content
	 *            模糊查询的内容
	 * @return
	 */
	public PageData<User> selectAllUserByPage(int page, int limit, String content);

	/**
	 * 分页查询管理员
	 * 
	 * @param page
	 *            当前页
	 * @param limit
	 *            每页的页数
	 * @param content
	 *            模糊查询的内容
	 * @return
	 */
	public PageData<User> selectAllAdminiByPage(int page, int limit, String content);

	/**
	 * 增加用户
	 * 
	 * @param user
	 *            所增加的用户
	 * @return
	 */

	public boolean insert(User user);

	/**
	 * 根据用户名查找用户信息
	 * 
	 * @param username
	 *            用户名
	 * @return 用户对象
	 */
	public User selectByUserName(String username);

	/**
	 * 根据用户手机号查询用户信息
	 * 
	 * @param userphone
	 *            用户手机号
	 * @return 用户信息
	 */
	public User selectByUserPhone(String userphone);

	/**
	 * 修改用户的信息
	 * 
	 * @param user
	 *            修改后的用户
	 * @return 是否成功的boolean值
	 */
	public boolean updateUser(User user);

	/**
	 * 禁用/启用 用户
	 * 
	 * @param userid
	 *            用户的userId
	 * @return
	 */
	public boolean deleteByPrimaryKey(Integer userid);

	/**
	 * 根据用户名查看用户是否存在，实现登录--zeng
	 * 
	 * @param username
	 *            输入的用户名
	 * @return
	 */
	public User selectUserByName(String username);

	/**
	 * 根据名字和电话号码搜索好友
	 * 
	 * @param content
	 *            根据输入信息搜索好友
	 * @return 返回一个用户的集合
	 */
	public List<User> getByNameOrPhone(String content, Integer userid);

	/**
	 * 根据id查询单个用户
	 * 
	 * @param userid
	 * @return
	 */
	public User selectByPrimaryKey(Integer userid);

	/**
	 * 修改用户信息
	 * 
	 * @param user
	 * @return 是否成功的boolean值
	 */
	public boolean updateByPrimaryKeySelective(User user);

	/**
	 * 统计总用户数
	 * 
	 * @return 总用户数
	 */
	public int selectCountUser();

}
