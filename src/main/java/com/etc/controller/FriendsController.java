package com.etc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.etc.entity.Friends;
import com.etc.entity.User;
import com.etc.service.FriendService;
import com.etc.util.CommenMassage;

@Controller
public class FriendsController {

	@Autowired
	private FriendService friendService;

	/**
	 * 跳转到加好友的界面
	 * 
	 * @return 好友界面
	 */
	@GetMapping(value = "addFriend")
	public String goAddFriend() {
		return "addFriend";
	}

	/**
	 * 跳转到查询好友的界面
	 * 
	 * @return 查询界面
	 */
	@GetMapping(value = "Online")
	public String goOnLine() {
		return "online";
	}

	/**
	 * 接收好友请求 通过传递过来的好友信息 处理存储到数据库
	 * 
	 * @param friends
	 *            好友信息对象
	 * @return 返回一个消息字符串
	 */
	@PostMapping(value = "sendFriendsReq")
	@ResponseBody
	public CommenMassage sendFriendsReq(Friends friends) {

		// 创建一个反馈信息对象
		CommenMassage cMassage = null;
		List<Friends> friendByReq = friendService.getFriendByReq(friends.getFromuserid(), friends.getTouserid());
		if (friendByReq.size() != 0) {
			cMassage = new CommenMassage("好友请求请求发送");
			return cMassage;
		}
		// 发送好友添加请求
		boolean bool = friendService.sendFriendsReq(friends);

		if (bool) {
			// 好友请求成功
			cMassage = new CommenMassage("好友请求发送成功");
		} else {
			// 好友请求发送失败
			cMassage = new CommenMassage("好友请求发送失败");
		}

		return cMassage;
	}

	/**
	 * 同意好友添加的方法
	 * 
	 * @param friends
	 *            好友对象
	 * @return
	 */
	@PostMapping(value = "addFriend")
	@ResponseBody
	public CommenMassage addFriend(Friends friends) {

		// 同意添加好友成功的方法
		boolean bool = friendService.addFriend(friends);

		// 创建一个反馈信息对象
		CommenMassage cMassage = null;
		if (bool) {
			// 好友请求成功
			cMassage = new CommenMassage("你们已经是好友了可以分享 文件吧");
		}

		return cMassage;
	}

	/**
	 * 删除好友的方法
	 * 
	 * @param friends
	 *            好友对象
	 * @return
	 */
	@PostMapping(value = "delFriends")
	public boolean delFriends(Friends friends) {

		return friendService.delFriends(friends);

	}

	/**
	 * 拒绝好友申请的方法
	 * 
	 * @param friends
	 *            好友对象
	 * @return
	 */
	@PostMapping(value = "delFriendsReq")
	@ResponseBody
	public CommenMassage delFriendsReq(Friends friends) {

		// 拒绝好友申请
		boolean bool = friendService.delFriendsReq(friends);

		// 创建一个反馈信息对象
		CommenMassage cMassage = null;
		if (bool) {
			// 好友请求成功
			cMassage = new CommenMassage("拒绝成功");
		}

		return cMassage;

	}

	/**
	 * 获取所有的好友申请
	 * 
	 * @param userid
	 *            用户的id
	 * @return 返回一个所有的添加好友的用户申请
	 */
	@GetMapping(value = "getFriendsReq")
	@ResponseBody
	public List<User> getFriendsReq(Integer userid) {

		return friendService.getFriendsReq(userid);

	}

	/**
	 * 返回所有的好友
	 * 
	 * @param userid
	 * @return
	 */
	@GetMapping(value = "getFriendsUser")
	@ResponseBody
	public List<User> getFriendsUser(Integer userid, String content) {
		// TODO Auto-generated method stub
		return friendService.getFriendsUser(userid, content);
	}

}
