package com.etc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etc.dao.UserRCMapper;
import com.etc.entity.UserRegisterCount;
import com.etc.service.UserRCService;

/**
 * 统计近一周用户注册的service接口实现类
 * 
 * @author chao
 *
 */
@Service
public class UserRCServiceImpl implements UserRCService {

	/**
	 * 注入 UserRCMapper
	 */
	@Autowired
	private UserRCMapper userRCMapper;

	/**
	 * 用户近一周每天注册数量
	 * 
	 * @return 每天注册数量的集合
	 */
	@Override
	public List<UserRegisterCount> getUserCount() {
		// TODO Auto-generated method stub

		return userRCMapper.getUserCount();

	}

}
