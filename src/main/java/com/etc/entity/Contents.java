package com.etc.entity;

import java.io.Serializable;

/**
 * 目录类
 * 
 * @author China 20192019年5月24日上午9:26:40
 */
public class Contents implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// 目录Id
	private Integer contentsid;
	// 用户Id
	private Integer userid;
	// 目录名
	private String contentsname;
	// 目录路径
	private String contentspath;
	// 上一级目录
	private Integer secondid;
	// 目录创建时间
	private String contentstime;

	public Integer getContentsid() {
		return contentsid;
	}

	public Contents(Integer userid, String contentsname, Integer secondid, String contentstime) {
		super();
		this.userid = userid;
		this.contentsname = contentsname;
		this.secondid = secondid;
		this.contentstime = contentstime;
	}

	public Contents() {
		super();
	}

	public Contents(Integer userid, String contentsname, String contentspath, Integer secondid, String contentstime) {
		super();
		this.userid = userid;
		this.contentsname = contentsname;
		this.contentspath = contentspath;
		this.secondid = secondid;
		this.contentstime = contentstime;
	}

	public void setContentsid(Integer contentsid) {
		this.contentsid = contentsid;
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public String getContentsname() {
		return contentsname;
	}

	public void setContentsname(String contentsname) {
		this.contentsname = contentsname == null ? null : contentsname.trim();
	}

	public String getContentspath() {
		return contentspath;
	}

	public void setContentspath(String contentpath) {
		this.contentspath = contentpath == null ? null : contentpath.trim();
	}

	public Integer getSecondid() {
		return secondid;
	}

	public void setSecondid(Integer secondid) {
		this.secondid = secondid;
	}

	public String getContentstime() {
		return contentstime;
	}

	public void setContentstime(String contenttime) {
		this.contentstime = contenttime;
	}

	@Override
	public String toString() {
		return "Contents [contentsid=" + contentsid + ", userid=" + userid + ", contentsname=" + contentsname
				+ ", contentspath=" + contentspath + ", secondid=" + secondid + ", contentstime=" + contentstime + "]";
	}

}