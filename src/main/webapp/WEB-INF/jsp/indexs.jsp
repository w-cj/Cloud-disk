<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Home</title>
<!-- plugins:css -->
<link rel="stylesheet"
	href="node_modules/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet"
	href="node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css">

<link rel="stylesheet"
	href="node_modules/jquery-bar-rating/dist/themes/css-stars.css">
<link rel="stylesheet"
	href="node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" />

<link rel="stylesheet" href="css/style.css">
<!-- endinject -->
<link rel="shortcut icon" href="images/favicon.png" />


</head>


<body>
	<div class="container-scroller">
		<!-- partial:partials/_navbar.html -->
		<%@ include file="head.jsp"%>
		<!-- partial -->
		<div class="container-fluid page-body-wrapper">
			<div class="row row-offcanvas row-offcanvas-right">
				<%@ include file="left.jsp"%>

				<div class="content-wrapper">
					<div class="row">
						<div class="col-md-4 stretch-card grid-margin">
							<div class="card bg-gradient-warning text-white">
								<div class="card-body">
									<h4 class="font-weight-normal mb-3">总用户数</h4>
									<h2 class="font-weight-normal mb-5"> ${countUser}</h2>

								</div>
							</div>
						</div>
						<div class="col-md-4 stretch-card grid-margin">
							<div class="card bg-gradient-info text-white">
								<div class="card-body">
									<h4 class="font-weight-normal mb-3">总文件量</h4>
									<h2 class="font-weight-normal mb-5">${countFile}</h2>
								</div>
							</div>
						</div>
						<div class="col-md-4 stretch-card grid-margin">
							<div class="card bg-gradient-success text-white">
								<div class="card-body">

									<h4 class="font-weight-normal mb-3">总分享数</h4>
									<h2 class="font-weight-normal mb-5">${countShare}</h2>

								</div>
							</div>
						</div>
					</div>

					<div class="page-container">
						<div class="row">
							<div class="col-md-6 grid-margin stretch-card">
								<div class="card">
									<div class="card-body">
										<h4 class="card-title">文件类型数量</h4>
										<canvas id="lineChart" height=200px ></canvas>
									</div>
								</div>
							</div>

							<div class="col-md-6 grid-margin stretch-card">
								<div class="card">
									<div class="card-body d-flex flex-column">
										<h4 class="card-title">近一周用户注册量</h4>
										<div class="mt-auto">
											<canvas id="barChart" height=200px></canvas>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- row-offcanvas ends -->
				</div>
				<!-- page-body-wrapper ends -->
			</div>
		</div>
	</div>

	<!-- <script src="js/jquery.js"></script> -->

	<script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
	<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
	<script
		src="node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js"></script>

	<script src="node_modules/chart.js/dist/Chart.min.js"></script>

	<script src="js/off-canvas.js"></script>
	<script src="js/misc.js"></script>

	<script src="js/dashboard.js"></script>
	<script src="js/chart.js"></script>
	<script src="js/chart2.js"></script>
</body>

</html>