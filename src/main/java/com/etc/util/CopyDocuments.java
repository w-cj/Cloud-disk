package com.etc.util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.etc.entity.Files;
import com.etc.service.FileService;

@Component
public class CopyDocuments {
    /*public static void main(String[] args) throws Exception {
        copyDocuments("G:\\source.txt","G:\\dest.txt");
    }*/

    @Autowired
    FileService fileService;
    /**
     * 文件的复制
     * @param fromsecondId 被复制的文件的父目录id
     * @param tosecondId 目标目录的id
     * @param fromcontentsName 被复制的文件的父目录名
     */
    public void copyDocuments(Integer fromsecondId,Integer tosecondId,String fromcontentsName) {
    	
    	//根据父目录id 和 文件名 获取 文件对象
    	Files file = fileService.selectByFnameAndSid(fromsecondId, fromcontentsName);
    	//修改 file 父目录id
    	file.setSecondid(tosecondId);
    	
    	file.setFileid(null);
    	//添加这条记录
    	fileService.addFile(file);
    }
}