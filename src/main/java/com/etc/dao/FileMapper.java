package com.etc.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.etc.entity.Files;

@Mapper
@Repository
public interface FileMapper {
	int deleteByPrimaryKey(Integer fileid);

	int insert(Files record);

	/**
	 * 添加的单个文件
	 * 
	 * @param record
	 *            文件对象
	 * @return 受影响行数
	 */
	int insertSelective(Files record);

	Files selectByPrimaryKey(@Param("fileid") Integer fileid);

	int updateByPrimaryKeySelective(Files record);

	int updateByPrimaryKey(Files record);

	/**
	 * 得到总记录数
	 * 
	 * @param content
	 *            模糊查询的参数
	 * @return 受影响的行数
	 */
	int getCount(String content);

	/**
	 * 对文件进行分页
	 * 
	 * @param start
	 *            分页起始位置
	 * @param limit
	 *            每页的条数
	 * @param content
	 *            模糊查询的参数
	 * @return 文件集合
	 */
	List<Files> getFileByBage(int start, int limit, String content);

	/**
	 * 根据用户id和文件名模糊查找
	 * 
	 * @param secondId
	 * @return 文件集合
	 */
	List<Files> getFileByLike(@Param(value = "userId") Integer userId,
			@Param(value = "content") String content);

	/**
	 * 根据上一级目录ID来查找上一级目录所拥有的文件
	 * 
	 * @param secondId
	 * @return 文件集合
	 */
	List<Files> getFileBySecondId(@Param(value = "secondId") Integer secondId,@Param(value = "userId") Integer userId);

	/**
	 * 根据上一级目录ID和文件名查询单个文件
	 * 
	 * @param secondId
	 *            上一级目录ID
	 * @param fileName
	 *            文件名
	 * @return 单个文件
	 */
	Files selectBySecondIdAndFileName(@Param(value = "secondId") Integer secondId,
			@Param(value = "fileName") String fileName);
	
	/**
	 * 根据上一级目录ID和文件名查询单个文件
	 * 
	 * @param secondId
	 *            上一级目录ID
	 * @param fileName
	 *            文件名
	 * @return 单个文件
	 */
	Files selectBySecondIdAndFileNameAndUserId(@Param(value = "secondId") Integer secondId,
			@Param(value = "fileName") String fileName,@Param(value = "userId") Integer userId);

	/**
	 * 根据上一级目录ID和文件名删除单个文件
	 * 
	 * @param secondId
	 *            上一级目录ID
	 * @param fileName
	 *            文件名
	 * @return 受影响的行数
	 */
	int deleteBySecondIdAndFileName(@Param(value = "secondId") Integer secondId,
			@Param(value = "fileName") String fileName);

	/**
	 * 根据用户id和文件类型查找所有文件
	 * 
	 * @param userId
	 *            用户id
	 * @param fileType
	 *            文件类型
	 * @return 文件集合
	 */
	List<Files> selectByUserIdAndFileType(@Param(value = "userId") Integer userId,
			@Param(value = "fileType") Integer fileType);
}