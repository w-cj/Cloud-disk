$.post("./SystemInfo", {
    _: new Date().toString()
}, function (result) {
    var list = JSON.parse(result);
    //JSON Data is ["C-82","D-56","E-64"]

    // 绘制图表。
    var chart_cpu = echarts.init(document.getElementById('charts-system-cpu'), 'macarons');
    chart_cpu.setOption({
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        series: [{
            name: '使用率',
            type: 'pie',
            radius: ['50%', '70%'],
            avoidLabelOverlap: false,
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: list[0].split("-")[1] + '%'
                },
                emphasis: {
                    show: true,
                    textStyle: {
                        fontSize: '12',
                        // fontWeight: 'bold'
                    }
                }
            },
            data: [{
                    value: parseInt(list[0].split("-")[1]),
                    name: '已使用'
                },
                {
                    value: 100 - parseInt(list[0].split("-")[1]),
                    name: '未使用'
                }
            ]
        }]
    });

    var chart_mem = echarts.init(document.getElementById('charts-system-mem'), 'macarons');
    chart_mem.setOption({
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        series: [{
            name: '使用',
            type: 'pie',
            radius: ['50%', '70%'],
            avoidLabelOverlap: false,
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: list[1].split("-")[1] + '%'
                },
                emphasis: {
                    show: true,
                    textStyle: {
                        fontSize: '12',
                        // fontWeight: 'bold'
                    }
                }
            },
            data: [{
                    value: parseInt(list[1].split("-")[1]),
                    name: '已使用'
                },
                {
                    value: 100 - parseInt(list[1].split("-")[1]),
                    name: '未使用'
                }
            ]
        }]
    });


    var chart_other = echarts.init(document.getElementById('charts-system-other'), 'macarons');
    chart_other.setOption({
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        series: [{
            name: '使用',
            type: 'pie',
            radius: ['50%', '70%'],
            avoidLabelOverlap: false,
            label: {
                normal: {
                    show: true,
                    position: 'center',
                    formatter: list[2].split("-")[1] + '%'
                },
                emphasis: {
                    show: true,
                    textStyle: {
                        fontSize: '12',
                        // fontWeight: 'bold'
                    }
                }
            },
            data: [{
                    value: parseInt(list[2].split("-")[1]),
                    name: '已使用'
                },
                {
                    value: 100 - parseInt(list[2].split("-")[1]),
                    name: '未使用'
                }
            ]
        }]
    });
});



(function () {

    var chart_other = echarts.init(document.getElementById('charts-click-cpu'), 'macarons');
    chart_other.setOption({
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: [],
            name: 'Time',
        },
        yAxis: {
            name: 'PV',
            type: 'value',

        },
        series: [{
            data: [],
            type: 'line',
            areaStyle: {}
        }],
        tooltip: {
            trigger: 'item',
            formatter: "{b}: {c} PV"
        }
    });
    setInterval(chartReload, 5000);

    function chartReload() {
        //http://localhost:9527/bookstore/AjaxChart ajax
        $.post("AjaxChart", {
            _: new Date().toString()
        }, function (result) {
            var timedata = JSON.parse(result);
            chart_other.setOption({
                xAxis: {
                    data: timedata[0]
                },
                series: [{
                    data: timedata[1]
                }]
            });
        });
    }
    chartReload();
})();