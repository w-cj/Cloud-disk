package com.etc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etc.dao.FileCountMapper;
import com.etc.entity.FileCount;
import com.etc.service.FileCountService;

/**
 * 统计每种类型文件的数量的service接口实现类
 * 
 * @author chao
 *
 */
@Service
public class FileCountServiceImpl implements FileCountService {
	// 对接口进行注入
	@Autowired
	private FileCountMapper Filecm;

	/**
	 * 统计每种类型文件的数量
	 * 
	 * @return 返回集合 集合里面包含每种类型文件的数量
	 */
	@Override
	public List<FileCount> getFileCount() {
		// TODO Auto-generated method stub
		return Filecm.getFileCount();
	}

}
