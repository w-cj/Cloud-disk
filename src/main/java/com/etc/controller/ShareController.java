package com.etc.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.etc.dao.ShareUserMapper;
import com.etc.entity.Contents;
import com.etc.entity.Files;
import com.etc.entity.Share;
import com.etc.service.ContentsService;
import com.etc.service.FileService;
import com.etc.service.ShareService;
import com.etc.service.ShareUserService;
import com.etc.util.CommenMassage;

@Controller
public class ShareController {

	@Autowired
	private ShareUserService shareUserService;
	@Autowired
	private ShareService shareService;
	@Autowired
	private FileService fileService;
	@Autowired
	private ContentsService contentsService;

	/**
	 * 删除一条分享记录
	 * 
	 * @param shareid
	 *            删除记录的id
	 * @return 是否删除成功
	 */
	@PostMapping(value = "deletShare")
	@ResponseBody
	public CommenMassage deleteShareById(@RequestParam("files") Integer files[]) {

		CommenMassage cMassage = new CommenMassage();
		int a = 0;
		// 遍历删除
		for (int i = 0; i < files.length; i++) {
			boolean bool = shareService.deleteShareById(files[i]);
			if (bool) {
				a++;
			}
		}
		// 当a等于数组的长度的时候 删除成功 删除失败
		if (a == files.length) {
			cMassage.setMsg("删除成功");
		} else {
			cMassage.setMsg("删除失败");
		}
		return cMassage;
	}

	/**
	 * 添加一条分享记录
	 * 
	 * @param share
	 *            分享对象
	 * @return 是否添加成功
	 */
	@PostMapping(value = "addShare")
	@ResponseBody
	public CommenMassage addShare(String[] fileids, String[] secondIds, String[] filenames, String[] touserid,
			Integer fromuserid, String sharestate) {
		// boolean bool = shareService.addShare(share);

		for (int i = 0; i < fileids.length; i++) {
			// 获取对应的文件id
			Files file = fileService.selectByFnameAndSid(Integer.parseInt(secondIds[i]), filenames[i]);
			Share share = new Share();
			share.setFileid(file.getFileid());
			share.setFromuserid(fromuserid);
			share.setShareaddress(file.getFilepath());
			share.setSharestate(sharestate);
			for (int j = 0; j < touserid.length; j++) {
				share.setTouserid(Integer.parseInt(touserid[j]));
				// 添加到分享文件
				shareService.addShare(share);
			}

		}

		// 判断是否添加成功
		CommenMassage cMassage = new CommenMassage();
		if (true) {
			cMassage.setMsg("添加成功");
		} else {
			cMassage.setMsg("添加失败");
		}
		return cMassage;
	}

	/**
	 * 获取我的分享记录
	 * 
	 * @param fromuserid
	 *            本用户的id
	 * @param request
	 *            我的分享请求
	 * @return 返回一个分享记录的集合
	 */
	@GetMapping(value = "myShare/{fromuserid}")
	public String getShareByFromid(@PathVariable("fromuserid") Integer fromuserid, HttpServletRequest request) {
		List<ShareUserMapper> shareFiles = shareUserService.getShareByFromid(fromuserid);
		request.setAttribute("shareFiles", shareFiles);
		return "share";
	}

	/**
	 * 获取好友的分享
	 * 
	 * @param touserid
	 *            被分享者的id
	 * @param request
	 *            好友分享请求
	 * @return 返回一个分享记录的集合
	 */
	@GetMapping(value = "friendShare")
	public String getShareByToid(@RequestParam("touserid") Integer touserid, HttpServletRequest request) {
		List<ShareUserMapper> shareFriend = shareUserService.getShareByToid(touserid, 1);
		request.setAttribute("shareFriend", shareFriend);
		return "friendShare";
	}

	/**
	 * 弹出层的界面
	 * 
	 * @param secondId
	 *            上一级目录id
	 * @param userId
	 *            用户id
	 * @param model
	 *            存放数据
	 * @return savefiles界面
	 */
	@GetMapping(value = "jump")
	public String jump(Integer secondId, Integer userId, Model model) {
		// 得到所有目录
		List<Contents> contentsList = contentsService.getContentsBySecondId(secondId, userId);
		model.addAttribute("contents", contentsList);
		return "savefiles";
	}

	/**
	 * 返回上一级的接口
	 * 
	 * @param secondId
	 *            上一级目录的id
	 * @param userId
	 *            用户id
	 * @param model
	 *            Model接口，调用其他控制器接口时使用
	 * @return files.jsp
	 */
	@GetMapping(value = "return")
	public String getSecondIdBySecondId(@RequestParam(value = "secondId") Integer secondId,
			@RequestParam(value = "userId") Integer userId, Model model) {

		Integer parentSecondId = contentsService.getSecondIdBySecondId(secondId);
		return jump(parentSecondId, userId, model);
	}

	/**
	 * 分享的文件保存
	 * 
	 * @param secondId
	 *            当前文件的上一级目录id
	 * @param fileName
	 *            文件名
	 * @param toSecondId
	 *            保存到那个目录下
	 * @param toUserId
	 *            谁保存文件
	 * @return
	 */
	@GetMapping(value = "saveShare")
	@ResponseBody
	public CommenMassage saveShare(Integer[] secondId, String[] fileName, Integer toSecondId, Integer[] toUserId) {

		if (toSecondId == null) {
			toSecondId = 0;
		}
		CommenMassage commenMassage = new CommenMassage();
		for (int i = 0; i < fileName.length; i++) {

			if (fileService.selectByFnameAndSidAndUid(toSecondId, fileName[i], toUserId[i]) == null) {
				Files file = fileService.selectByFnameAndSid(secondId[i], fileName[i]);
				file.setFiletime(new Date().toLocaleString());
				file.setSecondid(toSecondId);
				file.setUserid(toUserId[i]);
				file.setFileid(null);
				int a = fileService.addFile(file);
				if (a > 0) {
					commenMassage.setMsg("保存成功");
				} else {
					commenMassage.setMsg("保存失败");
				}
			} else {
				commenMassage.setMsg("文件已存在");
			}
		}
		return commenMassage;
	}

}
