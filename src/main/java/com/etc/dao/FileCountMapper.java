package com.etc.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.etc.entity.FileCount;
/**
 * 统计每种类型文件的数量的类
 * @author chao
 *
 */
@Repository
@Mapper
public interface FileCountMapper {
    /**
     * 统计每种类型文件的数量
     * @return   返回集合  集合里面包含每种类型文件的数量
     */
	List<FileCount> getFileCount();

}
