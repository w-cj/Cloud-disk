package com.etc.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.etc.entity.Friends;
import com.etc.entity.User;
import com.etc.service.FriendService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootJpaApplicationFriendsServiceTest {

	@Autowired
	FriendService friendService;

	@Test
	public void addFriendsReq() {

		// 创建一个好友对象
		Friends friends = new Friends();

		friends.setFromuserid(5);

		friends.setTouserid(6);

		friends.setRelationstate(2);

		// 调用添加好友请求的方法
		friendService.sendFriendsReq(friends);

	}

	@Test
	public void addFriend() {

		// 创建一个好友对象
		Friends friends = new Friends();

		friends.setFromuserid(5);

		friends.setTouserid(6);

		friends.setRelationstate(1);

		// 调用添加好友成功的方法
		friendService.addFriend(friends);
	}

	@Test
	public void getFriendsReq() {

		List<User> list = friendService.getFriendsReq(2);
		System.out.println(list);

	}

	@Test
	public void getFriendsUser() {

		List<User> list = friendService.getFriendsUser(5,"");
		System.out.println(list);

	}

	@Test
	public void delFriends() {

		// 创建一个好友对象
		Friends friends = new Friends();

		friends.setFromuserid(5);

		friends.setTouserid(6);

		friendService.delFriends(friends);
	}
	
	@Test
	public void delFriendsReq() {

		// 创建一个好友对象
		Friends friends = new Friends();

		friends.setFromuserid(5);

		friends.setTouserid(6);

		friendService.delFriendsReq(friends);
	}

}
