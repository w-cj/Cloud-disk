package com.etc.util;

/**
 * 校验手机号是否符合格式的工具类
 * @author cyf
 *
 */
public class PhoneRegex {

	/**
	 * 
	 * @param userphone 用户手机号
	 * @return 校验结果
	 */
	public static boolean checkPhone(String userphone) {
		String regex = "^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$";
		boolean flag = userphone.matches(regex);
		return flag;
	}

}
