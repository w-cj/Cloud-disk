package com.etc.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.etc.dao.ShareMapper;
import com.etc.entity.Share;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootJpaApplicationShareDao {

	@Autowired
	ShareMapper shareMapper;
	
	@Test
	public void insert() {
		//创建一个分享对象
		Share share = new Share();
		share.setFromuserid(3);
		share.setTouserid(2);
		share.setShareaddress("hhh");
		share.setSharestate("All");
		//添加一条分享记录
		shareMapper.insert(share);
	}
	
	@Test
	public void deleteByPrimaryKey() {
		//删除一条分享记录
		shareMapper.deleteByPrimaryKey(1);
	}

}
