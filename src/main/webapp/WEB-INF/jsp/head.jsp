<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<script src="js/jquery.js"></script>
<script src="layui/layui.js" charset="utf-8"></script>
<script type="text/javascript" src="layer/2.1/layer.js"></script>
<link rel="stylesheet" href="layui/css/layui.css" media="all">
<script type="text/javascript">
	layui.use('table', function() {

		$(document).on(
				"click",
				".updateuser",
				function() {

					$("#userid001").attr("value", "${admin.userid}");
					$
							.ajax({
								type : "get",
								url : "getUser/" + '${admin.userid}',
								success : function(data) {
									$("#username001").attr("value",
											data.username);
									$("#userphone001").attr("value",
											data.userphone);

									if (data.usersex == "男") {
										$("input[name='usersex'][value='男']")
												.attr("checked", true);
										$("input[name='usersex'][value='女']")
												.attr("checked", false);
									} else {
										$("input[name='usersex'][value='男']")
												.attr("checked", false);
										$("input[name='usersex'][value='女']")
												.attr("checked", true);
									}
									layui.form.render();
									$("#userdatetime").attr("value",
											data.userdatetime);
								}

							})

					layer.open({
						type : 1,
						area : [ "580px", "500px" ],
						content : $("#editformupdate")
					})
					layui.form.render();

				});

		layui.form.on('submit(update001)', function(data) {

			$.ajax({

				type : "put",//提交方式
				url : "updateUser",
				data : JSON.stringify(data.field),
				contentType : "application/json",
				success : function(cm) {

					layer.msg(cm.msg, {

						icon : 1,
						time : 2000

					}, function() {

						if (cm.msg == "修改用户成功") {
							location.reload();
						}

					})

				}
			})

		});

		$(document).on("click", ".updatepwd01", function() {
			layer.open({
				type : 1,
				area : [ "330px", "360px" ],
				content : $("#updatepassword")
			})
			layui.form.render();
		});

		layui.form.on('submit(updatepwd)', function(data) {
			$.ajax({
				type : "put",
				url : "backupdatePwd",
				data : JSON.stringify(data.field),
				contentType : "application/json",
				success : function(cm) {
					if (cm.msg == "修改密码成功") {
						layer.msg(cm.msg, {
							icon : 1,
							time : 2000

						}, function() {
							window.location.href = "backlogin";
						});
					} else if (cm.msg == "旧密码不能与新密码一致" || cm.msg == "旧密码输入错误") {
						layer.msg(cm.msg, {
							icon : 4,
							time : 2000
						}, function() {
							location.reload();
						})
					} else {
						layer.msg(cm.msg, {
							icon : 2,
							time : 2000
						}, function() {
							location.reload();
						})
					}

				}
			})

		});

	});
</script>

<!-- 修改密码的弹出层 -->
<div id="updatepassword" style="display: none" align="center">
	<form class="layui-form" action="" lay-filter="example03"
		id="examples03">
		<div class="layui-form-item">
			<br> <label class="layui-form-label">旧密码:</label>
			<div class="layui-input-block">
				<input type="password" name="userPwd" id="oldpwd" lay-verify="title"
					autocomplete="off" class="layui-input">
			</div>
		</div>

		<div class="layui-form-item">
			<br> <label class="layui-form-label">新密码:</label>
			<div class="layui-input-block">
				<input type="password" name="userPwd1" id="newpwd"
					lay-verify="title" autocomplete="off" class="layui-input">
			</div>
		</div>

		<div class="layui-form-item">
			<br> <label class="layui-form-label">确认密码:</label>
			<div class="layui-input-block">
				<input type="password" name="userPwd2" id="newpwd1"
					lay-verify="title" autocomplete="off" class="layui-input">
			</div>
		</div>

		<div class="layui-form-item">
			<div class="layui-input-block" align="center">
				<button class="layui-btn btn-update" type="button" lay-submit=""
					lay-filter="updatepwd">确认修改</button>
			</div>
		</div>

	</form>
</div>

<!-- 修改当前登录用户信息 -->
<div id="editformupdate" style="display: none">
	<form class="layui-form" action="" lay-filter="example02"
		id="examples02">

		<div class="layui-form-item">
			<br> <label class="layui-form-label">编号</label>
			<div class="layui-input-block">
				<input type="text" name="userid" id="userid001" lay-verify="title"
					autocomplete="off" class="layui-input" readonly>
			</div>
		</div>

		<div class="layui-form-item">
			<br> <label class="layui-form-label">用户名</label>
			<div class="layui-input-block">
				<input type="text" name="username" id="username001"
					lay-verify="title" autocomplete="off" class="layui-input">
			</div>
		</div>

		<div class="layui-form-item">
			<label class="layui-form-label">性别</label>
			<div class="layui-input-block">
				<input type="radio" name="usersex" class="sex" id="sexmen" value="男"
					title="男"> <input type="radio" name="usersex" class="sex"
					id="sexwomen" value="女" title="女">
			</div>
		</div>

		<div class="layui-form-item">
			<label class="layui-form-label">电话</label>
			<div class="layui-input-block">
				<input type="text" name="userphone" id="userphone001"
					lay-verify="title" autocomplete="off" class="layui-input">
			</div>
		</div>

		<div class="layui-form-item">
			<label class="layui-form-label">注册时间</label>
			<div class="layui-input-block">
				<input type="text" name="userdatetime" id="userdatetime"
					lay-verify="title" autocomplete="off" class="layui-input" readonly>
			</div>
		</div>

		<div class="layui-form-item">
			<div class="layui-input-block" align="center">
				<button class="layui-btn btn-update" type="button" lay-submit=""
					lay-filter="update001">立即提交</button>
			</div>
		</div>
	</form>
</div>


<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">

	<div
		class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
		<a class="navbar-brand brand-logo" href="#"><img
			src="images/logo.svg" alt="logo" /></a> <a
			class="navbar-brand brand-logo-mini" href="#"><img
			src="images/logo-mini.svg" alt="logo" /></a>
	</div>
	<div class="navbar-menu-wrapper d-flex align-items-stretch">

		<ul class="navbar-nav navbar-nav-right">
			<li class="nav-item d-none d-lg-block full-screen-link"><a
				class="nav-link"> <i class="mdi mdi-fullscreen"
					id="fullscreen-button"></i>
			</a></li>

			<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle nav-profile" id="profileDropdown"
				href="#" data-toggle="dropdown" aria-expanded="false"> <img
					src="images/faces/face1.jpg" alt="image"> <span
					class="d-none d-lg-inline">${admin.username}</span>
			</a>
				<div class="dropdown-menu navbar-dropdown w-100"
					aria-labelledby="profileDropdown">
					<button class="dropdown-item updatepwd01" id="updatepwd01">
						<i class="mdi mdi-border-color"></i> &nbsp;修改密码
					</button>
					<div class="dropdown-divider"></div>
					<button class="dropdown-item updateuser" id="update">
						<i class="mdi mdi-cached mr-2 text-success" lay-event="edit"></i>
						修改信息
					</button>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="backdoLogout"> <i
						class="mdi mdi-logout mr-2 text-primary"></i> 退出
					</a>
				</div></li>
			<li class="nav-item nav-logout d-none d-lg-block"><a
				class="nav-link" href="backdoLogout"> <i class="mdi mdi-power"></i>
			</a></li>
		</ul>
		<button
			class="navbar-toggler navbar-toggler-right d-lg-none align-self-center"
			type="button" data-toggle="offcanvas">
			<span class="mdi mdi-menu"></span>
		</button>
	</div>
</nav>
