<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/mdui/css/mdui.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/main.css">
<script
	src="${pageContext.request.contextPath}/static/mdui/js/mdui.min.js"></script>
<script src="${pageContext.request.contextPath}/static/jq/jq.min.js"></script>
<script src="${pageContext.request.contextPath}/static/coll/common.js"></script>
<script
	src="${pageContext.request.contextPath}/static/coll/jquery.cookie.js"></script>

<style>
#newfolder {
	display: inline-block;
	color: #09AAFF;
	text-decoration: none;
}
</style>
</head>
<body>
	<div class="mdui-toolbar-spacer">
		<a href="javascript:;" id="newfolder">新建文件夹</a>
	</div>
	<div id="su_file_list" class="mdui-list">
		<a href="javascript:;" id="history">返回上一级</a>
		<!-- 目录列表 -->
		<c:forEach items="${contents}" varStatus="i" var="item">
			<!-- 隐藏的新建目录列 -->
			<label class="mdui-list-item mdui-ripple mdui-text-truncate"
				style="display: none;" id="btnfolder">
				<div class="mdui-list-item-content">
					<img alt=""
						src="${pageContext.request.contextPath}/images/files.png"
						style="width: 22px; height: 22px;">
					<div class="su-file-info" style="width: 15%;">
						<input type="text" placeholder="新建文件夹" id="foldername"
							autofocus="autofocus">
					</div>
					<button id="affirm" type="button" style="margin-left: 120px">确认</button>
					<button id="cancel" style="margin-left: 20px">取消</button>
				</div>
			</label>

			<!-- 显示的目录 -->
			<label
				class="mdui-list-item mdui-ripple mdui-text-truncate catalogue">
				<div class="mdui-checkbox">
					<%-- <input id="files_index_${i.index}" type="checkbox"
						data-elev="${ item.contentsname }" /> <i
						class="mdui-checkbox-icon"></i> --%>
				</div>
				<div class="mdui-list-item-content">
					<img alt=""
						src="${pageContext.request.contextPath}/images/files.png"
						style="width: 22px; height: 22px;">
					<div class="su-file-info name" style="width: 40%;">
						<a
							href="${pageContext.request.contextPath}/jump?secondId=${item.contentsid}&userId=${user.userid}"
							class="folderClick">${ item.contentsname }</a>
					</div>
					<div class="su-file-info " style="width: 17%;">--</div>
					<div class="su-file-info  mdui-hidden-sm-down" style="width: 40%;">${ item.contentstime }</div>
					<input type="text" value="${item.contentsid }"
						style="display: none;" class="contentsid">
				</div>
			</label>

			<input type="text" value="0" style="display: none;" class="filetype">
			<input type="text" value="${item.secondid }" style="display: none;"
				class="secondid">
		</c:forEach>

		<!-- 空文件夹 -->
		<c:if test="${empty contents}">
			<label class="mdui-list-item mdui-ripple mdui-text-truncate"
				style="display: none;" id="btnfolder">
				<div class="mdui-list-item-content">
					<img alt=""
						src="${pageContext.request.contextPath}/images/files.png"
						style="width: 22px; height: 22px;">
					<div class="su-file-info" style="width: 15%;">
						<input type="text" placeholder="新建文件夹" id="foldername"
							autofocus="autofocus">
					</div>
					<button id="affirm" type="button">确认</button>
					<button id="cancel">取消</button>
				</div>
			</label>
			<label class="mdui-list-item mdui-ripple mdui-text-truncate"
				id="nonefile">
				<div class="mdui-list-item-content">
					<div class="su-file-info " style="width: 100px; margin: auto;">无文件</div>
				</div>
			</label>
		</c:if>
	</div>
</body>
<script src="${pageContext.request.contextPath}/layer/2.1/layer.js"></script>
<script>
	$(function() {
		$(".folderClick").on("click",function(){
			var contentsid = $(this).parent().parent().find(".contentsid").val();
			$.cookie("contentsid",contentsid);
		})
		
		function GetQueryString(name) {
			var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
			var r = window.location.search.substr(1).match(reg);
			if (r != null)
				return unescape(r[2]);
			return null;
		}
		if (GetQueryString("secondId") == 0 || $(".secondid").val() == 0) {
			$("#history").css("display", "none");
		}
		//上一级的单击事件
		$("#history")
				.on(
						"click",
						function() {
							if ($(".secondid").val() == null) {
								window.location.href = "${pageContext.request.contextPath}/return?secondId="
										+ GetQueryString("secondId")
										+ "&userId=${user.userid}";
							} else if ($(".secondid").val() == 0) {

							} else {
								window.location.href = "${pageContext.request.contextPath}/return?secondId="
										+ $(".secondid").val() + "&userId=${user.userid}";
							}
						});
		//新建文件夹的显示
		$("#newfolder").on("click", function() {
			$("#btnfolder").show();
		});

		//新建的单击事件
		$("#affirm").on("click", function() {
			if ($("#nonefile").length > 0) {
				$.ajax({
					url : "${pageContext.request.contextPath}/addContents",
					data : {
						"secondId" : GetQueryString("secondId"),
						"userId" : ${user.userid},
						"contentsName" : $("#foldername").val()
					},
					type : "get",
					success : function(data) {
						layer.msg(data.msg, {
							time : 2000
						//2秒关闭（如果不配置，默认是3秒）
						}, function() {
							window.location.reload();
						});

						$("#nonefile").css("display", "none");
					}
				});
			} else {
				$.ajax({
					url : "${pageContext.request.contextPath}/addContents",
					data : {
						"secondId" : $(".secondid").val(),
						"userId" : ${user.userid},
						"contentsName" : $("#foldername").val()
					},
					type : "get",
					success : function(data) {
						layer.msg(data.msg, {
							time : 2000
						//2秒关闭（如果不配置，默认是3秒）
						}, function() {
							window.location.reload();
						});

						$("#nonefile").css("display", "none");
					}
				});
			}

		});

		//取消的单击事件
		$("#cancel").on("click", function() {
			$("#btnfolder").css("display", "none");
		});

	});
</script>
</html>