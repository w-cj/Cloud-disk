<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@include file="header.jsp"%>

<div class="mdui-container">
	<div class="mdui-row">
		<div class="mdui-col-md-12">

			<div class="mdui-toolbar">
				<!-- <a href="javascript:;" class="mdui-btn mdui-btn-icon"><i class="mdui-icon material-icons">insert_drive_file</i></a> -->
				<span class="mdui-typo-title">未审核文件列表</span>
				<div class="mdui-toolbar-spacer"></div>
				<a href="javascript:;"
					class="mdui-btn mdui-btn-icon mdui-text-color-red"
					onclick="PAGE.alloper('#su_file_list>label>div>input');"
					mdui-tooltip="{content: '全选/取消全选'}"> <i
					class="mdui-icon  material-icons">check_box</i>
				</a> <a href="javascript:;" onclick="PAGE.deleteShare();"
					class="mdui-btn mdui-btn-icon mdui-text-color-red"
					mdui-tooltip="{content: '删除分享'}"> <i
					class="mdui-icon material-icons ">delete</i>
				</a>
			</div>

			<div id="su_file_list" class="mdui-list">
				<c:forEach items="${auditFiles}" varStatus="i" var="item">

					<label class="mdui-list-item mdui-ripple ">
						<div class="mdui-checkbox">
							<input type="checkbox" data-elev="${ item.filename }" /> <i
								class="mdui-checkbox-icon"></i>
						</div>
						<div class="mdui-list-item-content ">

							<div class="su-file-info " style="width: 20%; max-width: 160px;">${ item.fileid}</div>
							<div class="su-file-info mdui-hidden-sm-down" style="width: 40%;">${ item.filename }</div>
							<div class="su-file-info mdui-hidden-sm-down" style="width: 20%;">${ item.filesize }</div>
							<div class="su-file-info mdui-hidden-sm-down" style="width: 10%;">
								<c:choose>
									<c:when test="${ item.status ==0}">审核中</c:when>
									<c:otherwise>审核失败</c:otherwise>
								</c:choose>
							</div>
						</div>
					</label>
				</c:forEach>
			</div>
		</div>
	</div>


	<div class="mdui-divider" style="margin: 8px 0 16px 0;"></div>

	<%-- <c:if test="${!empty page}">
		<a href="myShare?page=${ page - 1 }&fromuserid=3"><button
				class="mdui-btn mdui-color-pink-a200 mdui-ripple">上一页</button></a>
		<a href="myShare?page=${ page + 1 }&fromuserid=3"><button
				class="mdui-btn mdui-color-purple-400 mdui-ripple">下一页</button></a>
		<a href="myShare?fromuserid=3"><button
				class="mdui-btn mdui-color-red mdui-ripple">全部显示</button></a>
		<span class="mdui-hidden-sm-down" style="margin: 0 0 0 8px;"> 第 ${page} 页/共 ${ pagecount }
			页</span>
	</c:if> --%>
	<br> <br> <br> <br>
</div>



<script>
	PAGE.deleteShare = function() {
		var files = $(".mdui-list-item-content div:eq(0)").text();

		$.ajax({
			type : "post",
			url : "${pageContext.request.contextPath}/deletAudit",
			data : {
				"files" : files
			},
			traditional : true,
			success : function(cm) {
				layer.msg(cm.msg, {
					icon : 1, //图标		
					time : 2000,
					//2秒关闭（如果不配置，默认是3秒）
					success : function(layero, index) {
						location.reload();
					}
				});
			}
		});
	}
</script>



<%@include file="footer.jsp"%>
