package com.etc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etc.dao.CodeMapper;
import com.etc.entity.Code;
import com.etc.service.CodeService;

/**
 * 短信验证码 service层
 * 
 * @author Administrator
 *
 */
@Service
public class CodeServiceImpl implements CodeService {

	/**
	 * 注入CodeMapper
	 */
	@Autowired
	private CodeMapper codeMapper;

	/**
	 * 增加短信验证码对象
	 * 
	 * @param record
	 *            短信验证码对象
	 * @return 增加结果
	 */
	@Override
	public boolean insert(Code record) {

		return codeMapper.insert(record) > 0;
	}

	/**
	 * 通过用户手机号码查询验证码对象
	 * 
	 * @param userPhone
	 *            用户手机号码
	 * @return 验证码对象
	 */
	@Override
	public Code selectByUserPhone(String userphone) {

		return codeMapper.selectByUserPhone(userphone);
	}

	/**
	 * 删除用户手机号码
	 * 
	 * @param userPhone
	 *            用户手机号码
	 * @return 删除结果
	 */
	@Override
	public int deleteByUserPhone(String userphone) {

		return codeMapper.deleteByUserPhone(userphone);
	}

	/**
	 * 根据验证码id修改验证码信息
	 * 
	 * @param record
	 *            短信对象
	 * @return 修改结果
	 */
	@Override
	public boolean updateByPrimaryKey(Code record) {

		return codeMapper.updateByPrimaryKeySelective(record) > 0;
	}

}
