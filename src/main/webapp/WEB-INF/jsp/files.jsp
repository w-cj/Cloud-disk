<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<%@include file="header.jsp"%>

<style>
#newfolder {
	display: inline-block;
	color: #09AAFF;
	text-decoration: none;
}
</style>

<div id="picturebox" style="top: 50%; left: 50%; display: none;">

	<div style="margin: 0px auto;">
		<div class="block no-shadow" style="background-color: #1a1a1a;">
			<!--正文内容-->
			<div class="main-0">
				<img src="/i/eg_cute.gif" align="middle" id="picturedemo"
					style="width: 100%; height: 100%">
			</div>
		</div>
	</div>
</div>

<div id="moviebox" style="display: none">
	<!--播放内容-->
	<div style="margin: 0px auto;">
		<div class="block no-shadow" style="background-color: #1a1a1a;">
			<!--正文内容-->
			<div class="main-0">
				<div class="player">
					<video width="650px" height="500px" controls="controls">
						<source id="movisss" type="video/mp4" />
					</video>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="musicbox" style="display: none">
	<!--播放内容-->
	<div style="margin: 0px auto;">
		<div class="block no-shadow" style="background-color: #1a1a1a;">
			<!--正文内容-->
			<div class="player" style="text-align: center;">
				<audio controls="controls">
					<source id="music" type="audio/mp3">
				</audio>
			</div>
		</div>
	</div>
</div>

<div class="mdui-container">
	<div class="mdui-row">
		<div class="mdui-col-md-12">

			<div class="mdui-toolbar">

				<span class="mdui-typo-title">文件列表</span>
				<div class="mdui-toolbar-spacer">
					<a href="javascript:;" id="newfolder">新建文件夹</a>
				</div>

				<input type="text" placeholder="搜索您的文件" id="content"><a
					href="javascript:;" id="seach">搜索</a> <a href="javascript:;"
					class="mdui-btn mdui-btn-icon  mdui-text-color-orange"
					mdui-tooltip="{content: '粘贴'}" id="paste"> <i
					class="mdui-icon material-icons">content_copy</i> <a
					href="javascript:;"
					class="mdui-btn mdui-btn-icon  mdui-text-color-orange"
					mdui-tooltip="{content: '复制'}" id="copy"> <i
						class="mdui-icon material-icons">content_copy</i>
				</a> <a href="javascript:;"
					class="mdui-btn mdui-btn-icon mdui-text-color-red"
					onclick="PAGE.alloper('#su_file_list>label>div>input');"
					mdui-tooltip="{content: '全选/取消全选'}"> <i
						class="mdui-icon material-icons">check_box</i>
				</a> <a href="javascript:;"
					class="mdui-btn mdui-btn-icon mdui-text-color-red"
					onclick="PAGE.deleteFiles();" mdui-tooltip="{content: '删除文件'}">
						<i class="mdui-icon material-icons ">delete</i>
				</a> <a href="javascript:;"
					class="mdui-btn mdui-btn-icon  mdui-text-color-orange"
					onclick="PAGE.renameFile();" mdui-tooltip="{content: '重命名'}"> <i
						class="mdui-icon material-icons">content_copy</i>
				</a> <a href="javascript:;"
					class="mdui-btn mdui-btn-icon  mdui-text-color-green"
					mdui-tooltip="{content: '分享文件'}"> <i
						class="mdui-icon material-icons">share</i>
				</a> <a href="javascript:;"
					class="mdui-btn mdui-btn-icon mdui-text-color-blue"
					onclick="PAGE.downloadFile();" mdui-tooltip="{content: '下载'}">
						<i class="mdui-icon material-icons">attachment</i>
				</a>
			</div>

			<!-- 所有列表 -->
			<div id="su_file_list" class="mdui-list">
				<a href="javascript:;" id="history">返回上一级</a>
				<!-- 目录列表 -->
				<c:forEach items="${contents}" varStatus="i" var="item">
					<!-- 隐藏的新建目录列 -->
					<label class="mdui-list-item mdui-ripple mdui-text-truncate"
						style="display: none;" id="btnfolder">
						<div class="mdui-list-item-content">
							<img alt=""
								src="${pageContext.request.contextPath}/images/files.png"
								style="width: 22px; height: 22px;">
							<div class="su-file-info" style="width: 15%;">
								<input type="text" placeholder="新建文件夹" id="foldername"
									autofocus="autofocus">
							</div>
							<button id="affirm" type="button">确认</button>
							<button id="cancel">取消</button>
						</div>
					</label>

					<!-- 显示的目录 -->
					<label
						class="mdui-list-item mdui-ripple mdui-text-truncate catalogue">
						<div class="mdui-checkbox">
							<input id="files_index_${i.index}" type="checkbox"
								data-elev="${ item.contentsname }" /> <i
								class="mdui-checkbox-icon"></i>
						</div>
						<div class="mdui-list-item-content">
							<img alt=""
								src="${pageContext.request.contextPath}/images/files.png"
								style="width: 22px; height: 22px;">
							<div class="su-file-info name" style="width: 40%;">
								<a
									href="${pageContext.request.contextPath}/directory?secondId=${item.contentsid}&userId=${user.userid}"
									id="folderClick">${ item.contentsname }</a>
							</div>
							<div class="su-file-info " style="width: 17%;">--</div>
							<div class="su-file-info  mdui-hidden-sm-down"
								style="width: 40%;">${ item.contentstime }</div>
						</div> <input type="text" value="${item.contentsid }"
						style="display: none;" class="contentsid">
					</label>
					<input type="text" value="0" style="display: none;"
						class="filetype">
					<input type="text" value="${item.secondid }" style="display: none;"
						class="secondid">
				</c:forEach>

				<!-- 文件列表 -->
				<c:forEach items="${files}" varStatus="i" var="item">
					<c:choose>
						<c:when test="${ item.filetype==4 }">
							<label
								class="mdui-list-item mdui-ripple mdui-text-truncate openpicture">
								<div class="mdui-checkbox">
									<input id="files_index_${i.index}" type="checkbox"
										data-elev="${ item.filename }" /> <i
										class="mdui-checkbox-icon"></i>
								</div>
								<div class="mdui-list-item-content">
									<img alt=""
										src="${pageContext.request.contextPath}/images/pic.png"
										style="width: 22px; height: 22px;">
									<div class="su-file-info name" style="width: 40%;">${ item.filename }</div>
									<div class="su-file-info " style="width: 20%;">${ item.filesize }</div>
									<div class="su-file-info  mdui-hidden-sm-down"
										style="width: 30%;">${ item.filetime }</div>
								</div> <input type="hidden" value="${item.filepath }"
								class="picturefilepath"> <input type="hidden"
								value="${item.filename }" class="picturefilename">
							</label>
							<input type="text" value="${item.filetype }"
								style="display: none;" class="filetype">
							<input type="text" value="${item.secondid }"
								style="display: none;" class="secondid">
							<input type="text" value="${item.fileid }" style="display: none;"
								class="fileid">
						</c:when>
						<c:when test="${ item.filetype==1 }">
							<label
								class="mdui-list-item mdui-ripple mdui-text-truncate openmusic">
								<div class="mdui-checkbox">
									<input id="files_index_${i.index}" type="checkbox"
										data-elev="${ item.filename }" /> <i
										class="mdui-checkbox-icon"></i>
								</div>
								<div class="mdui-list-item-content">
									<img alt=""
										src="${pageContext.request.contextPath}/images/aideo.png"
										style="width: 22px; height: 22px;">
									<div class="su-file-info name" style="width: 40%;">${ item.filename }</div>
									<div class="su-file-info " style="width: 20%;">${ item.filesize }</div>
									<div class="su-file-info  mdui-hidden-sm-down"
										style="width: 30%;">${ item.filetime }</div>
								</div> <input type="text" value="${item.filepath }"
								style="display: none;" class="filepath"> <input
								type="text" value="${item.filename }" style="display: none;"
								class="filename">
							</label>
							<input type="text" value="${item.filetype }"
								style="display: none;" class="filetype">
							<input type="text" value="${item.secondid }"
								style="display: none;" class="secondid">
							<input type="text" value="${item.fileid }" style="display: none;"
								class="fileid">

						</c:when>
						<c:when test="${ item.filetype==2 }">
							<label
								class="mdui-list-item mdui-ripple mdui-text-truncate openmovie">
								<div class="mdui-checkbox">
									<input id="files_index_${i.index}" type="checkbox"
										data-elev="${ item.filename }" /> <i
										class="mdui-checkbox-icon"></i>
								</div>
								<div class="mdui-list-item-content">
									<img alt=""
										src="${pageContext.request.contextPath}/images/video.png"
										style="width: 22px; height: 22px;">
									<div class="su-file-info name" style="width: 40%;">${ item.filename }</div>
									<div class="su-file-info " style="width: 20%;">${ item.filesize }</div>
									<div class="su-file-info  mdui-hidden-sm-down"
										style="width: 30%;">${ item.filetime }</div>
								</div> <input type="hidden" value="${item.filepath}"
								class="moviefilepath"> <input type="hidden"
								value="${item.filename }" class="moviefilename">
							</label>
							<input type="text" value="${item.filetype }"
								style="display: none;" class="filetype">
							<input type="text" value="${item.secondid }"
								style="display: none;" class="secondid">
							<input type="text" value="${item.fileid }" style="display: none;"
								class="fileid">
						</c:when>
						<c:otherwise>
							<label class="mdui-list-item mdui-ripple mdui-text-truncate">
								<div class="mdui-checkbox">
									<input id="files_index_${i.index}" type="checkbox"
										data-elev="${ item.filename }" /> <i
										class="mdui-checkbox-icon"></i>
								</div>
								<div class="mdui-list-item-content">
									<img alt=""
										src="${pageContext.request.contextPath}/images/txt.png"
										style="width: 22px; height: 22px;">
									<div class="su-file-info name" style="width: 40%;">${ item.filename }</div>
									<div class="su-file-info " style="width: 20%;">${ item.filesize }</div>
									<div class="su-file-info  mdui-hidden-sm-down"
										style="width: 30%;">${ item.filetime }</div>
								</div>
							</label>
							<input type="text" value="${item.filetype }"
								style="display: none;" class="filetype">
							<input type="text" value="${item.secondid }"
								style="display: none;" class="secondid">
							<input type="text" value="${item.fileid }" style="display: none;"
								class="fileid">
						</c:otherwise>
					</c:choose>
					<%-- <label class="mdui-list-item mdui-ripple mdui-text-truncate">
						<div class="mdui-checkbox">
							<input id="files_index_${i.index}" type="checkbox"
								data-elev="${ item.filename }" /> <i class="mdui-checkbox-icon"></i>
						</div>
						<div class="mdui-list-item-content">
							<div class="su-file-info " style="width: 50%;">${ item.filename }</div>
							<div class="su-file-info  mdui-hidden-sm-down"
								style="width: 40%;">${ item.filepath }</div>
						</div>
					</label> --%>
				</c:forEach>

				<!-- 空文件夹 -->
				<c:if test="${empty contents and empty files}">
					<label class="mdui-list-item mdui-ripple mdui-text-truncate"
						style="display: none;" id="btnfolder">
						<div class="mdui-list-item-content">
							<img alt=""
								src="${pageContext.request.contextPath}/images/files.png"
								style="width: 22px; height: 22px;">
							<div class="su-file-info" style="width: 15%;">
								<input type="text" placeholder="新建文件夹" id="foldername"
									autofocus="autofocus">
							</div>
							<button id="affirm" type="button">确认</button>
							<button id="cancel">取消</button>
						</div>
					</label>
					<label class="mdui-list-item mdui-ripple mdui-text-truncate"
						id="nonefile">
						<div class="mdui-list-item-content">
							<div class="su-file-info " style="width: 100px; margin: auto;">无文件</div>
						</div>
					</label>
				</c:if>
			</div>
		</div>
	</div>

	<div class="mdui-divider" style="margin: 8px 0 16px 0;"></div>



	<c:if test="${!empty page}">
		<a href="FileManager?type=${ file_type }&page=${ page - 1 }"><button
				class="mdui-btn mdui-color-pink-a200 mdui-ripple">上一页</button></a>
		<a href="FileManager?type=${ file_type }&page=${ page + 1 }"><button
				class="mdui-btn mdui-color-purple-400 mdui-ripple">下一页</button></a>
		<a href="FileManager?type=${ file_type }"><button
				class="mdui-btn mdui-color-red mdui-ripple">全部显示</button></a>
		<span class="mdui-hidden-sm-down" style="margin: 0 0 0 8px;">第
			${page} 页/共 ${ pagecount } 页</span>
	</c:if>

	<br> <br> <br>
</div>
<div id="showFriend" style="display: none">
	<div id="check">
		全部:<input type="radio" value="All" id="radio2" name="state">
		单个:<input type="radio" value="Only" id="radio1" name="state"
			checked="checked"> <input type="button" value="分享"
			id="shareBtn">
	</div>
</div>


<script>
	PAGE.renameFile = function() {

		var filetype = $("input:checkbox:checked").parent().parent().next(
				".filetype").val();
		var files = PAGE.getCheckedList("#su_file_list>label>div>input");
		if (files.length != 1) {

			mdui.alert("选择的文件数量不正确，有且只能选择一项文件");
			return;

		}

		mdui.prompt("新文件名:", "重命名文件", function(newfilename) {
			$.post("./renameFile", {
				oldname : files[0],
				newname : newfilename,
				filetype : filetype,
				secondId : $(".secondid").val()
			}, function(result) {
				layer.msg(result.msg, {
					time : 2000
				//2秒关闭（如果不配置，默认是3秒）
				}, function() {
					window.location.reload();
				});
			});
		});
	}

	PAGE.deleteFiles = function() {
		var files = PAGE.getCheckedList("#su_file_list>label>div>input");
		var c = [];
		$("input:checkbox:checked").each(function() {
			c.push($(this).parent().parent().next(".filetype").val());
		});

		if (files.length <= 0) {
			mdui.alert("请至少选择一个文件进行删除！");
			return;
		}

		$.ajax({
			url : "./deleteFile",
			data : {
				name : files,
				filetype : c,
				secondId : $(".secondid").val(),
				userId : ${user.userid}
			},
			type : "post",
			traditional : true,
			success : function(data) {
				window.location.href = "./directory?secondId="
						+ $(".secondid").val() + "&userId=${user.userid}";
			}
		});
		/* 
		$.post("./DeleteFile", {
			name : files,
			filetype : c,
			secondId : $(".secondid").val()
		}, function(result) {
			window.location.reload();
		}); */
	}

	PAGE.downloadFile = function() {

		var files = PAGE.getCheckedList("#su_file_list>label>div>input");
		var filetype = PAGE.getCheckedList("#su_file_list>label+input");
		if (files.length <= 0) {
			mdui.alert("请至少选择一个文件进行下载！");
			return;
		}
		
		window.location.href = "./downloadFile?fileName=" + files
				+ "&secondId=" + $(".secondid").val();
	}

	//分享文件的id
	var fileids = [];
	//分享文件的父目录id
	var secondIds = [];
	//分享文件的名字
	var filenames = [];
	//打开的层
	var index1 = null;
	
	var type = [];
	
	
	//分享文件
	$('.mdui-text-color-green').click(function() {
		$("input:checkbox:checked").each(function() {
			type.push($(this).parent().parent().nextAll().eq(0).val());
		});
		console.log("type"+type);
						filenames = PAGE
								.getCheckedList("#su_file_list>label>div>input");
						//var filesId= $(this).parent().next("#su_file_list").children(".fileid").val();
						$("input:checkbox:checked").each(
								function() {
									secondIds.push($(this).parent().parent()
											.nextAll().eq(1).val());
									fileids.push($(this).parent().parent()
											.nextAll().eq(2).val());
								});
						
						//加载好友列表
						$.ajax({
									type : "get",
									url : "${pageContext.request.contextPath}/getFriendsUser",
									data : {
										userid : ${user.userid},
										content : ""
									},
									success : function(data) {
										$('#check').nextAll('div').remove();
										$
												.each(
														data,
														function(index, user) {
															$('#check')
																	.after(
																			'<div class="shareFriend">'
																					+ '<span class="friendName">'
																					+ user.username
																					+ '</span>'
																					+ "      "
																					+ '<input type="checkbox" value='+user.userid+' name="friend">'
																					+ '</div>')
														})
									}
								});
						if (filenames.length <= 0) {
							mdui.alert("请至少选择一个文件进行分享！");
							return;
						} else {
							$.each(type,function(index,obj){
								if(type[0]==0){
									layer.msg("目录不支持分享")
								}else{
									index1 = layer.open({
										title : "选择好友",
										type : 1,
										area : [ "240px", "400px" ],
										content : $("#showFriend")
									});
									
								}
							});
						}
						type=[];
					});

	//选择好友分享事件
	$(document).on('click', '#shareBtn', function() {
		//分享的状态
		var boolCheck = $('input:radio[name="state"]:checked').val();
		//var friends = ("input[name='friend']:checked");
		//分享的好友
		var friendID = [];
		$("input[name='friend']:checked").each(function(i) {//把所有被选中的复选框的值存入数组
			friendID[i] = $(this).val();
		});
		if(friendID.length!=0){
		$.ajax({
			type : "post",
			url : "${pageContext.request.contextPath}/addShare",
			data : {
				"fileids" : fileids,
				"secondIds" : secondIds,
				"filenames" : filenames,
				"touserid" : friendID,
				"fromuserid" : ${user.userid},
				"sharestate" : boolCheck
			},
			traditional : true,
			success : function(result) {
				
				fileids = [];
				secondIds = [];
				filenames = [];
				layer.close(index1);
				layer.msg(result.msg, {
					time : 2000
				//2秒关闭（如果不配置，默认是3秒）

				});
			}

		});
		}else{
			layer.msg("请选择好友", {
				time : 2000
			//2秒关闭（如果不配置，默认是3秒）

			});
		}
	});
</script>


<script type="text/javascript">
	$(function() {
		function GetQueryString(name) {
			var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
			var r = window.location.search.substr(1).match(reg);
			if (r != null)
				return unescape(r[2]);
			return null;
		}
		if (GetQueryString("secondId") == null) {
			$("#newfolder").css("display", "none");
			$("#history").css("display", "none");
		}
		$("#history").on("click",function() {
							if ($(".secondid").val() == null) {
								window.location.href = "${pageContext.request.contextPath}/getSecondId?secondId="
										+ GetQueryString("secondId")
										+ "&userId=${user.userid}";
							} else if ($(".secondid").val() == 0) {

							} else {
								window.location.href = "${pageContext.request.contextPath}/getSecondId?secondId="
										+ $(".secondid").val() + "&userId=${user.userid}";
							}
						});

		//新建文件夹的显示
		$("#newfolder").on("click", function() {
			$("#btnfolder").show();
		});

		//新建的单击事件
		$("#affirm").on("click", function() {
			if ($("#nonefile").length > 0) {
				$.ajax({
					url : "${pageContext.request.contextPath}/addContents",
					data : {
						"secondId" : GetQueryString("secondId"),
						"userId" : ${user.userid},
						"contentsName" : $("#foldername").val()
					},
					type : "get",
					success : function(data) {
						layer.msg(data.msg, {
							time : 2000
						//2秒关闭（如果不配置，默认是3秒）
						}, function() {
							window.location.reload();
						});

						$("#nonefile").css("display", "none");
					}
				});
			} else {
				$.ajax({
					url : "${pageContext.request.contextPath}/addContents",
					data : {
						"secondId" : $(".secondid").val(),
						"userId" : ${user.userid},
						"contentsName" : $("#foldername").val()
					},
					type : "get",
					success : function(data) {
						layer.msg(data.msg, {
							time : 2000
						//2秒关闭（如果不配置，默认是3秒）
						}, function() {
							window.location.reload();
						});

						$("#nonefile").css("display", "none");
					}
				});
			}

		});

		//取消的单击事件
		$("#cancel").on("click", function() {
			$("#btnfolder").css("display", "none");
		});

		//被复制的文件名
		var fileName = [];
		//存储被复制的文件的父目录id
		var fromsecondId = null;
		//文件类型
		var type = [];

		//复制的单击事件
		$("#copy").on(
				"click",
				function() {
					$("input:checkbox:checked").each(function() {
						type.push($(this).parent().parent().next(".filetype").val());
						fileName.push($(this).parent().parent().find(".name").text().trim());
					});
					fromsecondId = $(".secondid").val();
					$.cookie('fileName', fileName);
					$.cookie('fromsecondId', fromsecondId);
					$.cookie('type', type);
				});

		//粘贴的单机事件
		$("#paste").on('click', function() {
			if ($("#nonefile").length > 0) {
				var tosecondId = GetQueryString("secondId");
				$.ajax({
					url : "${pageContext.request.contextPath}/copy",
					data : {
						"fromsecondId" : $.cookie('fromsecondId'),
						"userId" : ${user.userid},
						"fromcontentsName" : $.cookie('fileName'),
						"tosecondId" : tosecondId,
						type : $.cookie('type')
					},
					type : "get",
					traditional : true,
					success : function(data) {
						window.location.reload();
					}
				});
			} else {
				var tosecondId = $(".secondid").val();
				$.ajax({
					url : "${pageContext.request.contextPath}/copy",
					data : {
						"fromsecondId" : $.cookie('fromsecondId'),
						"userId" : ${user.userid},
						"fromcontentsName" : $.cookie('fileName'),
						"tosecondId" : tosecondId,
						type : $.cookie('type')
					},
					type : "get",
					success : function(data) {
						window.location.reload();
					}
				});
			}
		});

		//搜索的单击事件
		$("#seach")
				.on(
						"click",
						function() {
							window.location.href = "./seach?userId=${user.userid}&content="
									+ $("#content").val();
						});

		if (GetQueryString("secondId") == 0 || $(".secondid").val() == 0) {
			$("#history").css("display", "none");
		}

		//双击事件
		$(".catalogue").dblclick(
				function() {
					window.location.href = "./directory?secondId="
							+ $(".contentsid").val() + "&userId=${user.userid}";
				}); 
		
		//音乐的双击事件
		$(".openmusic").dblclick(function(){
				
			$("audio:first").attr("src", $(this).find(".filepath").val());
			layer.open({

				title : $(this).find(".filename").val() ,
				type : 1,
				content : $("#musicbox")
				})
			});
		
		//视频的双击事件
		$(".openmovie").dblclick(function(){
			$("video:first").attr("src", $(this).find(".moviefilepath").val());
			layer.open({
				title : $(this).find(".moviefilename").val(),
				type : 1,
				content : $("#moviebox")
			})
		})
		
		//图片的双击事件
		$(".openpicture").dblclick(function(){
			$("#picturedemo").attr("src", $(this).find(".picturefilepath").val());

			layer.open({
				title : $(this).find(".picturefilename").val(),
				type : 1,
				content : $("#picturebox")
			})
						
		})
		
		function fun() {
			var c = [];
			var a = $("input:checkbox:checked").parent().parent().next(
					".filetype").val();
			var b = $("input:checkbox:checked").length;

			$("input:checkbox:checked").each(function() {
				c.push($(this).parent().parent().next(".filetype").val());
			});
		}
	})
</script>


<%@include file="footer.jsp"%>
