package com.etc.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.etc.entity.Audit;

@Repository
@Mapper
public interface AuditMapper {
	int deleteByPrimaryKey(Integer fileid);

	int insert(Audit record);

	int insertSelective(Audit record);

	Audit selectByPrimaryKey(Integer fileid);

	int updateByPrimaryKeySelective(Audit record);

	int updateByPrimaryKey(Audit record);

	/**
	 * 模糊查询总审核的数目
	 * 
	 * @param content
	 *            模糊查询的内容
	 * @return 模糊查询审核的总数目
	 */
	int getCount(String content);

	/**
	 * 分页查询审核内容
	 * 
	 * @param page
	 *            当前页
	 * @param limit
	 *            每页的页数
	 * @param content
	 *            模糊查询的内容
	 * @return 用户的一个集合
	 */
	List<Audit> selectAllAuditByPage(@Param(value = "page") Integer page, @Param(value = "limit") Integer limit,
			@Param(value = "content") String content);

	/**
	 * 根据用户id查找
	 * 
	 * @param secondId
	 * @return 文件集合
	 */
	List<Audit> getMyAudit(@Param(value = "userId") Integer userId);

	Audit selectByName(@Param(value = "userId") Integer userId, @Param(value = "fileName") String fileName);
}