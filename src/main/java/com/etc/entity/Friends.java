package com.etc.entity;

import java.io.Serializable;

/**
 * 好友类
 * 
 * @author China 20192019年5月24日上午9:14:50
 */
public class Friends implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// 普通主键，无实质内涵
	private Integer friendsId;
	// 发送好友申请的用户Id
	private Integer fromuserid;
	// 接收好友申请的用户Id
	private Integer touserid;
	// 好友关系状态，1代表好友关系，2代表添加好友(发送申请但未回应)
	private Integer relationstate;

	public Integer getFriendsId() {
		return friendsId;
	}

	public void setFriendsId(Integer friendsId) {
		this.friendsId = friendsId;
	}

	public Integer getFromuserid() {
		return fromuserid;
	}

	public void setFromuserid(Integer fromuserid) {
		this.fromuserid = fromuserid;
	}

	public Integer getTouserid() {
		return touserid;
	}

	public void setTouserid(Integer touserid) {
		this.touserid = touserid;
	}

	public Integer getRelationstate() {
		return relationstate;
	}

	public void setRelationstate(Integer relationstate) {
		this.relationstate = relationstate;
	}

	@Override
	public String toString() {
		return "Friends [friendsId=" + friendsId + ", fromuserid=" + fromuserid + ", touserid=" + touserid
				+ ", relationstate=" + relationstate + "]";
	}

}