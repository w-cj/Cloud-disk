package com.etc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * 专门做页面跳转的控制器
 * @author zeng
 *
 */
@Controller
public class skipController {

	/**
	 * 跳转到用户列表页面
	 * @return  页面名
	 */
	@RequestMapping("userlist")
	public String tea() {
		return "userlist";
	}
	
	/**
	 * 跳转去后台主页
	 * @return  页面名
	 */
	@RequestMapping("index")
	public String index() {
		return "indexs";
	}
	
	/**
	 * 跳转到管理员列表
	 * @return 页面名
	 */
	@RequestMapping("adminlist")
	public String listAdmin() {
		return "adminlist";
	}
	
	/**
	 * 跳转到图片列表
	 * @return 页面名
	 */
	@RequestMapping("picture")
	public String picturelist() {
		return "picturelist";
	}
	
	/**
	 * 跳转到音乐列表
	 * @return 页面名
	 */
	@RequestMapping("music")
	public String musiclist() {
		return "musiclist";
	}
	
	/**
	 * 跳转到电影列表
	 * @return 页面名
	 */
	@RequestMapping("movie")
	public String movielist() {
		return "movielist";
	}
	
	/**
	 * 跳转到文件列表
	 * @return 页面名
	 */
	@RequestMapping("tmp")
	public String tmplist() {
		return "tmplist";
	}
	
	/**
	 * 跳转到后台登录
	 * @return 页面名
	 */
	@RequestMapping("backlogin")
	public String backlogin() {
		return "login";
	}
	
	/**
	 * cyf转发到主界面的控制器
	 * @return 主界面文件名
	 */
	@RequestMapping("toSystem")
	public String toSystem() {	
		return "system";
	}
	
	/**
	 * 跳转到关于我们页面
	 * @return 页面名
	 */
	@RequestMapping("about")
	public String aboutWe() {
		return  "about" ;
 	}
	
	/**
	 * 跳转到审核列表
	 * @return 页面名
	 */
	@RequestMapping("audit")
	public String auditlist() {
		return "auditlist";
	}
	
}
