package com.etc.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.etc.entity.FileSort;
/**
 * 分页查询每种文件类型的dao层
 * @author Administrator
 *
 */
@Repository
@Mapper
public interface FileSortMapper {
	
    
    /**
     * 分页查询图片
     * @param page  当前页
     * @param limit  每页的页数
     * @param content  模糊查询的内容
     * @return PageData的对象
     */
    List<FileSort>  selectPicture(@Param(value = "page") Integer page, @Param(value = "limit") Integer limit,
             @Param(value = "content") String content); 
    
    
    /**
     * 分页查询音乐
     * @param page  当前页
     * @param limit  每页的页数
     * @param content  模糊查询的内容
     * @return  PageData的对象
     */
    List<FileSort>  selectMusic(@Param(value = "page") Integer page, @Param(value = "limit") Integer limit,
             @Param(value = "content") String content); 
    
    /**
     * 分页查询视频
     * @param page  当前页
     * @param limit  每页的页数
     * @param content  模糊查询的内容
     * @return  PageData的对象
     */
    List<FileSort>  selectMovie(@Param(value = "page") Integer page, @Param(value = "limit") Integer limit,
             @Param(value = "content") String content); 
    
    
    /**
     * 分页查询文档
     * @param page  当前页
     * @param limit  每页的页数
     * @param content  模糊查询的内容
     * @return  PageData的对象
     */
    List<FileSort>  selecttmp(@Param(value = "page") Integer page, @Param(value = "limit") Integer limit,
             @Param(value = "content") String content); 
    

    
    
    /**
     * 模糊查询总数目
     * @param content  模糊查询的内容
     * @return  模糊查询的总数目
     */
 	int getCount(String content);
}