package com.etc.service;

import java.util.List;

import com.etc.dao.ShareUserMapper;

/**
 * 分享的service层
 * 
 * @author zeng
 *
 */
public interface ShareUserService {

	/**
	 * 查询我的分享记录
	 * 
	 * @param fromuserid
	 *            分享者的id
	 * @return 返回一个用户分享的集合
	 */
	public List<ShareUserMapper> getShareByFromid(Integer fromuserId);

	/**
	 * 返回一个好友分享记录的集合
	 * 
	 * @param touserid
	 *            被分享者的id
	 * @param fromuserid
	 *            登录用户的id
	 * @return 返回一个用户分享的集合
	 */
	public List<ShareUserMapper> getShareByToid(Integer touserId, Integer fromuserId);

	/**
	 * 分页查询
	 * 
	 * @param page
	 *            页码
	 * @param pageSize
	 *            页数
	 * @param content
	 *            模糊查询关键字
	 * @return 返回一个集合的分享记录
	 */
	public List<ShareUserMapper> getShareByPage(Integer page, Integer pageSize, String content, Integer fromuserId);
}
