﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>login</title>
<link rel="stylesheet" type="text/css" href="css/normalize.css">
<link rel="stylesheet" type="text/css" href="css/demo.css">
<link rel="stylesheet" href="js/vendor/jgrowl/css/jquery.jgrowl.min.css">
<!--必要样式-->
<link rel="stylesheet" type="text/css" href="css/component.css">

<link rel="stylesheet" href="layui/css/layui.css" media="all">

<script src="js/jquery.js"></script>
<script src="layui/layui.js" charset="utf-8"></script>
<script type="text/javascript" src="layer/2.1/layer.js"></script>

<!--[if IE]>
<script src="js/html5.js"></script>
<![endif]-->
<style>
input::-webkit-input-placeholder {
	color: rgba(0, 0, 0, 0.726);
}

input::-moz-placeholder { /* Mozilla Firefox 19+ */
	color: rgba(0, 0, 0, 0.726);
}

input:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
	color: rgba(0, 0, 0, 0.726);
}

input:-ms-input-placeholder { /* Internet Explorer 10-11 */
	color: rgba(0, 0, 0, 0.726);
}
</style>
</head>
<body>
	<div class="container demo-1">
		<div class="content">
			<div id="large-header" class="large-header">
				<canvas id="demo-canvas"></canvas>
				<div class="logo_box">
					<h3>欢迎登陆</h3>
					<form action="" name="f" method="post">
						<div class="input_outer">
							<span class="u_user"></span> <input id="username" name="username"
								class="text" style="color: #000000 !important" type="text"
								placeholder="请输入用户名">
						</div>
						<div class="input_outer">
							<span class="us_uer"></span> <input id="userpwd" name="userpwd"
								class="text"
								style="color: #000000 !important; position: absolute; z-index: 100;"
								value="" type="password" placeholder="请输入密码">
						</div>
						<div id="LOGIN" class="mb2">
							<input type="button" class="act-but submit" id="tijiao"
								style="color: #FFFFFF; width: 335px" value="登录">
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- /container -->
	<script src="js/TweenLite.min.js"></script>
	<script src="js/EasePack.min.js"></script>
	<script src="js/jquery.js"></script>
	<script src="js/rAF.js"></script>
	<script src="js/demo-1.js"></script>
	<script src="js/vendor/jgrowl/jquery.jgrowl.min.js"></script>
	<script src="js/Longin.js"></script>
	<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
	<script src="layui/layui.js" charset="utf-8"></script>
	<script>
		$(function() {
			$("#username").blur(
					function() {

						$.get("loginVilidate", "username="
								+ $("#username").val(), function(data, status) {

							//tips层
							/* alert(data.msg) */
							layui.use([ 'laypage', 'layer', ], function() {

								var laypage = layui.laypage,

								layer = layui.layer;

								layer.tips(data.msg, '#username')

							});

						});

					});
		})
	</script>

	<script type="text/javascript">
		layui.use('table', function(data) {
			$("#tijiao").click(function() {
				$.ajax({
					type : "post",
					url : "login",
					data : {
						username : $("#username").val(),
						userpwd : $("#userpwd").val()
					},
					success : function(cm) {
						if (cm.msg == "登录成功") {
							layer.msg(cm.msg, {
								 icon : 6, 
								time : 1000
							}, function() {
								window.location.href = "index";
							})
						} else {
							layer.msg(cm.msg, {
								icon : 5,
								time : 1000
							}, function() {
								location.reload();
							})

						}

					}
				})
			})

		})
	</script>
	<div style="text-align: center;"></div>
</body>
</html>