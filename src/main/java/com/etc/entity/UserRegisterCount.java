package com.etc.entity;

import java.io.Serializable;

/**
 * 统计用户注册量量表
 * 
 * @author Administrator
 *
 */

public class UserRegisterCount implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// 用户注册的时间
	private String time;
	// 注册数量
	private Integer count;

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public UserRegisterCount(String time, Integer count) {
		super();
		this.time = time;
		this.count = count;
	}

	public UserRegisterCount() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "UserRegisterCount [time=" + time + ", count=" + count + "]";
	}

}
