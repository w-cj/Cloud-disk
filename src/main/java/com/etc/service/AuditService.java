package com.etc.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.etc.entity.Audit;
import com.etc.util.PageData;

/**
 * 审核的service层
 * 
 * @author zeng
 *
 */
public interface AuditService {

	/**
	 * 分页查询用户
	 * 
	 * @param page
	 *            当前页
	 * @param limit
	 *            每页的页数
	 * @param content
	 *            模糊查询的内容
	 * @return
	 */
	public PageData<Audit> selectAllAuditByPage(int page, int limit, String content);

	/**
	 * 添加待审核文件到数据库
	 * 
	 * @param record
	 *            需要审核的文件
	 * @return 添加的结果
	 */
	public boolean insertSelective(Audit record);

	/**
	 * 根据主键修改审核信息
	 * 
	 * @param record
	 *            审核对象
	 * @return 修改结果
	 */
	public boolean updateByPrimaryKeySelective(Audit record);

	/**
	 * 根据文件ID查询审核对象
	 * 
	 * @param fileId
	 *            文件ID
	 * @return 审核对象
	 */
	public Audit selectByPrimaryKey(Integer fileId);

	/**
	 * 根据用户id查找
	 * 
	 * @param secondId
	 * @return 文件集合
	 */
	public List<Audit> getMyAudit(@Param(value = "userId") Integer userId);

	/**
	 * 通过文件名和用户ID查询审核对象
	 * 
	 * @param userId
	 *            用户ID
	 * @param name
	 *            文件名
	 * @return 审核对象
	 */
	public Audit selectByName(Integer userId, String name);

	/**
	 * 删除审核对象
	 * 
	 * @param fileId
	 *            文件ID
	 * @return 删除结果
	 */
	public boolean deleteByPrimaryKey(Integer fileId);

}
