package com.etc.entity;

import java.io.Serializable;

public class ShareUser implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// 用户名
	private String username;
	// 分享记录id
	private Integer shareid;
	// 分享者
	private Integer fromuserid;
	// 被分享者
	private Integer touserid;
	// 分享地址
	private String shareaddress;
	// 分享状态
	private String sharestate;
	// 分享的文件的Id
	private Integer fileid;
	// 分享的时间
	private String sharetime;
	// 分享的文件名字
	private String filename;
	// 分享的路径
	private String filepath;
	// 获取文件的父目录
	private Integer secondId;

	public Integer getSecondId() {
		return secondId;
	}

	public void setSecondId(Integer secondId) {
		this.secondId = secondId;
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public Integer getFileid() {
		return fileid;
	}

	public void setFileid(Integer fileid) {
		this.fileid = fileid;
	}

	public String getSharetime() {
		return sharetime;
	}

	public void setSharetime(String sharetime) {
		this.sharetime = sharetime;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username == null ? null : username.trim();
	}

	public Integer getShareid() {
		return shareid;
	}

	public void setShareid(Integer shareid) {
		this.shareid = shareid;
	}

	public Integer getFromuserid() {
		return fromuserid;
	}

	public void setFromuserid(Integer fromuserid) {
		this.fromuserid = fromuserid;
	}

	public Integer getTouserid() {
		return touserid;
	}

	public void setTouserid(Integer touserid) {
		this.touserid = touserid;
	}

	public String getShareaddress() {
		return shareaddress;
	}

	public void setShareaddress(String shareaddress) {
		this.shareaddress = shareaddress == null ? null : shareaddress.trim();
	}

	public String getSharestate() {
		return sharestate;
	}

	public void setSharestate(String sharestate) {
		this.sharestate = sharestate == null ? null : sharestate.trim();
	}

	@Override
	public String toString() {
		return "ShareUser [username=" + username + ", shareid=" + shareid + ", fromuserid=" + fromuserid + ", touserid="
				+ touserid + ", shareaddress=" + shareaddress + ", sharestate=" + sharestate + ", fileid=" + fileid
				+ ", sharetime=" + sharetime + ", filename=" + filename + ", filepath=" + filepath + ", secondId="
				+ secondId + "]";
	}

}