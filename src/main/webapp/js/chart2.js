$(function() {
	/*
	 * ChartJS ------- Data and config for chartjs
	 */
	'use strict';

	var time = [];
	var count = [];

	
	$.ajax({

		type : "get",
		url : "getUsercount",
		dataType : "json",
		success : function(data) {

			$.each(data, function(index, obj) {

				time.push(obj.time)
				count.push(obj.count);
				
			});

			var comit = {
				labels : time,
				datasets : [ {
					label : '个数/个',
					data : count,
					barWidth:10,
					backgroundColor : [ 'rgba(255, 99, 132, 0.2)',
							'rgba(54, 162, 235, 0.2)',
							'rgba(255, 206, 86, 0.2)',
							'rgba(75, 192, 192, 0.2)',
							'rgba(153, 102, 255, 0.2)',
							'rgba(255, 159, 64, 0.2)',
							'rgba(255, 99, 132, 0.2)',
							 ],
					borderColor : [ 'rgba(255,99,132,1)',
							'rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)',
							'rgba(75, 192, 192, 1)', 'rgba(153, 102, 255, 1)',
							'rgba(255, 159, 64, 1)', 'rgba(255,99,132,1)'],
					borderWidth : 1
					
				} ]
			};

			var options = {
				scales : {
					yAxes : [ {
						ticks : {
							beginAtZero : true
						}
					} ]
				},
				
				legend : {
					display : false
				},
				
				elements : {
					
					point : {
						radius : 0
						
					}
				}

			};
			
			// Get context with jQuery - using jQuery's .get() method.
			if ($("#barChart").length) {
				var barChartCanvas = $("#barChart").get(0).getContext("2d");
				// This will get the first returned node in the jQuery
				// collection.
				var barChart = new Chart(barChartCanvas, {
					type : 'bar',
					data : comit,
					
					options : options
				});
			}

			
		}
	});
});
