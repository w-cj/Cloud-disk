package com.etc.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.etc.entity.ShareUser;

@Mapper
@Repository
public interface ShareUserMapper {
    int insert(ShareUser record);

    
    /**
     * 查询我的分享记录
     * @param fromuserid 分享者的id
     * @return 返回一个用户分享的集合
     */
    List<ShareUserMapper> selectShareByFromid(@Param("fromuserid") Integer fromuserid);
    
    /**
     * 返回一个好友分享记录的集合
     * @param touserid 被分享者的id
     * @param fromuserid 登录用户的id
     * @return 返回一个用户分享的集合
     */
    List<ShareUserMapper> selectShareByToid(@Param("touserid") Integer touserid,@Param("fromuserid") Integer fromuserid);
    
    /**
     * 分页查询
     * @param page 页码
     * @param pageSize 页数
     * @param content 模糊查询关键字
     * @return 返回一个集合的分享记录
     */
    List<ShareUserMapper> getShareByPage(@Param("start")Integer start , @Param("pageSize")Integer pageSize, @Param("content")String content ,@Param("fromuserid") Integer fromuserid);
}