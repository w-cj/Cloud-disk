<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="zh-CN" />
	<meta name="renderer" content="webkit" />
	<meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no" />

	<title>验证 | Linkresources</title>

	<link href="./static/template/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="./static/login/css/style.css" rel="stylesheet">
	<link href="./static/common/image/icon.ico" rel="shortcut icon">
	<!--[if lt IE 9]>
        <div id='Not_' class="show-ui" style="height: 40px;background-color: rgb(221,79,67);text-align:center;line-height: 40px;color: white;">
            <b>无法访问! 您的浏览器版本过低或是兼容模式,请使用最新/更高版本的浏览器: IE10+ chrome FireFox 等，如果是国内浏览器请打开极速浏览模式（webkit内核）。</b>
        </div>
    <![endif]-->
</head>

<body>

	<div class="mvideocon" style="ospacity: 0.95;">
        <video class="mvideo" type="video/mp4" preload="metadata" loop="" autoplay="" muted="" poster="https://web.ccpgamescdn.com/eveonlineassets/bgvid/video-bg-image.jpg">
            <source src="static/bg.mp4" autostart="false">
        </video>
    </div>
    
	<div id='login-view' class="show-ui">
		<div id='login-sub'>
			<div class="container-fluid">
				<div class="row">
					<img id='logo' src="static/common/logo.png">
					<br>
					<span style="color: red;" class="ft-size-12 ft-color-black ft-text-index">账号或密码错误，请重试</span> 
				</div>
				<div id='login-form' class="row">
					<!-- 表单开始 -->
					<form class="form-horizontal" role="form" id="loginAction_home" name="login_form" action="${pageContext.request.contextPath}/Login" target="_parent" method="post">
						<div class="form-group has-feedback">
							<div class="col-sm-12">
								<input type="text" class="form-control border-1 minput" name="login-userid" placeholder="用户账号" autocomplete="off">
								<span class="glyphicon glyphicon-user form-control-feedback" aria-hidden="true"></span>

							</div>
						</div>
						<div class="form-group has-feedback">
							<div class="col-sm-12">
								<input type="password" class="form-control border-1 minput" name="login-passwd" placeholder="密码" autocomplete="off">
								<span class="glyphicon glyphicon-th form-control-feedback" aria-hidden="true"></span>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-4 col-sm-6"></div>
							<div class="col-xs-8 col-sm-6">
								<a href="register.jsp" id="form-help" class="text-right help-text" >没有账号？立即注册！</a>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<div id='login-button' class="btn btn-default btn-info border-1" onclick="btnLogin()">
									<b>验证</b>
								</div>
							</div>
						</div>
					</form>
					<!-- 表单结束 -->
				</div>
			</div>
		</div>
	</div>
	<div id='view-info' class="alert alert-info show-ui tools-info">默认信息框</div>
</body>
<script type="text/javascript" src="./static/template/jq/jq.min.js"></script>
<script type="text/javascript" src="../common/URL.js"></script>
<script type="text/javascript" src="../common/md5.js"></script>
<script type="text/javascript" src="../common/js/login.js"></script>
<script type="text/javascript">
	function btnLogin() {
		$("#loginAction_home").submit();
	}
</script>


</html>