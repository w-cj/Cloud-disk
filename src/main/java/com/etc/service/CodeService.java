package com.etc.service;

import com.etc.entity.Code;

/**
 * 短信验证码 service层
 * 
 * @author Administrator
 *
 */
public interface CodeService {
	/**
	 * 增加短信验证码对象
	 * 
	 * @param record
	 *            短信验证码对象
	 * @return 增加结果
	 */
	public boolean insert(Code record);

	/**
	 * 通过用户手机号码查询验证码对象
	 * 
	 * @param userPhone
	 *            用户手机号码
	 * @return 验证码对象
	 */
	public Code selectByUserPhone(String userPhone);

	/**
	 * 删除用户手机号码
	 * 
	 * @param userPhone
	 *            用户手机号码
	 * @return 删除结果
	 */
	public int deleteByUserPhone(String userPhone);

	/**
	 * 根据验证码id修改验证码信息
	 * 
	 * @param record
	 *            短信对象
	 * @return 修改结果
	 */
	public boolean updateByPrimaryKey(Code record);
}
