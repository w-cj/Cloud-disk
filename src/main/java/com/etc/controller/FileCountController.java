package com.etc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.etc.entity.FileCount;
import com.etc.service.FileCountService;

@RestController
public class FileCountController {

	@Autowired
	private FileCountService fileCS;

	
	@GetMapping(value="filelist")
	public List<FileCount> getFileCount() {

		List<FileCount> list = fileCS.getFileCount();

		return list;
	}

}
