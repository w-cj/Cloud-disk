package com.etc.service;

import java.util.List;

import com.etc.entity.UserRegisterCount;

/**
 * 统计近一周用户注册的service接口
 * 
 * @author chao
 *
 */
public interface UserRCService {

	/**
	 * 用户近一周每天注册数量
	 * 
	 * @return 每天注册数量的集合
	 */
	public List<UserRegisterCount> getUserCount();

}
