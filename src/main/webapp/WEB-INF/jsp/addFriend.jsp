<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@include file="header.jsp"%>


<div class="mdui-container">

	<div class="mdui-row">
		<div class="mdui-col-md-1"></div>
		<div class="mdui-col-md-10">
			<form  role="form" 
				name="login_form"
				target="_parent" action="findPeople">
				<div style="margin-top: 10%;">
					<div class="mdui-textfield">
						<div class="mdui-textfield">
							<input id="content" name="content" class="mdui-textfield-input" type="text"
								maxlength="18" placeholder="搜索用户" autocomplete="off" />
							<div class="mdui-textfield-helper">搜索支持模糊查询</div>
						</div>
						<button onclick="getUser()" id="search" class="mdui-btn mdui-color-purple-400 mdui-ripple" type="button">搜索</button>
					</div>
				</div>
			</form>
		</div>
		<div class="mdui-col-md-1"></div>
	</div>

	<div class="mdui-row">
		<div class="mdui-col-md-1"></div>
		<div class="mdui-col-md-10">
			<div style="margin-top: 30px;" id="divshow">
				<%-- <c:forEach items="${users}" varStatus="i" var="item">
					<div class="mdui-chip su-user-list">
						<span class="mdui-chip-icon mdui-text-uppercase">${ item.username.toCharArray()[0] }</span>
						<span class="mdui-chip-title">${ item.username }</span>
						<span class="mdui-chip" id="add">添加</span>
					</div>
				</c:forEach> --%>
			</div>
		</div>
		<div class="mdui-col-md-1"></div>
	</div>

</div>

<script src="layer/2.1/layer.js"></script>
<script>

	var content = null;
	var getUser = function(){
		if($('#content').val()==""){
			content = $('#content').val();	
		}
		//查找陌生人添加好友
		$.ajax({
			url:"findPeople?userid=${user.userid}&content="+$('#content').val(),
			type:"get",
			success:function(data){
				//先移除原来的
				$("#divshow").children('div').remove();
				//在后面添加div
				$.each(data,function(index,data1){
					if(data1.username!='${user.username}'){
						$("#divshow").append('<div class="mdui-chip su-user-list" style="margin:5px">'+'<span class="mdui-chip-icon mdui-text-uppercase">'+'</span>'+'<span class="mdui-chip-title">'+data1.username+'</span>'+'<button class="adddiv btn-default" style="text-align:center;margin:5px" type="button" title='+data1.userid+'>'+'点我添加'+'</button>'+'</div>')
					}
					
				});
				
			}
		});
	}
	
	//发送好友请求好友的方法
	$(document).on('click','.adddiv',function(){
		var touserid = $(this).prop('title');
		var fromuserid = ${user.userid};
		$.ajax({
			url:"sendFriendsReq",
			type:"post",
			data:{"fromuserid":fromuserid,"touserid":touserid,"relationstate":"2"},
			success:function(cm){
				layer.msg(cm.msg,
						{
					icon : 1, //图标		
					time : 2000
				//2秒关闭（如果不配置，默认是3秒）
				}
				);
			}
		});
	});

	
</script>

<%@include file="footer.jsp"%>
