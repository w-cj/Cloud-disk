package com.etc.service;

import java.util.List;

import com.etc.entity.Contents;

/**
 * 目录的service层
 * 
 * @author zeng
 *
 */
public interface ContentsService {

	/**
	 * 根据上一级目录的id获取该目录所拥有的文件
	 * 
	 * @param secondId
	 *            上一级目录ID
	 * @param userId
	 *            用户ID
	 * @return 所有目录
	 */
	public List<Contents> getContentsBySecondId(Integer secondId, Integer userId);

	/**
	 * 添加目录
	 * 
	 * @param contents
	 *            目录对象
	 * @return 增加结果
	 */
	public boolean addContents(Contents contents);

	/**
	 * 根据上一级目录的id来查找上一级目录的再上一级目录的id
	 * 
	 * @param secondId
	 *            上一级目录ID
	 * @return 目录ID
	 */
	public Integer getSecondIdBySecondId(Integer secondId);

	/**
	 * 重命名
	 * 
	 * @param oldname
	 *            修改前的目录名
	 * @param newfilename
	 *            要修改为的目录名
	 * @param secondId
	 *            上一级目录ID
	 * @return 修改结果
	 */
	public boolean reName(String oldName, String newFileName, Integer secondId);

	/**
	 * 删除目录
	 * 
	 * @param secondId
	 * @param contentsName
	 * @return 删除结果
	 */
	public boolean deleteContents(Integer secondId, String contentsName);

	/**
	 * 根据上一级目录获取上一级目录路径
	 * 
	 * @param secondId
	 *            上一级目录id
	 * @return 上一级目录路径
	 */
	public String getContentsPathById(Integer secondId);

	/**
	 * 增加目录
	 * 
	 * @param contents
	 *            目录对象
	 * @return 增加结果
	 */
	public int insertSelectiveGetKey(Contents contents);

	/**
	 * 根据上一级目录id和目录名得到当前目录
	 * 
	 * @param secondId
	 *            上一级目录ID
	 * @param contentsName
	 *            目录名
	 * @return 目录对象
	 */
	public Contents selectBySecondIdAndContentsName(Integer secondId, String contentsName);

	/**
	 * 根据目录ID查询目录
	 * 
	 * @param contentsid
	 *            目录ID
	 * @return 目录
	 */
	public Contents selectByPrimaryKey(Integer contentsId);

	/**
	 * 根据上一级目录的id和模糊查询名字获取该目录所拥有的目录
	 * 
	 * @param userId
	 *            用户ID
	 * @param content
	 *            模糊查询的条件
	 * @return 目录对象的集合
	 */
	public List<Contents> getContentsByLike(Integer userId, String content);
}
