package com.etc.service;

import com.etc.entity.Share;

/**
 * 好友分享的service层
 * 
 * @author zeng
 *
 */
public interface ShareService {

	/**
	 * 删除一条分享
	 * 
	 * @param shareid
	 *            分享的id
	 * @return 受影响行数
	 */
	public boolean deleteShareById(Integer shareId);

	/**
	 * 添加一条分享记录
	 * 
	 * @param share
	 *            分享对象
	 * @return 受影响行数
	 */
	public boolean addShare(Share share);

	/**
	 * 查询分享数
	 * 
	 * @return int类型的分享数
	 */
	public Integer selectShare();
}
