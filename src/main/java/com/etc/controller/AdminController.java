package com.etc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.etc.entity.User;
import com.etc.service.UserService;
import com.etc.util.PageData;
/**
 * 管理员
 * @author China
 *20192019年5月24日上午10:19:21
 */
@RestController
public class AdminController {

	@Autowired
	private UserService userService;

	/**
	 * 分页查询所有用户
	 * 
	 * @param page
	 *            第几页
	 * @param limit
	 *            每页的数目
	 * @param content
	 *            模糊查询的内容
	 * @return
	 */
	@GetMapping(value = "listAdmin")
	public PageData<User> getAllByPage(int page, int limit, String content) {

		if (content == null) {
			content = "";
		}
		PageData<User> pd = userService.selectAllAdminiByPage(page, limit, content);
		pd.setCode(0);
		pd.setMsg("请求成功");

		return pd;
	}
	
}
