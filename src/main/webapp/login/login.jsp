<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="zh-CN" />
<meta name="renderer" content="webkit" />
<meta name="viewport"
	content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no" />

<title>前台登录 | Linkresources</title>

<link href="./static/template/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="./static/login/css/style.css" rel="stylesheet">
<link href="./static/common/image/icon.ico" rel="shortcut icon">
<!--[if lt IE 9]>
        <div id='Not_' class="show-ui" style="height: 40px;background-color: rgb(221,79,67);text-align:center;line-height: 40px;color: white;">
            <b>无法访问! 您的浏览器版本过低或是兼容模式,请使用最新/更高版本的浏览器: IE10+ chrome FireFox 等，如果是国内浏览器请打开极速浏览模式（webkit内核）。</b>
        </div>
    <![endif]-->
</head>

<body>
	<div class="mvideocon" style="">
		<video class="mvideo" type="video/mp4" preload="metadata" loop=""
			autoplay="" muted="" poster="">
			<source src="static/bg.mp4" autostart="false">
		</video>
	</div>
	<iframe class="phonenot" frameborder="no" border="0" marginwidth="0"
		marginheight="0" width=330 height=86
		src="//music.163.com/outchain/player?type=2&id=1933561&auto=1&height=66"></iframe>
	<div id='login-view' class="show-ui" style="opacity: 0.84;">
		<div id='login-sub'>
			<div class="container-fluid">
				<div class="row">
					<img id='logo' src="static/common/logo.png"> <br> <span
						class="ft-size-12 ft-color-black ft-text-index">${ msg }</span>
				</div>
				<div id='login-form' class="row">
					<!-- 表单开始 -->
					<form class="form-horizontal" role="form" id="loginAction_home"
						name="login_form"
						action="" target="_parent"
						method="post">
						<!--用户名校验提示 -->
						<div class="group" id="gaution" style="color:red;opacity:0;">
							该用户不存在！！！
						</div>
						<!--用户名校验提示结束 -->
						<div class="form-group has-feedback">
							<div class="col-sm-12">
								<input type="text" class="form-control border-1 minput"
									id="username" name="username" placeholder="用户账号"
									autocomplete="off"> <span
									class="glyphicon glyphicon-user form-control-feedback"
									aria-hidden="true"></span>

							</div>
						</div>
						<!--密码校验提示 -->
						<div class="group" id="warning" style="color:red;opacity:0;">
							密码有误，请重新输入！！！
						</div>
						<!--密码校验提示结束 -->
						<div class="form-group has-feedback">
							<div class="col-sm-12">
								<input type="password" id="userpwd" class="form-control border-1 minput"
									name="userpwd" placeholder="密码" autocomplete="off">
								<span class="glyphicon glyphicon-th form-control-feedback"
									aria-hidden="true"></span>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-8 col-sm-6" style="margin-left:-50px">
							<a href="phonelogin.jsp" id="form-help1"
									class="text-right help-text">试试手机号登录！</a>
							</div>
							<div class="col-xs-8 col-sm-6" style="margin-left:30px">
								<a href="register.jsp" id="form-help"
									class="text-right help-text">没有账号？立即注册！</a>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<div id='login-button' class="btn btn-danger btn-info border-1">
									<b>验证</b>
								</div>
							</div>
						</div>
					</form>
					<!-- 表单结束 -->
				</div>
			</div>
		</div>
	</div>
	<div id='view-info' class="alert alert-info show-ui tools-info">默认信息框</div>
</body>
<script type="text/javascript" src="./static/template/jq/jq.min.js"></script>
<!--前台登录失焦事件 -->
<script type="text/javascript">
$(function () {
	$("#username").blur(function () {
		$.ajax({
			url:"../checkUserByName",
			type:"GET",
			data:{
				"username":$("#username").val()
			},
			success:function(data){
				if(data.msg=="该用户不存在"){
					//显示警告信息
					$("#gaution").css("opacity",1);
					//清空表单内容
					$("#loginAction_home")[0].reset();
				}else{
					//隐藏警告信息
					$("#gaution").css("opacity",0);
				}
			}
			
		})
		
	})
})

</script>
<!--前台登录失焦事件 -->

<!--前台登录校验 -->
<script type="text/javascript">
	$(function(){
		$("#login-button").click(function () {
			$.ajax({
				url:"../checkUser",
				type:"GET",
				data:{
					"username":$("#username").val(),
					"userpwd":$("#userpwd").val()
				},
			success:function(data){
				if(data.msg=="密码错误"){
					$("#warning").css("opacity",1);
				  }else{
						$("#warning").css("opacity",0);
						location.href="../toSystem";
					}
				
				}
			})
			
		});
	})
	
</script>
<!--前台登录校验 -->

</html>