package com.etc.entity;

import java.io.Serializable;

/**
 * 修改密码时候的三个属性
 * 
 * @author zeng
 *
 */
public class UserPwd implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// 原密码
	private String userPwd;
	// 新密码
	private String userPwd1;
	// 第二次输入的新密码（确认密码）
	private String userPwd2;

	public String getUserPwd() {
		return userPwd;
	}

	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}

	public String getUserPwd1() {
		return userPwd1;
	}

	public void setUserPwd1(String userPwd1) {
		this.userPwd1 = userPwd1;
	}

	public String getUserPwd2() {
		return userPwd2;
	}

	public void setUserPwd2(String userPwd2) {
		this.userPwd2 = userPwd2;
	}

	public UserPwd(String userPwd, String userPwd1, String userPwd2) {
		super();
		this.userPwd = userPwd;
		this.userPwd1 = userPwd1;
		this.userPwd2 = userPwd2;
	}

	public UserPwd() {
		// TODO Auto-generated constructor stub
	}

}
