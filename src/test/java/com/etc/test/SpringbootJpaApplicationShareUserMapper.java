package com.etc.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.etc.dao.ShareMapper;
import com.etc.dao.ShareUserMapper;
import com.etc.entity.Share;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootJpaApplicationShareUserMapper {

	@Autowired
	ShareUserMapper ShareUserMapper;
	
	@Test
	public void selectShareByFromid() {
		//查询我的分享
		List<ShareUserMapper> list = ShareUserMapper.selectShareByFromid(3);
		System.out.println(list);
	}
	
	@Test
	public void selectShareByToid() {
		//查询好友的分享
		List<ShareUserMapper> list = ShareUserMapper.selectShareByToid(2,3);
		System.out.println(list);
	}

}
