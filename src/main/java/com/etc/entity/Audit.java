package com.etc.entity;

import java.io.Serializable;

public class Audit implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer fileid;

	private String filename;

	private String filepath;

	private Integer secondid;

	private String filetime;

	private Integer filetype;

	private Integer userid;

	private String filesize;

	private Integer status;

	private User user;

	public Audit() {
		// TODO Auto-generated constructor stub
	}

	public Audit(Integer fileid, String filename, String filepath, Integer secondid, String filetime, Integer filetype,
			Integer userid, String filesize, Integer status) {
		super();
		this.fileid = fileid;
		this.filename = filename;
		this.filepath = filepath;
		this.secondid = secondid;
		this.filetime = filetime;
		this.filetype = filetype;
		this.userid = userid;
		this.filesize = filesize;
		this.status = status;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Audit [fileid=" + fileid + ", filename=" + filename + ", filepath=" + filepath + ", secondid="
				+ secondid + ", filetime=" + filetime + ", filetype=" + filetype + ", filesize=" + filesize
				+ ", status=" + status + ", user=" + user + "]";
	}

	public Integer getFileid() {
		return fileid;
	}

	public void setFileid(Integer fileid) {
		this.fileid = fileid;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename == null ? null : filename.trim();
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath == null ? null : filepath.trim();
	}

	public Integer getSecondid() {
		return secondid;
	}

	public void setSecondid(Integer secondid) {
		this.secondid = secondid;
	}

	public String getFiletime() {
		return filetime;
	}

	public void setFiletime(String filetime) {
		this.filetime = filetime;
	}

	public Integer getFiletype() {
		return filetype;
	}

	public void setFiletype(Integer filetype) {
		this.filetype = filetype;
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public String getFilesize() {
		return filesize;
	}

	public void setFilesize(String filesize) {
		this.filesize = filesize == null ? null : filesize.trim();
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}