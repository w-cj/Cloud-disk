<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
<!-- Required meta tags -->
<script src="js/jquery-3.3.1.js"></script>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>音乐列表</title>
<!-- plugins:css -->
<link rel="stylesheet"
	href="node_modules/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet"
	href="node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css">

<link rel="stylesheet"
	href="node_modules/jquery-bar-rating/dist/themes/css-stars.css">
<link rel="stylesheet"
	href="node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" />

<link rel="stylesheet" href="css/style.css">
<!-- endinject -->
<link rel="shortcut icon" href="images/favicon.png" />



</head>

<body>


	<div id="box" style="display: none">
		<!--播放内容-->
		<div style="margin: 0px auto;">
			<div class="block no-shadow" style="background-color: #1a1a1a;">
				<!--正文内容-->
				<div class="main-0">
					<div class="player" style="text-align: center;">
						<audio controls="controls">
							<source id="music" type="audio/mp3">
						</audio>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container-scroller">
		<!-- partial:partials/_navbar.html -->
		<%@ include file="head.jsp"%>
		<!-- partial -->
		<div class="container-fluid page-body-wrapper">
			<div class="row row-offcanvas row-offcanvas-right">
				<%@ include file="left.jsp"%>
				<div class="content-wrapper">
					<table class="layui-hide" id="test" lay-filter="test"></table>
				</div>
			</div>
			<!-- row-offcanvas ends -->
		</div>
		<!-- page-body-wrapper ends -->
	</div>
	<!-- container-scroller -->

	<script type="text/html" id="toolbarDemo">
    <div class="layui-btn-container demoTable">

		<button class="layui-btn layui-btn-sm but002" data-type="reload">搜索</button>

		<div class="layui-inline">
			<input class="layui-text" name="id" id="demoReload"
				autocomplete="off">
		</div>

  </div>
</script>
    <!-- 对文件进行播放和删除的按钮 -->
	<script type="text/html" id="barDemo">
  <btton class="btn btn-primary btn-xs btn btn-danger"  lay-event="edit"><i class="mdi mdi-upload"></i>播放</btton>
</script>

	<script>
		layui.use('table', function() {
			
			var table = layui.table;
			
			table.render({
				elem : '#test',
				url : 'listMusic',
				toolbar : '#toolbarDemo',
				title : '音乐数据表',
				cols : [ [ {
					field : 'fileid',
					title : '编号',
					fixed : 'left',

					unresize : true,
					sort : true
				}, {
					field : 'filename',
					title : '音乐文件名'

				}, {
					field : 'filepath',
					title : '文件位置'

				}, {
					field : 'username',
					title : '上传用户'

				}, {
					field : 'filetime',
					title : '上传时间'
				}, {
					field : 'filesize',
					title : '音乐大小'
				}, {
					fixed : 'right',
					title : '操作',
					toolbar : '#barDemo',
					width : 120
				} ] ],
				page : true,
				id : 'testReload'
			});

			//监听行工具事件
			table.on('tool(test)', function(obj) {

				var data = obj.data;
				
				if (obj.event === 'edit') {
					/* 赋值src 不能赋在source内，要赋值在audio中更换音乐的播放才有效 */
					$("audio:first").attr("src", data.filepath);
					layer.open({

						title : data.filename,
						type : 1,
						content : $("#box")
					})

					layui.form.render();
				}
			});
			var $ = layui.$, active = {
				reload : function() {
					var demoReload = $('#demoReload');
					//执行重载
					table.reload('testReload', {
						page : {
							curr : 1
						//重新从第 1 页开始
						},
						where : {
							content : demoReload.val()
						}
					});
				}
			};

			$('.but002').on('click', function() {

				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});

		});
	</script>

	<!-- plugins:js -->
	<script src="node_modules/jquery/dist/jquery.min.js"></script>
	<script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
	<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
	<script
		src="node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js"></script>

	<script src="node_modules/chart.js/dist/Chart.min.js"></script>

	<script src="js/off-canvas.js"></script>
	<script src="js/misc.js"></script>

	<script src="js/dashboard.js"></script>

</body>

</html>