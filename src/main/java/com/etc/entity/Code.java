package com.etc.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class Code implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer codeid;

	private String userphone;

	private Integer code;

	public Code() {
		// TODO Auto-generated constructor stub
	}

	public Code(Integer codeid, String userphone, Integer code) {
		super();
		this.codeid = codeid;
		this.userphone = userphone;
		this.code = code;
	}

	@Override
	public String toString() {
		return "Code [codeid=" + codeid + ", userphone=" + userphone + ", code=" + code + "]";
	}

	public Integer getCodeid() {
		return codeid;
	}

	public void setCodeid(Integer codeid) {
		this.codeid = codeid;
	}

	public String getUserphone() {
		return userphone;
	}

	public void setUserphone(String userphone) {
		this.userphone = userphone == null ? null : userphone.trim();
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
}