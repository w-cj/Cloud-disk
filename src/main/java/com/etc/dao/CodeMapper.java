package com.etc.dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.etc.entity.Code;
@Repository
@Mapper
public interface CodeMapper {
    int deleteByPrimaryKey(Integer codeid);

    int deleteByUserPhone(String userphone);
    
    int insert(Code record);

    int insertSelective(Code record);

    Code selectByPrimaryKey(Integer codeid);
    
    Code selectByUserPhone(String userphone);

    int updateByPrimaryKeySelective(Code record);
    /**
     * cyf
     * 根据验证码id修改验证码信息
     * @param record 验证码的实体类
     * @return 修改的结果
     */
    int updateByPrimaryKey(Code record);
 
   
}