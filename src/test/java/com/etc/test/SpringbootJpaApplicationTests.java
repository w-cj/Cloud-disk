package com.etc.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.etc.dao.ContentsMapper;
import com.etc.entity.Contents;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootJpaApplicationTests {

	@Autowired
	ContentsMapper cm ; 

	@Test
	public void contextLoads() {
		Contents ct = new Contents() ;

		ct.setContentstime("2019-05-20");
		ct.setUserid(1);


		ct.setContentstime("2019-05-20");


		ct.setContentspath("xyz");
		ct.setContentsid(3);
		ct.setSecondid(2);
		ct.setContentsname("adc");
		int i = cm.insertSelective(ct) ;
		System.out.println(i);
	}

}
