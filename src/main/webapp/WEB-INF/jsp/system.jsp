<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@include file="header.jsp"%>


<div class="mdui-container">
	<div class="mdui-row">
		<div class="mdui-col-md-12">
			<div class="mdui-card">
				<div class="mdui-card-media" style="height: 200px;">
					<img src="static/img/1.jpg" />
					<div class="mdui-card-media-covered">
						<div class="mdui-card-primary">
							<div class="mdui-card-primary-title">
								你好，<span class="mdui-text-color-yellow">${user.username}</span>
							</div>
							<div id="su_weather" class="mdui-card-primary-subtitle"></div>
						</div>
					</div>
				</div>
				<div class="mdui-card-actions">
					<a href="http://www.weather.com.cn/weather/101250101.shtml"
						class="mdui-btn mdui-ripple mdui-text-color-white">查看完整天气情况</a>
					<button class="mdui-btn mdui-ripple"></button>
				</div>
			</div>
		</div>
	</div>

	<!-- <div class="mdui-row">
		<div class="mdui-col-md-12">
			<div class="mdui-panel-item mdui-panel-item-open mdui-hoverable">
				<div class="mdui-panel-item-header">当前访问量</div>
				<div class="mdui-panel-item-body">

					<div id="charts-click-cpu" style="width: 100%; height: 240px;"></div>
				</div>
			</div>
		</div>
	</div> -->


	<!-- <div class="mdui-row">
		<div class="mdui-col-md-4">
			<div class="mdui-panel-item mdui-panel-item-open mdui-hoverable">
				<div class="mdui-panel-item-header">C:/ 使用情况</div>
				<div class="mdui-panel-item-body">
					<div id="charts-system-cpu" style="width: 100%; height: 240px;"></div>
				</div>
			</div>
		</div>
		<div class="mdui-col-md-4">
			<div class="mdui-panel-item mdui-panel-item-open mdui-hoverable">
				<div class="mdui-panel-item-header">D:/ 使用情况</div>
				<div class="mdui-panel-item-body">
					<div id="charts-system-mem" style="width: 100%; height: 240px;"></div>
				</div>
			</div>
		</div>
		<div class="mdui-col-md-4">
			<div class="mdui-panel-item mdui-panel-item-open mdui-hoverable">
				<div class="mdui-panel-item-header">E:/ 使用情况</div>
				<div class="mdui-panel-item-body">
					<div id="charts-system-other" style="width: 100%; height: 240px;"></div>
				</div>
			</div>
		</div>
	</div> -->

	<div class="mdui-row">

		<div class="mdui-col-md-4">
			<div class="mdui-card mdui-hoverable">
				<div class="mdui-card-media">
					<img src="static/img/2.jpg" style="height: 220px;" />
					<div class="mdui-card-media-covered">
						<div class="mdui-card-primary">
							<div class="mdui-card-primary-title">网络云盘</div>
							<div class="mdui-card-primary-subtitle">帮助您分享您的资源</div>
						</div>
					</div>
				</div>
				<div class="mdui-card-actions">
					<button class="mdui-btn mdui-ripple mdui-color-orange"
						onclick="PAGE.upload();">上传文件</button>
					<button class="mdui-btn mdui-ripple mdui-color-green-700"
						onclick="PAGE.repassword();">修改密码</button>
				</div>
			</div>
		</div>

		<!-- <div class="mdui-col-md-4">
			<div class="mdui-card mdui-hoverable">
				<div class="mdui-card-media ">
					<img src="static/img/7.jpg" style="height: 220px;" />
					<div class="mdui-card-media-covered">
						<div class="mdui-card-primary">
							<div class="mdui-card-primary-title">社区交友</div>
							<div class="mdui-card-primary-subtitle">认识您的朋友</div>
						</div>
					</div>
				</div>
				<div class="mdui-card-actions">
					<a href="Online"><button
							class="mdui-btn mdui-ripple mdui-color-orange">跳转到社区</button></a>
				</div>
			</div>
		</div> -->

		<div class="mdui-col-md-4">
			<div class="mdui-card mdui-hoverable">
				<div class="mdui-card-media ">
					<img src="static/img/8.jpg" style="height: 220px;" />
					<div class="mdui-card-media-covered">
						<div class="mdui-card-primary">
							<div class="mdui-card-primary-title">我的分享</div>
							<div class="mdui-card-primary-subtitle">管理你的文件</div>
						</div>
					</div>
				</div>
				<div class="mdui-card-actions">
					<button class="mdui-btn mdui-ripple mdui-color-light-green-800"
						onclick="PAGE.toFileManager();">跳转到我的分享</button>
				</div>
			</div>
		</div>

	</div>
</div>

<div class="mdui-dialog" id="su_loading">
	<div class="mdui-dialog-title">您好，新用户</div>
	<div class="mdui-dialog-content">
		<h3>我们正在为您准备一些必要的数据....</h3>
		<p>比如，腾出空间让您存储文件，划分数据库表记录您的文件列表~</p>
		<div class="mdui-progress">
			<div class="mdui-progress-indeterminate"></div>
		</div>
	</div>
	<div class="mdui-dialog-actions"></div>
</div>

<br>
<br>
<br>
<br>
<br>

<script src="static/echarts.common.min.js"></script>
<script src="static/macarons.js"></script>
<script src="static/coll/system_chart.js"></script>

<script>
	(function() {

		/* <c:if test="${!empty first}">
		//新用户第一次访问时展现的内容
		$(function(){
			setTimeout(function(){
				var inst = new mdui.Dialog('#su_loading');
				inst.open();
				setTimeout(function(){
					inst.close();
					mdui.snackbar({
						  message: '欢迎，请开始愉快的使用吧！相信，这是一次愉快的体验！', 
						  position: 'bottom'
					  });
				},10000);
			},1000);
		}); 
		</c:if> */

		/*  function getNowFormatDate() {
		     var date = new Date();
		     var seperator1 = "-";
		     var seperator2 = ":";
		     var month = date.getMonth() + 1;
		     var strDate = date.getDate();
		     if (month >= 1 && month <= 9) {
		         month = "0" + month;
		     }
		     if (strDate >= 0 && strDate <= 9) {
		         strDate = "0" + strDate;
		     }
		     var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
		             + " " + date.getHours() + seperator2 + date.getMinutes()
		             + seperator2 + date.getSeconds();
		     return currentdate;
		 } */
		/* 
		   function XWeather(){
		       var weather_json = ${weather_str};
		       var show_str = ['现在的时间是 ',getNowFormatDate(),
		           '温度',weather_json['weatherinfo']['temp'],"°C",
		           "湿度",weather_json['weatherinfo']['SD'],
		           "气压",weather_json['weatherinfo']['AP']
		           ].join(" ")
		       $("#su_weather").html(show_str);
		   }
		   
		   setInterval(function() {
		       XWeather();
		   }, 1000); */

	})();

	PAGE.repassword = function() {
		mdui.prompt("您的新密码：", "修改密码", function(newfilename) {
			$.post("./repassword", {
				newPassword : newfilename
			}, function(result) {
				mdui.alert("修改完毕！");
			});
		});
	}

	PAGE.toFileManager = function() {
		window.location.href = "myShare/${user.userid}";
	}
</script>

<%@include file="footer.jsp"%>
