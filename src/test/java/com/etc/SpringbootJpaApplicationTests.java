package com.etc;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.etc.dao.ContentsMapper;
import com.etc.dao.UserMapper;
import com.etc.entity.Contents;
import com.etc.service.UserService;


@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootJpaApplicationTests {

	@Autowired
	ContentsMapper cm ; 
	
	@Resource
	private UserService  userService;
	
	@Resource
	private  UserMapper userMapper;

	@Test
	public void contextLoads() {
		Contents ct = new Contents() ;

		ct.setContentstime("2019-05-20");

		ct.setUserid(1);

		ct.setContentspath("xyz");

		ct.setContentsid(3);
		ct.setSecondid(2);
		ct.setContentsname("adc");
		int i = cm.insertSelective(ct) ;
		System.out.println(i);
	}
	
	@Test
	public void getUserByPage() {

	System.out.println(userService.selectAllUserByPage(1, 2, ""));
	
	}
	
	@Test
	public void getUserByPage2() {
		
		
	userMapper.selectAllUserByPage(0, 2, "").forEach(System.out::println);
	
	}
	
	

}
