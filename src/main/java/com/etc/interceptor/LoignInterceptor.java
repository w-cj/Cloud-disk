package com.etc.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.etc.entity.User;

public class LoignInterceptor extends HandlerInterceptorAdapter {
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		StringBuffer sb = new StringBuffer();
		User user = (User) request.getSession().getAttribute("user");
		User user2 = (User) request.getSession().getAttribute("admin");
		if (user != null || user2 != null) {
			return true;
		} else {
			response.sendRedirect("login/login.jsp");
			return false;
		}
	}
}
