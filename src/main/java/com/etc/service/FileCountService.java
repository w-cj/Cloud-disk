package com.etc.service;

import java.util.List;

import com.etc.entity.FileCount;

/**
 * 统计每种类型文件的数量的service接口
 * 
 * @author chao
 *
 */
public interface FileCountService {

	/**
	 * 统计每种类型文件的数量
	 * 
	 * @return 返回集合 集合里面包含每种类型文件的数量
	 */
	public List<FileCount> getFileCount();

}
