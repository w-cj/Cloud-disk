package com.etc.controller;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.etc.entity.Files;
import com.etc.service.FileService;

@RestController
public class FilesController {

	@Autowired
	private FileService fileService;

	@GetMapping("getfiless/{fileid}")
	public StringBuffer getFilessContent(@PathVariable(value = "fileid") Integer fileid) {

		StringBuffer sb = new StringBuffer();
		// 实例化FileReader对象
		FileReader fr = null;
		String str = null;

		Files files = fileService.getFileById(fileid);

		String[] strings = files.getFilepath().split("/");
		String destDir = "D:\\apache-tomcat-8.5.13-windows-x64\\apache-tomcat-8.5.13\\webapps";
		String realpath = destDir + "\\" + strings[3] + "\\" + strings[4];

		try {

			fr = new FileReader(realpath);
			// 实例化BufferedReader

			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(realpath), "UTF-8"));
			// 如果要读取多行数据,请使用循环
			// 如果没有读到数据 null
			// String str = br.readLine();
			while ((str = br.readLine()) != null) {
				if (str != null) {

					sb.append(str + "\r\n");

				}
			}
			br.close();
			fr.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return sb;
	}

}