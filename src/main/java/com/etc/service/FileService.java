package com.etc.service;

import java.util.List;

import com.etc.entity.Files;
import com.etc.util.PageData;

/**
 * 文件的service层
 * @author zeng
 *
 */
public interface FileService {

	/**
	 * 实现文件的显示和分页
	 * 
	 * @param page
	 *            第几页
	 * @param limit
	 *            显示数据条数
	 * @param content
	 *            模糊查询
	 * @return PageData工具类
	 */
	public PageData<Files> getFileByPage(int page, int limit, String content);

	/**
	 * 根据上一级目录的id获取该目录所拥有的文件
	 * 
	 * @param secondId
	 *            上一级目录id
	 * @return 文件列表
	 */
	public List<Files> getFileBySecondId(Integer secondId, Integer userId);

	/**
	 * 根据上一级目录ID和文件名模糊查找
	 * 
	 * @param secondId
	 * @return 文件集合
	 */
	public List<Files> getFileByLike(Integer userId, String content);

	/**
	 * 根据id查询文件
	 * 
	 * @param fileid
	 * @return 文件对象
	 */
	public Files getFileById(Integer fileId);

	/**
	 * 重命名文件
	 * 
	 * @param oldname
	 * @param newfilename
	 * @param secondId
	 * @return 修改结果
	 */
	public boolean reName(String oldName, String newFileName, Integer secondId);

	/**
	 * 删除文件
	 * 
	 * @param secondId
	 * @param filesName
	 * @return
	 */
	public boolean deleteFiles(Integer secondId, String filesName);

	/**
	 * 统计所有文件
	 * 
	 * @return 文件数
	 */
	public int getCount();

	/**
	 * 根据用户id和文件类型查找所有文件
	 * 
	 * @param userId
	 *            用户id
	 * @param fileType
	 *            文件类型
	 * @return 文件集合
	 */
	public List<Files> selectByUserIdAndFileType(Integer userId, Integer fileType);

	/**
	 * 查询单个文件的方法
	 * 
	 * @param secondId
	 *            文件的父目录id
	 * @param fileName
	 *            文件的路径
	 * @return 文件对象
	 */
	public Files selectByFnameAndSid(Integer secondId, String fileName);

	/**
	 * 查询单个文件的方法
	 * 
	 * @param secondId
	 *            文件的父目录id
	 * @param fileName
	 *            文件的路径
	 * @return 文件对象
	 */
	public Files selectByFnameAndSidAndUid(Integer secondId, String fileName, Integer userId);

	/**
	 * 添加单个文件
	 * 
	 * @param file
	 *            文件对象
	 * @return 受影响行数的主键值
	 */
	public int addFile(Files file);
}
