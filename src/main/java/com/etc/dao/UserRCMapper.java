package com.etc.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.etc.entity.UserRegisterCount;

@Repository
@Mapper
public interface UserRCMapper {
	
	/**
	 * 得到近一周用户每天注册的数量
	 * @return  近一周的用户注册数量的集合
	 */
	List<UserRegisterCount>  getUserCount();
	

}

