package com.etc.controller;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.etc.entity.Code;
import com.etc.entity.User;
import com.etc.service.CodeService;
import com.etc.service.UserService;
import com.etc.util.CommenMassage;
import com.etc.util.PageData;
import com.etc.util.PhoneRegex;

/**
 * 关于用户的控制器
 * @author zeng
 *
 */
@RestController
public class UserController {

	@Autowired
	private UserService userService;
	@Autowired
	private CodeService codeService;
	@Autowired
	private CommenMassage commenMassage;

	/**
	 * 分页查询所有用户
	 * 
	 * @param page
	 *            第几页
	 * @param limit
	 *            每页的数目
	 * @param content
	 *            模糊查询的内容
	 * @return
	 */
	@GetMapping("listUser")
	public PageData<User> getAllByPage(int page, int limit, String content) {

		if (content == null) {
			content = "";
		}
		PageData<User> pd = userService.selectAllUserByPage(page, limit, content);
		pd.setCode(0);
		pd.setMsg("请求成功");

		return pd;
	}

	/**
	 * 根据id查询单个用户信息
	 * 
	 * @param userid
	 *            条件
	 * @return
	 */
	@GetMapping("getUser/{userid}")
	public User getUserById(@PathVariable(value = "userid") int userid) {

		User user = userService.selectByPrimaryKey(userid);
		return user;
	}

	/**
	 * 增加用户
	 * 
	 * @param user
	 * @return
	 */
	@PutMapping("addUser")
	public CommenMassage addUser(@RequestBody User user) {

		boolean flag = userService.insert(user);
		String str = "增加用户成功";
		if (!flag) {
			str = "增加用户失败";
		}
		commenMassage.setMsg(str);
		return commenMassage;

	}

	/**
	 * 修改用户的信息
	 * 
	 * @param user
	 *            修改后的用户
	 * @return
	 */
	@PutMapping("updateUser")
	public CommenMassage updateUser(@RequestBody User user) {

		boolean flag = userService.updateUser(user);

		String str = "修改用户成功";
		if (!flag) {
			str = "修改用户失败";
		}
		commenMassage.setMsg(str);
		return commenMassage;
	}

	/**
	 * 通过id禁用用户 状态为0
	 * 
	 * @param userid
	 * @return
	 */
	@DeleteMapping("deleteUser/{userid}")
	public CommenMassage deleteUser(@PathVariable(value = "userid") int userid) {

		boolean flag = userService.deleteByPrimaryKey(userid);

		String str = "操作成功";
		if (!flag) {

			str = "操作失败";

		}
		commenMassage.setMsg(str);
		return commenMassage;

	}

	/**
	 * cyf 通过用户名校验用户是否存在
	 * 
	 * @param username
	 *            用户名
	 * @return 校验结果
	 */
	@GetMapping("checkUserByName")
	public CommenMassage checkUserByName(@RequestParam(value = "username") String username) {

		if (username.equals("")) {
			commenMassage.setMsg("");
			return commenMassage;
		}

		User user = userService.selectByUserName(username);

		if (user == null) {
			commenMassage.setMsg("该用户不存在");
		} else {
			commenMassage.setMsg("该用户存在");
		}

		return commenMassage;

	}

	/**
	 * cyf 通过用户名和密码校验用户信息
	 * 
	 * @param username
	 *            用户名
	 * @param userpwd
	 *            密码
	 * @return 校验结果
	 */
	@GetMapping("checkUser")
	public CommenMassage checkUser(@RequestParam(value = "username") String username,
			@RequestParam(value = "userpwd") String userpwd, HttpSession session) {

		if (username == null || username.equals("") || userpwd == null || userpwd.equals("")) {
			username = "";
			userpwd = "";
		}

		User user = userService.selectByUserName(username);

		if (user == null) {
			commenMassage.setMsg("密码错误");
		} else if (user.getUserpwd().equals(userpwd)) {
			commenMassage.setMsg("密码正确");
			// 把用户信息存到session中
			session.setAttribute("user", user);
		} else {
			commenMassage.setMsg("密码错误");
		}

		return commenMassage;

	}

	/**
	 * cyf 密码框校验
	 * 
	 * @param username
	 *            用户名
	 * @param userpwd
	 *            密码
	 * @return 校验结果
	 */
	@GetMapping("checkUserPwd")
	public CommenMassage checkUserPwd(@RequestParam(value = "userpwd") String userpwd) {

		if (userpwd == null || userpwd.equals("")) {

			commenMassage.setMsg("密码为空");

		} else {
			commenMassage.setMsg("校验密码");
		}
		return commenMassage;

	}

	/**
	 * cyf通过正则校验手机号是否符合规则及是否已被注册
	 * 
	 * @param userphone
	 *            用户手机号
	 * @return 校验结果
	 */
	@GetMapping("checkUserByPhone")
	public CommenMassage checkUserByPhone(@RequestParam(value = "userphone") String userphone) {
		boolean flag = PhoneRegex.checkPhone(userphone);
		if (flag) {
			User user = userService.selectByUserPhone(userphone);
			if (user == null) {
				commenMassage.setMsg("该手机号未注册");
			} else {
				commenMassage.setMsg("该手机号已注册");
			}
		} else {
			commenMassage.setMsg("该手机号不合规");
		}

		return commenMassage;

	}

	/**
	 * cyf接收表单数据并注册
	 * 
	 * @param user
	 *            封装的用户信息
	 * @return 添加的结果
	 */
	@GetMapping("register")
	public CommenMassage register(User user, String password) {
		if ((!user.getUsername().equals("")) && (!user.getUserphone().equals(""))
			&& (user.getUserphone().length() == 11) && (!user.getUserpwd().equals("")) 
			&& (!password.equals(""))) {
			Code code = codeService.selectByUserPhone(user.getUserphone());
			int pwd = Integer.parseInt(password);
			if(code==null) {
				commenMassage.setMsg("请先获取验证码");
			}else if (code.getCode() == pwd) {
				int n=codeService.deleteByUserPhone(user.getUserphone());
				boolean flag = userService.insert(user);
				if (flag) {
					commenMassage.setMsg("添加成功");
					// 注册成功后创建该用户的文件夹
					String path = "D:\\apache-tomcat-8.5.13-windows-x64\\apache-tomcat-8.5.13\\webapps\\files"
							+ user.getUserid();
					File file = new File(path);
					file.mkdirs();
				} else {
					commenMassage.setMsg("添加失败");
				}
			} else {
				commenMassage.setMsg("验证码有误");
			}
		} else {
			commenMassage.setMsg("添加失败");
		}

		return commenMassage;

	}

	/**
	 * 查找所有陌生人
	 * 
	 * @param content
	 *            模糊查询关键字
	 * @return 返回一个用户的集合
	 */
	@GetMapping(value = "findPeople")
	public List<User> findPeople(String content,Integer userid) {
		if (content == null) {
			content = "";
		}
		return userService.getByNameOrPhone(content,userid);
	}

	/**
	 * 手机号登录功能
	 * 
	 * @param userphone
	 *            用户手机号
	 * @param password
	 *            验证码
	 * @return 验证的结果信息
	 */
	@GetMapping("phonelogin")
	public CommenMassage register(String userphone, String password, HttpSession session) {
		if ((!userphone.equals("")) && (userphone.length() == 11) && (!password.equals(""))) {
			Code code = codeService.selectByUserPhone(userphone);
			int pwd = Integer.parseInt(password);
			User user = userService.selectByUserPhone(userphone);
			if (code.getCode() == pwd) {
				commenMassage.setMsg("登陆成功");
				session.setAttribute("user", user);
				int a=codeService.deleteByUserPhone(userphone);
			} else {
				commenMassage.setMsg("验证码有误");
			}
		} else {
			commenMassage.setMsg("信息有误");
		}

		return commenMassage;

	}

	/**
	 * 修改密码
	 * 
	 * @param userpwd
	 * @return
	 */
	@PostMapping("repassword")
	public CommenMassage repassword(@RequestParam(value = "newPassword") String userpwd, HttpSession session) {
		User user = (User) session.getAttribute("user");
		user.setUserpwd(userpwd);
		boolean flag = userService.updateByPrimaryKeySelective(user);
		if (flag) {
			commenMassage.setMsg("修改成功");
		} else {
			commenMassage.setMsg("修改失败");
		}
		return commenMassage;
	}

}
