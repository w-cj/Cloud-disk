package com.etc.service.impl;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etc.dao.ShareUserMapper;
import com.etc.service.ShareUserService;

/**
 * 分享的service层实现类
 * 
 * @author zeng
 *
 */
@Service
public class ShareUserServiceImpl implements ShareUserService {

	/**
	 * 注入ShareUserMapper
	 */
	@Autowired
	private ShareUserMapper shareUserMapper;

	/**
	 * 查询我的分享记录
	 */
	@Override
	public List<ShareUserMapper> getShareByFromid(Integer fromuserId) {
		// TODO Auto-generated method stub
		return shareUserMapper.selectShareByFromid(fromuserId);
	}

	/**
	 * 返回一个好友的分享记录
	 */
	@Override
	public List<ShareUserMapper> getShareByToid(Integer touserId, Integer fromuserId) {
		// TODO Auto-generated method stub
		return shareUserMapper.selectShareByToid(touserId, fromuserId);
	}

	/**
	 * 分页查询分享记录
	 */
	@Override
	public List<ShareUserMapper> getShareByPage(Integer page, Integer pageSize, String content, Integer fromuserId) {
		// TODO Auto-generated method stub
		return shareUserMapper.getShareByPage(page, pageSize, content, fromuserId);
	}

}
