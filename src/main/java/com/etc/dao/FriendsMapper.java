package com.etc.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.etc.entity.Friends;
import com.etc.entity.User;

@Mapper
@Repository
public interface FriendsMapper {

	
	/**
	 * 删除好友请求
	 * @param recond 好友对象
	 * @return 
	 */
	int delFriendsOrReq(Friends recond);
	
	/**
	 * 添加好友
	 * @param record 好友对象
	 * @return 添加好友的主键值
	 */
    int insertSelective(Friends record);
    
    /**
     * 添加好友请求记录
     * @param recond 好友对象
     * @return 
     */
    int addFriendsReq(Friends recond);
    
    /**
     * 处理好友请求(同意添加)
     * @param recond
     * @return
     */
    int handleFriendsReq(Friends recond);
    
    /**
     * 查找好友请求
     * @param userid userid 用户id
     * @return 返回所有的好友的请求集合
     */
    List<User> getFriendsReq(Integer userid);
    
    /**
     * 查找所有的关系好友
     * @param userid 用户id
     * @return 返回一个好友的集合
     */ 
    List<User> getFriendsUser(Integer userid);
    
    /**
     * 判断好友请求是否存在
     * @param fromuserid 发送请求者
     * @param touserid 被发送的请求者
     * @return
     */
    List<Friends> getFriendByReq(@Param("fromuserid") Integer fromuserid,@Param("touserid") Integer touserid);
}