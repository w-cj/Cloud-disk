package com.etc.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.etc.entity.Audit;
import com.etc.entity.Files;
import com.etc.entity.FileSort;
import com.etc.service.AuditService;
import com.etc.service.FileService;
import com.etc.service.FileSortService;
import com.etc.util.CommenMassage;
import com.etc.util.PageData;

@RestController
public class FileSortController {

	@Autowired
	private FileSortService fss;

	@Autowired
	private AuditService auditService;

	@Autowired
	private FileService fileService;

	@Autowired
	private CommenMassage commenMassage;

	/**
	 * 分页查询所有图片
	 * 
	 * @param page
	 *            第几页
	 * @param limit
	 *            每页的数目
	 * @param content
	 *            模糊查询的内容
	 * @return
	 */
	@GetMapping(value = "listPicture")
	public PageData<FileSort> getAllPictureByPage(int page, int limit, String content) {

		if (content == null) {
			content = "";
		}
		PageData<FileSort> pd = fss.selectPictureBypage(page, limit, content);
		pd.setCode(0);
		pd.setMsg("请求成功");

		return pd;
	}

	/**
	 * 分页查询所有音乐
	 * 
	 * @param page
	 *            第几页
	 * @param limit
	 *            每页的数目
	 * @param content
	 *            模糊查询的内容
	 * @return
	 */
	@GetMapping(value = "listMusic")
	public PageData<FileSort> getAllMusicByPage(int page, int limit, String content) {

		if (content == null) {
			content = "";
		}
		PageData<FileSort> pd = fss.selectMusicBypage(page, limit, content);
		pd.setCode(0);
		pd.setMsg("请求成功");

		return pd;
	}

	/**
	 * 分页查询所有视频
	 * 
	 * @param page
	 *            第几页
	 * @param limit
	 *            每页的数目
	 * @param content
	 *            模糊查询的内容
	 * @return
	 */
	@GetMapping(value = "listMovie")
	public PageData<FileSort> getAllMovieByPage(int page, int limit, String content) {

		if (content == null) {
			content = "";
		}
		PageData<FileSort> pd = fss.selectMovieBypage(page, limit, content);
		pd.setCode(0);
		pd.setMsg("请求成功");

		return pd;
	}

	/**
	 * 分页查询所有视频
	 * 
	 * @param page
	 *            第几页
	 * @param limit
	 *            每页的数目
	 * @param content
	 *            模糊查询的内容
	 * @return
	 */
	@GetMapping(value = "listtmp")
	public PageData<FileSort> getAllTmpByPage(int page, int limit, String content) {

		if (content == null) {
			content = "";
		}
		PageData<FileSort> pd = fss.selectTmpBypage(page, limit, content);
		pd.setCode(0);
		pd.setMsg("请求成功");

		return pd;
	}

	/**
	 * 分页查询所有为审核视频
	 * 
	 * @param page
	 *            第几页
	 * @param limit
	 *            每页的数目
	 * @param content
	 *            模糊查询的内容
	 * @return
	 */
	@GetMapping(value = "listAudit")
	public PageData<Audit> getAllAuditByPage(int page, int limit, String content) {

		if (content == null) {
			content = "";
		}
		PageData<Audit> pd = auditService.selectAllAuditByPage(page, limit, content);
		pd.setCode(0);
		pd.setMsg("请求成功");
		return pd;
	}

	@PostMapping(value = "addAudit")
	public CommenMassage addAudit(@RequestBody Audit audit) {
		Files file = new Files(null, audit.getFilename(), audit.getFilepath(), audit.getSecondid(),
				new Date().toLocaleString(), audit.getFiletype(), audit.getUserid(), audit.getFilesize());
		int i = fileService.addFile(file);
		if (i > 0) {
			commenMassage.setMsg("通过成功");
			audit.setStatus(1);
			auditService.updateByPrimaryKeySelective(audit);
		} else {
			commenMassage.setMsg("通过失败");
		}
		return commenMassage;
	}

	@DeleteMapping(value = "deleteAudit/{fileid}")
	public CommenMassage deleteAudit(@PathVariable(value = "fileid") Integer fileId) {
		Audit audit = auditService.selectByPrimaryKey(fileId);
		audit.setStatus(2);
		boolean flag = auditService.updateByPrimaryKeySelective(audit);
		if (flag) {
			commenMassage.setMsg("删除成功");
		} else {
			commenMassage.setMsg("删除失败");
		}
		return commenMassage;
	}
}
