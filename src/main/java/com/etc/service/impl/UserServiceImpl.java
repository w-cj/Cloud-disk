package com.etc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etc.dao.UserMapper;
import com.etc.entity.User;
import com.etc.service.UserService;
import com.etc.util.PageData;

/**
 * 对用户和管理员操作的service接口实现类
 * 
 * @author chao
 *
 */
@Service(value = "userService")
public class UserServiceImpl implements UserService {

	/**
	 *  注入UserMapper
	 */
	@Autowired
	private UserMapper userMapper;

	/**
	 * 分页查询用户
	 * 
	 * @param page
	 *            当前页
	 * @param limit
	 *            每页的页数
	 * @param content
	 *            模糊查询的内容
	 * @return
	 */
	@Override
	public PageData<User> selectAllUserByPage(int page, int limit, String content) {
		// TODO Auto-generated method stub

		int start = (page - 1) * limit;

		List<User> list = userMapper.selectAllUserByPage(start, limit, content);

		int count = userMapper.getCount(content);

		PageData<User> pd = new PageData<>(list, count, limit, page);
		return pd;

	}

	/**
	 * 增加用户
	 * 
	 * @param user
	 *            所增加的用户
	 * @return
	 */
	@Override
	public boolean insert(User user) {
		// TODO Auto-generated method stub
		return userMapper.insert(user) > 0;
	}

	/**
	 * 修改用户的信息
	 * 
	 * @param user
	 *            修改后的用户
	 * @return 是否成功的boolean值
	 */
	@Override
	public boolean updateUser(User user) {
		// TODO Auto-generated method stub
		return userMapper.updateByPrimaryKey(user) > 0;

	}

	/**
	 * 禁用/启用 用户
	 * 
	 * @param userid
	 *            用户的userId
	 * @return
	 */
	@Override
	public boolean deleteByPrimaryKey(Integer userid) {
		// TODO Auto-generated method stub
		return userMapper.deleteByPrimaryKey(userid) > 0;

	}

	/**
	 * 根据用户名查找用户信息
	 * 
	 * @param username
	 *            用户名
	 * @return 用户对象
	 */
	@Override
	public User selectByUserName(String username) {

		User user = userMapper.selectByUserName(username);

		return user;
	}

	/**
	 * 根据用户手机号查询用户信息
	 * 
	 * @param userphone
	 *            用户手机号
	 * @return 用户信息
	 */
	@Override
	public User selectByUserPhone(String userphone) {

		User user = userMapper.selectByUserPhone(userphone);
		return user;
	}

	/**
	 * 根据用户名查看用户是否存在，实现登录--zeng
	 * 
	 * @param username
	 *            输入的用户名
	 * @return
	 */
	@Override
	public User selectUserByName(String username) {
		// TODO Auto-generated method stub
		return userMapper.selectUserByName(username);
	}

	/**
	 * 根据名字和电话号码搜索好友
	 * 
	 * @param content
	 *            根据输入信息搜索好友
	 * @return 返回一个用户的集合
	 */
	@Override
	public List<User> getByNameOrPhone(String content, Integer userid) {
		// TODO Auto-generated method stub
		return userMapper.selectByNameOrPhone(content, userid);
	}

	/**
	 * 根据id查询单个用户
	 * 
	 * @param userid
	 * @return
	 */
	@Override
	public User selectByPrimaryKey(Integer userid) {
		// TODO Auto-generated method stub
		return userMapper.selectByPrimaryKey(userid);
	}

	/**
	 * 修改用户信息
	 * 
	 * @param user
	 * @return 是否成功的boolean值
	 */
	@Override
	public boolean updateByPrimaryKeySelective(User user) {
		// TODO Auto-generated method stub
		return userMapper.updateByPrimaryKeySelective(user) > 0;
	}

	/**
	 * 分页查询管理员
	 * 
	 * @param page
	 *            当前页
	 * @param limit
	 *            每页的页数
	 * @param content
	 *            模糊查询的内容
	 * @return
	 */
	@Override
	public PageData<User> selectAllAdminiByPage(int page, int limit, String content) {
		// TODO Auto-generated method stub
		int start = (page - 1) * limit;

		List<User> list = userMapper.selectAllAdminByPage(start, limit, content);

		int count = userMapper.getAdminCount(content);

		PageData<User> pd = new PageData<>(list, count, limit, page);
		return pd;
	}

	/**
	 * 统计总用户数
	 * 
	 * @return 总用户数
	 */
	@Override
	public int selectCountUser() {
		// TODO Auto-generated method stub
		return userMapper.selectCountUser();
	}

}
