package com.etc.util;

import org.springframework.stereotype.Component;
/**
 * 用户登录验证后返回信息类
 * @author China
 *20192019年5月24日上午10:27:42
 */
@Component
public class CommenMassage {
	     //返回信息
         private String  msg;

		public String getMsg() {
			return msg;
		}

		public void setMsg(String msg) {
			this.msg = msg;
		}

		@Override
		public String toString() {
			return "CommenMassage [msg=" + msg + "]";
		}

		public CommenMassage(String msg) {
			super();
			this.msg = msg;
		}
          public CommenMassage() {
			// TODO Auto-generated constructor stub
		}
          
}
