package com.etc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etc.dao.AuditMapper;
import com.etc.entity.Audit;
import com.etc.service.AuditService;
import com.etc.util.PageData;

/**
 * 审核的service层实现
 * 
 * @author zeng
 *
 */
@Service
public class AuditServiceImpl implements AuditService {

	/**
	 * 注入AuditMapper
	 */
	@Autowired
	private AuditMapper auditMapper;

	/**
	 * 分页查询用户
	 * 
	 * @param page
	 *            当前页
	 * @param limit
	 *            每页的页数
	 * @param content
	 *            模糊查询的内容
	 * @return
	 */
	@Override
	public PageData<Audit> selectAllAuditByPage(int page, int limit, String content) {
		int start = (page - 1) * limit;

		List<Audit> list = auditMapper.selectAllAuditByPage(start, limit, content);

		int count = auditMapper.getCount(content);

		PageData<Audit> pd = new PageData<>(list, count, limit, page);
		return pd;
	}

	/**
	 * 添加待审核文件到数据库
	 * 
	 * @param record
	 *            需要审核的文件
	 * @return 添加的结果
	 */
	@Override
	public boolean insertSelective(Audit record) {

		return auditMapper.insertSelective(record) > 0;
	}

	/**
	 * 根据主键修改审核信息
	 * 
	 * @param record
	 *            审核对象
	 * @return 修改结果
	 */
	public boolean updateByPrimaryKeySelective(Audit record) {
		// TODO Auto-generated method stub
		return auditMapper.updateByPrimaryKeySelective(record) > 0;
	}

	/**
	 * 根据文件ID查询审核对象
	 * 
	 * @param fileId
	 *            文件ID
	 * @return 审核对象
	 */
	@Override
	public Audit selectByPrimaryKey(Integer fileId) {
		// TODO Auto-generated method stub
		return auditMapper.selectByPrimaryKey(fileId);
	}

	/**
	 * 根据用户id查找
	 * 
	 * @param secondId
	 * @return 文件集合
	 */
	@Override
	public List<Audit> getMyAudit(Integer userId) {
		// TODO Auto-generated method stub
		return auditMapper.getMyAudit(userId);
	}

	/**
	 * 通过文件名和用户ID查询审核对象
	 * 
	 * @param userId
	 *            用户ID
	 * @param name
	 *            文件名
	 * @return 审核对象
	 */
	@Override
	public Audit selectByName(Integer userId, String fileName) {
		// TODO Auto-generated method stub
		return auditMapper.selectByName(userId, fileName);
	}

	/**
	 * 删除审核对象
	 * 
	 * @param fileId
	 *            文件ID
	 * @return 删除结果
	 */
	@Override
	public boolean deleteByPrimaryKey(Integer fileId) {
		// TODO Auto-generated method stub
		return auditMapper.deleteByPrimaryKey(fileId) > 0;
	}

}
